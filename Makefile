VMECTOOLS_DIR := /home/bfaber/projects/VMECTools/
VMECTOOLS_LIB := $(addprefix $(VMECTOOLS_DIR),lib/libvmectools.a)
LIBSTELL := $(addprefix $(VMECTOOLS_DIR),lib/mini_libstell.a)
#LIBSTELL_DIR := /home/bfaber/projects/stellopt/src/LIBSTELL/Release/
#LIBSTELL := $(addprefix $(LIBSTELL_DIR),libstell.a)

OBJ_DIR:=objs
$(shell mkdir -p $(OBJ_DIR))
CXX_OBJ_DIR := $(addprefix $(OBJ_DIR)/,cxx)
$(shell mkdir -p $(CXX_OBJ_DIR))
F_OBJ_DIR := $(addprefix $(OBJ_DIR)/,fortran)
$(shell mkdir -p $(F_OBJ_DIR))

LIB_DIR:=lib
$(shell mkdir -p $(LIB_DIR))

CXX_SRC_DIR := src_cpp
CXX_SRC := $(shell find $(CXX_SRC_DIR) -name '*.cpp' | sed "s|^\./||")
CXX_OBJ := $(addprefix $(CXX_OBJ_DIR)/,$(subst .cpp,.o,$(notdir $(CXX_SRC))))
CXX_LIB := $(addprefix $(LIB_DIR)/,libptsm3dcxx.a)

F90_SRC_DIR := src_F90
F90_MOD_SRC_DIR := $(addprefix $(F90_SRC_DIR)/,modules)
F90_MOD_SRC := $(shell find $(F90_MOD_SRC_DIR) -maxdepth 1 -name '*.F90' | sed "s|&\./||")
F90_MOD := $(addprefix $(F_OBJ_DIR)/,$(subst .F90,.mod,$(notdir $(F90_MOD_SRC))))
F90_SRC := $(shell find $(F90_SRC_DIR) -maxdepth 1 -name '*.F90' | sed "s|&\./||")
F90_OBJ := $(addprefix $(F_OBJ_DIR)/,$(subst .F90,.o,$(notdir $(F90_SRC)))) \
					 $(addprefix $(F_OBJ_DIR)/,$(subst .F90,.o,$(notdir $(F90_MOD_SRC))))
FORTRAN_LIB := $(addprefix $(LIB_DIR)/,libptsm3df.a)
FC_INC := $(addprefix -I ,$(F_OBJ_DIR)) $(addprefix -J ,$(F_OBJ_DIR))

F90_LEGACY_SRC_DIR := $(addprefix $(F90_SRC_DIR)/,legacy)
F90_LEGACY_MOD_SRC_DIR := $(addprefix $(F90_LEGACY_SRC_DIR)/,modules)
F90_LEGACY_MOD_SRC := $(shell find $(F90_LEGACY_MOD_SRC_DIR) -maxdepth 1 -name '*.F90' | sed "s|&\./||")
F90_LEGACY_SRC := $(shell find $(F90_LEGACY_SRC_DIR) -maxdepth 1 -name '*.F90' | sed "s|&\./||")
F90_LEGACY_MOD := $(addprefix $(F_OBJ_DIR)/,$(subst .F90,.mod,$(notdir $(F90_LEGACY_MOD_SRC))))
F90_LEGACY_MOD_OBJ := $(addprefix $(F_OBJ_DIR)/,$(subst .F90,.o,$(notdir $(F90_LEGACY_MOD_SRC))))
F90_LEGACY_OBJ := $(addprefix $(F_OBJ_DIR)/,$(subst .F90,.o,$(notdir $(F90_LEGACY_SRC))))	$(addprefix $(F_OBJ_DIR)/,$(subst .F90,.o,$(notdir $(F90_LEGACY_MOD_SRC))))
LEGACY_LIB := $(addprefix $(LIB_DIR)/,libptsm3dlegacy.a)

LDFLAGS :=
LIBS := -lnetcdff -lhdf5_hl_fortran -lhdf5_fortran -lhdf5_hl -lhdf5
PRECOMP :=
WITH_BLAZE = 0
ifeq ($(WITH_BLAZE),1)
PRECOMP += -DWITH_BLAZE
endif

DEBUG = 0
ifeq ($(DEBUG),1)
PRECOMP += -DDEBUG
FC_COMPILE_FLAGS = -g -O0
CXX_COMPILE_FLAGS = -g -O0
else
FC_COMPILE_FLAGS = -O2 -march=native
CXX_COMPILE_FLAGS = -O2 -march=native
endif

DEP_DIR := .d
$(shell mkdir -p $(DEP_DIR) >/dev/null)
DEPFLAGS = -MT $@ -MMD -MP -MF $(DEP_DIR)/$*.Td

POSTCOMPILE = @mv -f $(DEP_DIR)/$*.Td $(DEP_DIR)/$*.d && touch $@

EXEC:=PTSM3D_C
LEGACY_EXEC:=PTSM3D_F
F_EXEC := test_f
MPIEXEC = @mpiexec

all: exec legacy locallib
locallib: cxxlib fortlib legacy_lib
exec: $(EXEC)
cxxlib: $(CXX_LIB)
fortlib: $(FORTRAN_LIB)
legacy_lib: $(LEGACY_LIB)
legacy: $(LEGACY_EXEC)
ftest: $(F_EXEC)
test: $(EXEC) ctest

ifeq ($(MAKECMDGOALS),)
MAKE_CXX = 1
MAKE_LEGACY = 1
else
ifeq ($(MAKECMDGOALS),all)
MAKE_CXX = 1
MAKE_LEGACY = 1
endif
ifeq ($(MAKECMDGOALS),exec)
MAKE_CXX = 1
endif
ifeq ($(MAKECMDGOALS),cxxlib)
MAKE_CXX = 1
endif
ifeq ($(MAKECMDGOALS),fortlib)
MAKE_CXX = 1
endif
ifeq ($(MAKECMDGOALS),ftest)
MAKE_CXX = 1
endif
ifeq ($(MAKECMDGOALS),legacy)
MAKE_LEGACY = 1
endif
ifeq ($(MAKECMDGOALS),legacy_lib)
MAKE_LEGACY = 1
endif
endif

###############################################################################
#                                                                             #
# Make rules for the C++/SLEPc version of PTSM3D                              #
#                                                                             #
###############################################################################
ifeq ($(MAKE_CXX),1)
PETSC_HOME:=
SLEPC_HOME:=
ifeq (,${PETSC_DIR})
  ifeq (,$(PETSC_HOME))
		$(error Requires complex PETSc!)
  else
    PETSC_DIR:=$(PETSC_HOME)
    export $(PETSC_DIR)
  endif
endif

ifneq (,${SLEPC_DIR})
  SLEPC_CMPLX_LOWER:=$(findstring complex,${SLEPC_DIR})
  SLEPC_CMPLX_UPPER:=$(findstring COMPLEX,${SLEPC_DIR})
  ifneq (,$(SLEPC_CMPLX_LOWER))
    SLEPC_COMPLEX_DIR:=${SLEPC_DIR}
  endif 
  ifneq (,$(SLEPC_CMPLX_UPPER))
    SLEPC_COMPLEX_DIR:=${SLEPC_DIR}
  endif
  ifeq (,$(SLEPC_CMPLX_LOWER))
    ifeq (,$(SLEPC_CMPLX_UPPER))
			$(error Requires complex SLEPc!)
    endif
  endif
else
  ifneq (,$(SLEPC_HOME))
    SLEPC_COMPLEX_DIR:=$(SLEPC_HOME)
    SLEPC_DIR:=$(SLEPC_HOME)
    export $(SLEPC_DIR)
  endif
  ifeq (,$(SLEPC_HOME))
		$(error Requires complex SLEPc, PETSc!)
  endif
endif

F90_DEP_FILE := $(addprefix $(F90_SRC_DIR)/,PTSM3D_F90.dep)
sinclude $(F90_DEP_FILE)

#Define the Fortran compile rules
%.o : %.F90
$(F_OBJ_DIR)/%.o : $(F90_SRC_DIR)/%.F90
	@echo "Compiling $<"
	${FLINKER} $(FC_COMPILE_FLAGS) $(FC_INC) -c -o $@ $<

%.o : %.F90
$(F_OBJ_DIR)/%.o : $(F90_SRC_DIR)/modules/%.F90
	@echo "Compiling $<"
	${FLINKER} $(FC_COMPILE_FLAGS) $(FC_INC) -c -o $@ $<

$(F_EXEC) : $(F90_OBJ) $(CXX_LIB)
	@echo "Linking"
	${FLINKER} $(FC_COMPILE_FLAGS) $(FC_INC) -o $@ $^ ${SLEPC_SYS_LIB}

$(FORTRAN_LIB): $(F90_OBJ) $(F90_MOD)
	ar crs $@ $^

#Define the C++ compile rules
%.o : %.cpp
$(CXX_OBJ_DIR)/%.o : $(CXX_SRC_DIR)/%.cpp $(DEP_DIR)/%.d
	@echo "Compiling $<"
	@${CXXLINKER} $(CXX_COMPILE_FLAGS) $(PRECOMP) $(DEPFLAGS) -c -o $@ $<
	@$(POSTCOMPILE)

$(EXEC): $(CXX_OBJ)
	@echo "Linking"
	@${CXXLINKER} $(CXX_COMPILE_FLAGS) $(LDFLAGS) -o $@ $^ $(VMECTOOLS_LIB) $(LIBSTELL) $(LIBS) ${SLEPC_SYS_LIB}
	@${RM} PTSM3D_C.o

$(CXX_LIB): $(CXX_OBJ)
	ar crs $@ $^

#Empty target for the dependency files
$(DEP_DIR)/%.d: ;
.PRECIOUS: $(DEP_DIR)/%.d

include $(wildcard $(patsubst %,$(DEP_DIR)/%.d,$(basename $(SRCS))))
include ${SLEPC_DIR}/lib/slepc/conf/slepc_common
endif

###############################################################################
#                                                                             #
# Make rules for the legacy Fortran version of PTSM3D                         #
#                                                                             #
###############################################################################
ifeq ($(MAKE_LEGACY),1)
FC=mpifort
FCFLAGS=-O3 -march=native -fdefault-real-8 -fdefault-double-8 -ffree-line-length-0

INCS := $(addprefix -I ,$(VMECTOOLS_DIR)/objs) $(addprefix -I ,$(LIBSTELL_DIR)) $(addprefix -I ,$(F_OBJ_DIR)) $(addprefix -J ,$(F_OBJ_DIR))

LINK = $(FC) $(FCFLAGS) -o

depfile := $(addprefix $(F90_LEGACY_SRC_DIR)/,PTSM3D.dep)
include $(depfile)

$(F_OBJ_DIR)/%.o : $(F90_LEGACY_MOD_SRC_DIR)/%.F90
	@echo "Compiling $<"
	$(FC) $(FCFLAGS) $(INCS) -c -o $@ $<

$(F_OBJ_DIR)/%.o : $(F90_LEGACY_SRC_DIR)/%.F90
	@echo "Compiling $<"
	$(FC) $(FCFLAGS) $(INCS) -c -o $@ $<

$(LEGACY_EXEC) : $(F90_LEGACY_OBJ)
	@echo "Linking $@"
	$(FC) $(FCFLAGS) $(INCS) -o $@ $^ $(VMECTOOLS_LIB) $(LIBSTELL) -lnetcdff -lopenblas

$(LEGACY_LIB): $(F90_LEGACY_OBJ) $(F90_LEGACY_MOD)
	ar crs $@ $^

endif


.PHONY: localclean clean_fortran ctest list

localclean:
	@rm -f $(CXX_OBJ) $(EXEC) $(F90_OBJ) $(F90_MOD) $(F_EXEC) $(F90_LEGACY_OBJ) $(F90_LEGARCY_MOD) $(LEGACY_EXEC)

clean_fortran:
	@rm -f $(F90_LEGACY_OBJ) $(F90_LEGACY_MOD_OBJ) $(F90_LEGACY_MOD) $(LEGACY_EXEC)

ctest: 
	$(MPIEXEC) -n 1 -bind-to core ./$(EXEC)

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'
