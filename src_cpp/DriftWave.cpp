#include <exception>
#include <iterator>
#include <algorithm>
#include <tuple>
#include <utility>

#include "DriftWave.hpp"
#include "PTSM3D.hpp"
#include "Geom.hpp"

DriftWave::DriftWave(const int kxInd, const int kyInd,const int nFields,
                     const int nModes, PTSM3D* const ptsm3dPtr)
{
  if (nFields > 0) {
    _kxInd = kxInd;
    _kyInd = kyInd;
    _nFields = nFields;
    if (nModes > 0) {
      setNModes(nModes); 
    } else if (nModes == 0) {
      addEmptyEntry();
    } else {
      throw std::invalid_argument("nModes < 0!");
    }
  }
  else {
    throw std::invalid_argument("nFields <= 0!");
  }
  _satModel = ptsm3dPtr;

  _triplets.resize(3);
  _triplets.at(0) = std::make_tuple(0,1,0);
  _triplets.at(1) = std::make_tuple(0,1,1);
  _triplets.at(2) = std::make_tuple(0,1,2);
}

DriftWave::~DriftWave()
{
}

void DriftWave::setNModes(const int nModes) {
  _nModes = nModes;
  _zIndices.resize(_nModes);
  _eigVals.resize(_nModes);
  _eigVecs.resize(_nModes);
  _massMat.resize(_nModes);
  _massMatInv.resize(_nModes);
  _avgBkDk.resize(_nModes);
  _crossBk.resize(_nModes);
  _zonalTauCC.resize(_nModes);
  _nonZonalTauCC.resize(_nModes);
  _resonantKxZonal.resize(_nModes);
  _resonantKNonZonal.resize(_nModes);
  _maxTriplet.resize(_nModes);
  _subModeTriple.resize(_nModes);
  _kPerpTriplet.resize(_nModes);

  for(int i=0; i<_nModes; ++i)
  {
    _eigVals[i].resize(_nFields);
    _massMat.at(i).resize(_nFields*_nFields);
    _massMatInv.at(i).resize(_nFields*_nFields);
  }
}

void DriftWave::addEmptyEntry() {
  _nModes = 0;
  _zIndices.resize(1,std::make_tuple(1,0,0,0,0));
  _eigVals.resize(1);
  _eigVecs.resize(1);
  _massMat.resize(1);
  _massMatInv.resize(1);
  _avgBkDk.resize(1,std::make_tuple(0.0,0.0));
  _crossBk.resize(1,0.0);
  _zonalTauCC.resize(1,std::make_tuple(0.0,0.0,0.0));
  _nonZonalTauCC.resize(1,std::make_tuple(0.0,0.0,0.0));
  _maxTriplet.resize(1,std::make_tuple(-1,-1,-1));
  _subModeTriple.resize(1,std::make_tuple(-1,-1,-1));
  _kPerpTriplet.resize(1,std::make_tuple(0,0,0,0,0,0));
  _resonantKNonZonal.resize(1,std::make_tuple(-1,-1,-1,-1));
  _resonantKxZonal.resize(1,-1);

  _eigVals.at(0).resize(_nFields,0.0);
  _eigVecs.at(0).resize(1,0.0);
  _massMat.at(0).resize(1,0.0);
  _massMatInv.at(0).resize(1,0.0);
}

void DriftWave::setEigVal(const int modeID, const int fieldID, const c_double& eval)
{
   _eigVals.at(modeID).at(fieldID) = eval;
}

void DriftWave::setEigVecField(const int modeID, const int fieldID, const c_Vec& evec)
{
  std::copy(evec.begin(),evec.end(),_eigVecs.at(modeID).begin()+fieldID*std::get<0>(_zIndices.at(modeID)));
}

c_double DriftWave::getEigVal(const int modeID, const int fieldID) const
{
   return _eigVals.at(modeID).at(fieldID);
}

void DriftWave::getEigVal(const int modeID, const int fieldID, c_double& eval)
{
   eval = _eigVals.at(modeID).at(fieldID);
}

c_Vec DriftWave::getEigVecField(const int modeID, const int fieldID) const
{
  auto nz = std::get<0>(_zIndices.at(modeID));
  c_Vec evec(nz);
  std::copy(_eigVecs.at(modeID).begin()+fieldID*nz,_eigVecs.at(modeID).begin()+(fieldID+1)*nz,evec.begin());
  return evec;
}

void DriftWave::getEigVecField(const int modeID, const int fieldID, c_Vec& evec)
{
  auto nz = std::get<0>(_zIndices.at(modeID));
  evec.resize(nz);
  std::copy(_eigVecs.at(modeID).begin()+fieldID*nz,_eigVecs.at(modeID).begin()+(fieldID+1)*nz,evec.begin());
}

void DriftWave::setZonalTauCC(const int modeID, const std::tuple<double,c_double,c_double> x) {
  _zonalTauCC.at(modeID) = x;
}

void DriftWave::setZonalTauCC(const int modeID, const double x1, const c_double x2, const c_double x3) {
  _zonalTauCC.at(modeID) = std::make_tuple(x1,x2,x3);
}

void DriftWave::setNonZonalTauCC(const int modeID, const std::tuple<double,c_double,c_double> x) {
  _nonZonalTauCC.at(modeID) = x;
}

void DriftWave::setNonZonalTauCC(const int modeID, const double x1, const c_double x2, const c_double x3) {
  _nonZonalTauCC.at(modeID) = std::make_tuple(x1,x2,x3);
}

void DriftWave::setKPerpTriplet(const int modeID, const std::tuple<int,int,int,int,int,int> x) { 
  _kPerpTriplet.at(modeID) = x;
}

double DriftWave::getZonalTauCC(const int modeID) const { 
  return std::get<0>(_zonalTauCC.at(modeID));
}

c_double DriftWave::getZonalTau(const int modeID) const { 
  return std::get<1>(_zonalTauCC.at(modeID));
}

c_double DriftWave::getZonalCC(const int modeID) const { 
  return std::get<2>(_zonalTauCC.at(modeID));
}

double DriftWave::getNonZonalTauCC(const int modeID) const { 
  return std::get<0>(_nonZonalTauCC.at(modeID));
}

c_double DriftWave::getNonZonalTau(const int modeID) const { 
  return std::get<1>(_nonZonalTauCC.at(modeID));
}

c_double DriftWave::getNonZonalCC(const int modeID) const { 
  return std::get<2>(_nonZonalTauCC.at(modeID));
}

int DriftWave::getMaxNonZonalKx2(const int modeID) const { 
  return std::get<0>(_resonantKNonZonal.at(modeID));
}

int DriftWave::getMaxNonZonalKx3(const int modeID) const { 
  return std::get<1>(_resonantKNonZonal.at(modeID));
}

int DriftWave::getMaxNonZonalKy2(const int modeID) const { 
  return std::get<2>(_resonantKNonZonal.at(modeID));
}

int DriftWave::getMaxNonZonalKy3(const int modeID) const { 
  return std::get<3>(_resonantKNonZonal.at(modeID));
}

int DriftWave::getMaxZonalKx2(const int modeID) const { 
  return _resonantKxZonal.at(modeID);
}

std::tuple<int,int,int> DriftWave::getMaxTriplet(const int modeID) const { 
  return _maxTriplet.at(modeID);
}

std::tuple<int,int,int> DriftWave::getSubModeTriple(const int modeID) const { 
  return _subModeTriple.at(modeID);
}

std::tuple<int,int,int,int,int,int> DriftWave::getKPerpTriplet(const int modeID) const { 
  return _kPerpTriplet.at(modeID);
}

std::tuple<double,c_double,c_double> DriftWave::getZonalTauCCTuple(const int modeID) const {
  return _zonalTauCC.at(modeID);
}

std::tuple<double,c_double,c_double> DriftWave::getNonZonalTauCCTuple(const int modeID) const {
  return _nonZonalTauCC.at(modeID);
}

int DriftWave::kInds(const int ix, const int iy) {
  auto nkx = _satModel->getNkx();
  return (nkx/2+1)*iy + ix;
}

// Multiplication is a convolution over a field line
c_double DriftWave::convolve(const DriftWave& dw1, const int modeID1, const DriftWave& dw2,
                             const int modeID2, const int fieldID, const bool negKx2)
{
  c_double result = 0.0;
  PTSM3D* satModel = dw1.getSatModel();
  auto dw1ZIndices = dw1.getZIndices(modeID1);
  auto dw2ZIndices = dw2.getZIndices(modeID2);
  auto nz1 = std::get<0>(dw1ZIndices); 
  auto nz2 = std::get<0>(dw2ZIndices); 
  auto gZIdx11 = std::get<1>(dw1ZIndices);
  auto gZIdx12 = std::get<2>(dw1ZIndices);
  int gZIdx21, gZIdx22;
  if (negKx2) {
    gZIdx21 = std::get<3>(dw2ZIndices);
    gZIdx22 = std::get<4>(dw2ZIndices);
  } else {
    gZIdx21 = std::get<1>(dw2ZIndices);
    gZIdx22 = std::get<2>(dw2ZIndices);
  }

  if (gZIdx12 <= gZIdx11) throw std::domain_error("gZIdx12 <= gZIdx11!");
  if (gZIdx22 <= gZIdx21) throw std::domain_error("gZIdx22 <= gZIdx21!");

  // Check whether there is an overlap between modes
  if (gZIdx12 <= gZIdx21) return result;
  if (gZIdx11 >= gZIdx22) return result;

  if (dw1.getNFields() != dw2.getNFields()) throw std::domain_error("dw1.nFields != dw2.nFields!");

  auto lowerIdx = std::max(gZIdx11,gZIdx21);
  auto upperIdx = std::min(gZIdx12,gZIdx22);

  c_Vec localDW1 = dw1.getEigVecField(modeID1, fieldID);
  c_Vec localDW2 = dw2.getEigVecField(modeID2, fieldID);

  d_Vec jac = satModel->geom->jac;
  d_Vec modB = satModel->geom->modB;

  if (negKx2) std::reverse(localDW2.begin(),localDW2.end());

  auto zRange = upperIdx - lowerIdx;
  auto z1 = lowerIdx - gZIdx11;
  auto z2 = lowerIdx - gZIdx21;
  double mode1Norm = 0, mode2Norm = 0;
  for (int i = 0; i < zRange; ++i) result += modB.at(lowerIdx+i)*jac.at(lowerIdx+i)*std::conj(localDW1.at(z1+i))*localDW2.at(z2+i);
  for (int i = 0; i < nz1; ++i) mode1Norm += modB.at(gZIdx11+i)*jac.at(gZIdx11+i)*std::real(std::conj(localDW1.at(i))*localDW1.at(i));
  for (int i = 0; i < nz2; ++i) mode2Norm += modB.at(gZIdx21+i)*jac.at(gZIdx21+i)*std::real(std::conj(localDW2.at(i))*localDW2.at(i));
  /*
  if (gZIdx12 > gZIdx21) {
    auto zRange = gZIdx12 - gZIdx21;
    // The first drift wave will have local indices starting at zRange
    // The second drift wave will have local indices ending at zRange
    for (int i = 0; i <= zRange; ++i) result += std::conj(localDW1[nz1-zRange+i])*localDW2[i];
  } else {
    auto zRange = gZIdx22 - gZIdx11;
    for (int i = 0; i <= zRange; ++i) result += std::conj(localDW1[i])*localDW2[nz2-zRange+i];
  }
  */

  return result/(std::sqrt(mode1Norm)*std::sqrt(mode2Norm));
}

c_double DriftWave::convolve(const DriftWave& dw1, const int modeID1, const DriftWave& dw2,
                             const int modeID2, const DriftWave& dw3, const int modeID3,
                             const int fieldID, const bool negKx2, const bool negKx3)
{
  c_double result;
  auto dw1ZIndices = dw1.getZIndices(modeID1);
  auto dw2ZIndices = dw2.getZIndices(modeID2);
  auto dw3ZIndices = dw3.getZIndices(modeID3);
  auto nz1 = std::get<0>(dw1ZIndices); 
  auto nz2 = std::get<0>(dw2ZIndices); 
  auto nz3 = std::get<0>(dw3ZIndices); 
  auto gZIdx11 = std::get<1>(dw1ZIndices);
  auto gZIdx12 = std::get<2>(dw1ZIndices);
  int gZIdx21, gZIdx22, gZIdx31, gZIdx32;
  if (negKx2) {
    gZIdx21 = std::get<3>(dw2ZIndices);
    gZIdx22 = std::get<4>(dw2ZIndices);
  } else {
    gZIdx21 = std::get<1>(dw2ZIndices);
    gZIdx22 = std::get<2>(dw2ZIndices);
  }
  if (negKx3) {
    gZIdx31 = std::get<3>(dw3ZIndices);
    gZIdx32 = std::get<4>(dw3ZIndices);
  } else {
    gZIdx31 = std::get<1>(dw3ZIndices);
    gZIdx32 = std::get<2>(dw3ZIndices);
  }
  if (gZIdx12 <= gZIdx11) throw std::domain_error("gZIdx12 <= gZIdx11!");
  if (gZIdx22 <= gZIdx21) throw std::domain_error("gZIdx22 <= gZIdx21!");
  if (gZIdx32 <= gZIdx31) throw std::domain_error("gZIdx32 <= gZIdx31!");

  if (dw1.getNFields() != dw2.getNFields()) throw std::domain_error("dw1.nFields != dw2.nFields!");
  if (dw1.getNFields() != dw3.getNFields()) throw std::domain_error("dw1.nFields != dw3.nFields!");
  if (dw2.getNFields() != dw3.getNFields()) throw std::domain_error("dw2.nFields != dw3.nFields!");

  // Check whether there is an overlap between modes
  if (gZIdx12 <= gZIdx21) return result;
  if (gZIdx12 <= gZIdx31) return result;
  if (gZIdx22 <= gZIdx31) return result;
  if (gZIdx11 >= gZIdx22) return result;
  if (gZIdx11 >= gZIdx32) return result;
  if (gZIdx21 >= gZIdx32) return result;

  i_Vec lowerIndices{gZIdx11,gZIdx21,gZIdx31};
  i_Vec upperIndices{gZIdx12,gZIdx22,gZIdx32};

  auto lowerIdx = std::max_element(lowerIndices.begin(),lowerIndices.end());
  auto upperIdx = std::min_element(upperIndices.begin(),upperIndices.end());

  c_Vec localDW1 = dw1.getEigVecField(modeID1, fieldID);
  c_Vec localDW2 = dw2.getEigVecField(modeID2, fieldID);
  c_Vec localDW3 = dw3.getEigVecField(modeID3, fieldID);

  if (negKx2) std::reverse(localDW2.begin(),localDW2.end());
  if (negKx3) std::reverse(localDW3.begin(),localDW3.end());


  result = 0.0;
  auto zRange = *upperIdx - *lowerIdx;
  auto z1 = *lowerIdx - gZIdx11;
  auto z2 = *lowerIdx - gZIdx21;
  auto z3 = *lowerIdx - gZIdx31;
  for (int i = 0; i < zRange; ++i) result += std::conj(localDW1.at(z1+i))*localDW2.at(z2+i)*localDW3.at(z3+i);
  return result;
}

DriftWave& DriftWave::operator=(const DriftWave& dw)
{
  if (this != &dw)
  {
    setKxInd(dw.getKxInd());
    setKyInd(dw.getKyInd());
    setNFields(dw.getNFields());
    setNModes(dw.getNModes());
    for (auto i = 0; i<_nModes; ++i) {
      _zIndices.at(i) = dw.getZIndices(i);
      _eigVals.at(i) = dw.getEigVals(i);
      _eigVecs.at(i) = dw.getEigVec(i);
      _massMat.at(i) = dw.getMassMat(i);
      _massMatInv.at(i) = dw.getMassMatInv(i);
    }
  }
  return *this;
}

DriftWave DriftWave::copy(const DriftWave& dw)
{
  DriftWave newDW(dw.getKxInd(),dw.getKyInd(),dw.getNModes(),dw.getNFields(),dw.getSatModel());
  newDW = dw;
  return newDW;
}


c_double DriftWave::fieldlineAverage(const c_Vec& vec, const int modeID, const int fieldID)
{
  c_double vecAvg, norm, iterValue;
  auto evec = getEigVecField(modeID,fieldID);
  auto jac = _satModel->geom->jac;
  auto modB = _satModel->geom->modB;
  if (evec.size() != vec.size()) throw(std::runtime_error("PTSM3D Error! Vector must have same size as eigenvector to average over field line!"));
  vecAvg = 0;
  norm = 0;
  auto gZIdx1 = std::get<1>(_zIndices.at(modeID));
  auto nz = std::get<0>(_zIndices.at(modeID));
  for (auto i=0; i<nz; ++i) {
    iterValue = modB[gZIdx1+i]*jac[gZIdx1+i]*evec[i]*std::conj(evec[i]);
    vecAvg += iterValue*vec[i];
    norm += iterValue;
  }
  return vecAvg/norm;
}

c_double DriftWave::fieldlineAverage(const d_Vec& vec, const int modeID, const int fieldID)
{
  c_double vecAvg, norm, iterValue;
  auto evec = getEigVecField(modeID,fieldID);
  auto jac = _satModel->geom->jac;
  auto modB = _satModel->geom->modB;
  if (evec.size() != vec.size()) throw(std::runtime_error("PTSM3D Error! Vector must have same size as eigenvector to average over field line!"));
  vecAvg = 0;
  norm = 0;
  auto gZIdx1 = std::get<1>(_zIndices.at(modeID));
  auto nz = std::get<0>(_zIndices.at(modeID));
  for (auto i=0; i<nz; ++i) {
    iterValue = modB[gZIdx1+i]*jac[gZIdx1+i]*evec[i]*std::conj(evec[i]);
    vecAvg += iterValue*vec[i];
    norm += iterValue;
  }
  return vecAvg/norm;
}

void DriftWave::computeAvgBkDk(const int modeID)
{
  auto phi = getEigVecField(modeID,0);
  auto jac = _satModel->geom->jac;
  auto modB = _satModel->geom->modB;
  auto kxMap = _satModel->getKxMap();
  auto kyMap = _satModel->getKyMap();
  d_Vec Bk(phi.size()),Dk(phi.size());
  _satModel->geom->buildBk(kxMap.at(_kxInd),kyMap.at(_kyInd),std::get<1>(_zIndices.at(modeID)),std::get<2>(_zIndices.at(modeID)),Bk);
  _satModel->geom->buildDk(kxMap.at(_kxInd),kyMap.at(_kyInd),std::get<1>(_zIndices.at(modeID)),std::get<2>(_zIndices.at(modeID)),Dk);
  double Bk_Avg = 0;
  double Dk_Avg = 0;
  double norm = 0;
  double normIter = 0;
  auto gZIdx1 = std::get<1>(_zIndices.at(modeID));
  auto nz = std::get<0>(_zIndices.at(modeID));
  for (auto i=0; i<phi.size(); ++i) {
    normIter = std::real(modB[gZIdx1+i]*jac[gZIdx1+i]*phi[i]*std::conj(phi[i])); 
    Dk_Avg += normIter*Dk[i];
    Bk_Avg += normIter*Bk[i];
    norm += normIter;
  }
  _avgBkDk.at(modeID) = std::make_tuple(Bk_Avg/norm,Dk_Avg/norm);
}

void DriftWave::mirrorOverZero(const int modeID)
{
  auto nz = std::get<0>(_zIndices.at(modeID));
  for (auto i = 0; i < _nFields; ++i) std::reverse(_eigVecs.at(modeID).begin()+nz*i,_eigVecs.at(modeID).begin()+nz*(i+1));
}

void DriftWave::computeCrossBk(const int modeID) {
  auto nz = std::get<0>(_zIndices.at(modeID));
  d_Vec Bk(nz);
  _crossBk.resize(_satModel->getNkx()*_satModel->getNky());
  auto kxMap = _satModel->getKxMap();
  auto kyMap = _satModel->getKyMap();
  auto kx = kxMap[_kxInd];
  auto ky = kyMap[_kyInd];
  for (auto k = 0; k < _satModel->getNky(); ++k) {
    for (auto j = 0; j < _satModel->getNkx(); ++j) {
      _satModel->geom->buildCrossBk(kx,ky,kxMap[j],kyMap[k],std::get<1>(_zIndices.at(modeID)),std::get<2>(_zIndices.at(modeID)),Bk);
      _crossBk[kInds(j,k)] = fieldlineAverage(Bk,modeID,0);
    }
  } 
}

void DriftWave::computeZonalCoupling(DriftWave& dw1, const DriftWave& dw2, const int kxIdx3, const bool negKx2) {

  PTSM3D* ptsm3dPtr = dw1.getSatModel();
  auto kxMap = ptsm3dPtr->getKxMap();
  auto kyMap = ptsm3dPtr->getKyMap();
  auto kxEnergySpec = ptsm3dPtr->getKxEnergySpec();
  auto kyEnergySpec = ptsm3dPtr->getKyEnergySpec();
  auto nkx = ptsm3dPtr->getNkx();

  const int kxIdx1 = dw1.getKxInd();
  const int kyIdx1 = dw1.getKyInd();
  const int kxIdx2 = dw2.getKxInd();
  const double kx1 = kxMap[kxIdx1];
  const double ky1 = kyMap[kyIdx1];
  const double kx2 = !negKx2 ? kxMap[kxIdx2] : -kxMap[kxIdx2];

  int mode_1, mode_2;
  c_double overlap;
  for (mode_1=0; mode_1<dw1.getNModes(); ++mode_1) {
    c_Vec zTau;
    c_Vec zCC;
    d_Vec zTauCC;
    i_Vec zkx2Vec;


    for (auto mode_2 = 0; mode_2 < dw2.getNModes(); ++mode_2) {
      if (std::imag(dw1.getEigVal(mode_1,0))*std::imag(dw2.getEigVal(mode_2,0)) > 0) {
        overlap = convolve(dw1,mode_1,dw2,mode_2,0,negKx2);
        zCC.emplace_back(ky1*(kx1-kx2)*
            (dw1.getMassMatInvEntry(mode_1,0,1)*dw2.getMassMatEntry(mode_2,1,1) +
             dw1.getMassMatInvEntry(mode_1,0,2)*dw2.getMassMatEntry(mode_2,2,1)));
        zTau.emplace_back(-PETSC_i/(dw2.getEigVal(mode_2,1) - std::conj(dw1.getEigVal(mode_1,0))));
        zTauCC.emplace_back(std::abs(overlap*zTau.back()*zCC.back())*
            kxEnergySpec[kxIdx1]*kxEnergySpec[kxIdx2]*kxEnergySpec[kxIdx3]*kxEnergySpec[kxIdx3]*
            kyEnergySpec[kyIdx1]*kyEnergySpec[kyIdx1]);
        zkx2Vec.emplace_back(kxIdx2);
      }
    }

    auto maxIter = std::max_element(zTauCC.begin(),zTauCC.end());
    auto maxIdx = std::distance(zTauCC.begin(),std::find(zTauCC.begin(),zTauCC.end(),*maxIter));
    if (maxIter != zTauCC.end()) {
      if (*maxIter > dw1.getZonalTauCC(mode_1)) {
        dw1.setZonalTauCC(mode_1,zTauCC[maxIdx],zTau[maxIdx],zCC[maxIdx]);
        dw1.setResonantKxZonal(mode_1,zkx2Vec[maxIdx]);
      }
    } else {
      if (dw1.getZonalTauCC(mode_1) == 0.0) {
        dw1.setZonalTauCC(mode_1,0.0,0.0,0.0);
        dw1.setResonantKxZonal(mode_1,-1);
      }
    }
  }
}

void DriftWave::computeNonZonalCoupling(DriftWave& dw1, const DriftWave& dw2, const DriftWave& dw3,
    const std::tuple<bool,bool,bool,bool> negKSign) {

  PTSM3D* ptsm3dPtr = dw1.getSatModel();
  auto kxMap = ptsm3dPtr->getKxMap();
  auto kyMap = ptsm3dPtr->getKyMap();
  auto kxEnergySpec = ptsm3dPtr->getKxEnergySpec();
  auto kyEnergySpec = ptsm3dPtr->getKyEnergySpec();
  auto nkx = ptsm3dPtr->getNkx();

  const int kxIdx1 = dw1.getKxInd();
  const int kyIdx1 = dw1.getKyInd();
  const int kxIdx2 = dw2.getKxInd();
  const int kyIdx2 = dw2.getKyInd();
  const int kxIdx3 = dw3.getKxInd();
  const int kyIdx3 = dw3.getKyInd();
  const double kx1 = kxMap[kxIdx1];
  const double ky1 = kyMap[kyIdx1];
  const double kx2 = !std::get<0>(negKSign) ? kxMap[kxIdx2] : -kxMap[kxIdx2];
  const double ky2 = !std::get<1>(negKSign) ? kyMap[kyIdx2] : -kyMap[kyIdx2];
  const double kx3 = !std::get<2>(negKSign) ? kxMap[kxIdx3] : -kxMap[kxIdx3];
  const double ky3 = !std::get<3>(negKSign) ? kyMap[kyIdx3] : -kyMap[kyIdx3];
  const std::tuple<int,int,int,int,int,int> kPerpTriplet = std::make_tuple(kxIdx1,kyIdx1,!std::get<0>(negKSign) ? kxIdx2 : -kxIdx2,!std::get<1>(negKSign) ? kyIdx2 : kyIdx2,!std::get<2>(negKSign) ? kxIdx3 : -kxIdx3,!std::get<3>(negKSign) ? kyIdx3 : -kyIdx3);

  /*
  DebugMsg(std::cout << "Computing coupling with k = (" << kxIdx1 << "," << kyIdx1 << ")";);
  DebugMsg(std::cout << ", k' = (" << kxIdx2 << "," << kyIdx2 << ")";);
  DebugMsg(std::cout << ", k'' = (" << kxIdx3 << "," << kyIdx3 << ")";);
  DebugMsg(std::cout << '\n';);
  */
  int mode_1, mode_2, mode_3;
  for (mode_1=0; mode_1<dw1.getNModes(); ++mode_1) {
    c_Vec nzTau;
    c_Vec nzCC;
    d_Vec nzTauCC;
    i_Vec kx2Vec;
    i_Vec ky2Vec;
    i_Vec kx3Vec;
    i_Vec ky3Vec;
    i_Vec mode2Vec;
    i_Vec mode3Vec;
    std::vector<std::tuple<int,int,int>> subModeVec;
    std::vector<std::tuple<int,int,int>> maxTriplet;


    for (auto mode_2 = 0; mode_2 < dw2.getNModes(); ++mode_2) {
      for (auto mode_3 = 0; mode_3 < dw3.getNModes(); ++mode_3) {
        if (std::imag(dw1.getEigVal(mode_1,0))*std::imag(dw2.getEigVal(mode_2,0))*std::imag(dw3.getEigVal(mode_3,0)) > 3*machEps) {
          for (auto& triplet : dw1.triplets()) {
              auto x = std::get<0>(triplet);
              auto y = std::get<1>(triplet);
              auto z = std::get<2>(triplet);

              auto overlap12 = convolve(dw1,mode_1,dw2,mode_2,0,std::get<0>(negKSign));
              auto overlap13 = convolve(dw1,mode_1,dw3,mode_3,0,std::get<2>(negKSign));
              nzCC.emplace_back((kx1*ky2-ky1*kx2)*
                (dw1.getMassMatInvEntry(mode_1,x,1)*dw3.getMassMatEntry(mode_3,0,z)*dw2.getMassMatEntry(mode_2,1,y) +
                0.5*dw1.getMassMatInvEntry(mode_1,x,2)*dw3.getMassMatEntry(mode_3,0,z)*dw2.getMassMatEntry(mode_2,2,y)));
              nzTau.emplace_back(-PETSC_i*1.0/(dw3.getEigVal(mode_3,z) + dw2.getEigVal(mode_2,y) - std::conj(dw1.getEigVal(mode_1,x))));
              nzTauCC.emplace_back(std::abs(overlap12 * overlap13 * nzTau.back() * nzCC.back())*
                  kxEnergySpec[kxIdx1]*kxEnergySpec[kxIdx2]*kxEnergySpec[kxIdx3]*
                  kyEnergySpec[kyIdx1]*kyEnergySpec[kyIdx2]*kyEnergySpec[kyIdx3]);
              kx2Vec.emplace_back(kxIdx2);
              ky2Vec.emplace_back(kyIdx2);
              kx3Vec.emplace_back(kxIdx3);
              ky3Vec.emplace_back(kyIdx3);
              maxTriplet.emplace_back(triplet);
              subModeVec.emplace_back(std::make_tuple(mode_1,mode_2,mode_3));
              //DebugMsg(std::cout << nzTau.back() << '\t' << nzCC.back() << '\t' << nzTauCC.back() << '\n';);
          }
        }
      }
    }
    

    auto maxIter = std::max_element(nzTauCC.begin(),nzTauCC.end());
    if (maxIter != nzTauCC.end()) {
      auto maxIdx = std::distance(nzTauCC.begin(),std::find(nzTauCC.begin(),nzTauCC.end(),*maxIter));
      if (*maxIter > dw1.getNonZonalTauCC(mode_1)) {
        dw1.setNonZonalTauCC(mode_1,nzTauCC[maxIdx],nzTau[maxIdx],nzCC[maxIdx]);
        dw1.setMaxTriplet(mode_1,maxTriplet[maxIdx]);
        dw1.setResonantKNonZonal(mode_1,kx2Vec[maxIdx],ky2Vec[maxIdx],kx3Vec[maxIdx],ky3Vec[maxIdx]);
        dw1.setSubModeTriple(mode_1,subModeVec[maxIdx]);
        dw1.setKPerpTriplet(mode_1,kPerpTriplet);
      }
    } else {
      if (dw1.getNonZonalTauCC(mode_1) == 0.0) {
        dw1.setNonZonalTauCC(mode_1,0.0,0.0,0.0);
        dw1.setMaxTriplet(mode_1,std::make_tuple(-1,-1,-1));
        dw1.setResonantKNonZonal(mode_1,-1,-1,-1,-1);
        dw1.setSubModeTriple(mode_1,std::make_tuple(-1,-1,-1));
        dw1.setKPerpTriplet(mode_1,std::make_tuple(0,0,0,0,0,0));
      }
    }
  }
}
