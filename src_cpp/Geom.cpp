#include <iostream>
#include <vector>
#include <cmath>
#include <functional>

#include "Definitions.hpp"
#include "PTSM3D.hpp"
#include "Geom.hpp"
#include "Utils.hpp"
#include "Interfaces.hpp"

Geom::Geom() {}

void Geom::setGeomSize(const int nz)
{
  nPts = (nz % 2 == 1) ? nz : nz+1;
/*
  if (nz % 2 == 1)
  {
    setNPts(nz);
  } else {
    setNPts(nz);
  }
*/
  gxx.resize(nPts);
  gxy.resize(nPts);
  gyy.resize(nPts);
  modB.resize(nPts);
  jac.resize(nPts);
  dBdx.resize(nPts);
  dBdy.resize(nPts);
  dBdz.resize(nPts);
  eta.resize(nPts);
  modBInv.resize(nPts);
  jacBInv.resize(nPts);
  dparModBInv.resize(nPts);
  dpar2ModBInv.resize(nPts);
  dparJacBInv.resize(nPts);
}

void Geom::s_alpha(const double q0, const double shat, const double invEps, const double np)
{
  // Const definitions
  const auto nz = getNPts();
  const auto pi = Utils::pi();
  setq0(q0);
  setShat(shat);
  setInvEps(invEps);
  setNfp(1);

  int nz2, nz0;
  double dz, lz;
  double sinz, cosz, psz;

  nz2 = (nz-1)/2;
  nz0 = (nz-1)/(int)np;
  dz = 2.0/((double)nz0);
  setDEta(dz);
  
  for (int i=0; i<nz; ++i)
  {
    lz = (i-nz2)*dz; 
    psz = pi*shat*lz;
    sinz = std::sin(pi*lz);
    cosz = std::cos(pi*lz); 
    eta[i] = lz;
    gxx[i] = 1.0;
    gxy[i] = psz;
    gyy[i] = 1.0 + (psz)*(psz);
    modB[i] = 1.0/(1.0 + invEps*cosz);
    jac[i] = q0*(1.0 + invEps*cosz);
    //dBdy[i] = -(cosz + psz*sinz)/((1.0+invEps*cosz)*(1.0+invEps*cosz));
    //dBdx[i] = -sinz/((1.0+invEps*cosz)*(1.0+invEps*cosz));
    dBdy[i] = -(cosz + psz*sinz);
    dBdx[i] = -sinz;
    dBdz[i] = invEps*sinz/((1.0+invEps*cosz)*(1.0+invEps*cosz)); 
    modBInv[i] = 1.0/modB[i];
    jacBInv[i] = 1.0/(jac[i]*modB[i]);
  }
}

void Geom::buildBk(const double kx, const double ky, const int z1, const int z2, d_Vec& Bk)
{
  int i, j;
  for (i=z1; i<=z2; ++i)
  {
    j = i-z1;
    Bk[j] = ((kx*kx*(gxx[i]) + 2*kx*ky*(gxy[i]) + ky*ky*(gyy[i]))/(modB[i]*modB[i]));
  }
}

void Geom::buildDk(const double kx, const double ky, const int z1, const int z2, d_Vec& Dk)
{
  int i, j;
  for (i=z1; i<=z2; ++i)
  {
    j = i-z1;
    Dk[j] = 2*(kx*(dBdx[i]) + ky*(dBdy[i]))/modB[i];
  }
}

void Geom::buildCrossBk(const double kx1, const double ky1, const double kx2, const double ky2, const int z1, const int z2, d_Vec& Bkpk)
{
  for (auto i=z1; i<=z2; ++i)
  {
    auto j = i-z1;
    Bkpk[j] = ((kx1*kx2*(gxx[i]) + (kx2*ky1+kx1*ky2)*(gxy[i]) + ky1*ky2*(gyy[i]))/(modB[i]*modB[i]));
  }
}

void Geom::computeDerivedQuantities(const int errOrder)
{
  std::vector<double> FDSpline1, FDSpline2;
  const int sBound = errOrder/2;
  const auto dz = getDEta();
  double modBInvSum, modBInvSum2, jacBInvSum;

  FDSpline1.resize(errOrder+1);
  FDSpline2.resize(errOrder+1);
  
  for (auto i=0; i<nPts; i++)
  {
    modBInv[i] = 1.0/(modB[i]);
    jacBInv[i] = 1.0/(jac[i]*modB[i]);
  }
  
  for (auto i=0; i<nPts; i++)
  {
    modBInvSum = 0;
    modBInvSum2 = 0;
    jacBInvSum = 0;
    if(i < sBound) {
      Utils::FDCoeffGen(-sBound+std::abs(sBound-i),errOrder-i,1,dz,FDSpline1);
      Utils::FDCoeffGen(-sBound+std::abs(sBound-i),errOrder-i,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = i-sBound+std::abs(sBound-i)+j;
        modBInvSum += FDSpline1[j]*modBInv[idx];
        modBInvSum2 += FDSpline2[j]*modBInv[idx];
        jacBInvSum += FDSpline1[j]*jacBInv[idx];
      }
    } else if (i >= nPts-sBound) {
      Utils::FDCoeffGen(nPts-1-i-errOrder,nPts-1-i,1,dz,FDSpline1);
      Utils::FDCoeffGen(nPts-1-i-errOrder,nPts-1-i,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = nPts-errOrder+j-1;
        modBInvSum += FDSpline1[j]*modBInv[idx];
        modBInvSum2 += FDSpline2[j]*modBInv[idx];
        jacBInvSum += FDSpline1[j]*jacBInv[idx];
      }
    } else {
      Utils::FDCoeffGen(-sBound,sBound,1,dz,FDSpline1);
      Utils::FDCoeffGen(-sBound,sBound,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = i-sBound+j;
        modBInvSum += FDSpline1[j]*modBInv[idx];
        modBInvSum2 += FDSpline2[j]*modBInv[idx];
        jacBInvSum += FDSpline1[j]*jacBInv[idx];
      }
    }

    dparModBInv[i] = modBInvSum;
    dpar2ModBInv[i] = modBInvSum2;
    dparJacBInv[i] = jacBInvSum;
  
  }
}

int Geom::findZeroIndex()
{
  int zeroIndex = -1;
  auto dz = getDEta();
  for (auto i=0; std::abs(eta[i]) > dz; ++i) {
    zeroIndex = i;
  }
  return zeroIndex;
}

/*
void Geom::callVmec2Pest(std::string eqFile, double surface, const double dky,
                         const int nz0, const int buffer)
{
  char *diag_name;
  std::string diag_str;
  int x1, x2, data_size, n_dims;
  double* c_data;

  Interfaces::v2p_opts vmec2pest_options;
  char *file = &eqFile[0u];
  char gridType[] = "gene";
  char x3Coord[] = "theta";
  char normType[] = "minor_r";
  vmec2pest_options.vmec_file = file;
  vmec2pest_options.grid_type = gridType;
  vmec2pest_options.x3_coord = x3Coord;
  vmec2pest_options.norm_type = normType;

  vmec2pest_options.x1 = &surface;
  vmec2pest_options.nx1 = 1;
  vmec2pest_options.nx2 = 1;
  vmec2pest_options.nx3 = 1;
  
  vmec2pest_options.nfpi = 1.0;
  vmec2pest_options.x3_center = 0.0;
  DebugMsg(std::cout << "Calling vmec2pest_c_interface" << '\n';);
  vmec2pest_c_interface(&vmec2pest_options);
  
  diag_str = "nfp";
  diag_name = &diag_str[0u];
  x1 = 0;
  x2 = 0;
  n_dims = -1;
  data_size = 1;
  c_data = new double[data_size];
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  setNfp((int)c_data[0]);

  diag_str = "shat";
  diag_name = &diag_str[0u];
  n_dims = 0;
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  setShat(c_data[0]);

  diag_str = "safety_factor_q";
  diag_name = &diag_str[0u];
  n_dims = 0;
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  setq0(c_data[0]);

  delete[] c_data;

  //Compute the maximum distance needed to compute along a field line 
  auto max_z = 1.0/(std::abs(shat)*dky);
  data_size = (std::ceil(max_z/Utils::pi()) + buffer)*nfp*nz0;
  vmec2pest_options.nx3 = data_size;
  vmec2pest_options.nfpi = (double)((std::ceil(max_z/Utils::pi())+buffer)*nfp);
  data_size += 1;
  vmec2pest_c_interface(&vmec2pest_options);

  v2p_DiagMap["g11"] = &Geom::setGxx;
  v2p_DiagMap["g12"] = &Geom::setGxy;
  v2p_DiagMap["g22"] = &Geom::setGyy;
  v2p_DiagMap["bmag"] = &Geom::setModB;
  v2p_DiagMap["jac"] = &Geom::setJac;
  v2p_DiagMap["curv_drift_x2"] = &Geom::setDBdx;
  v2p_DiagMap["curv_drift_x1"] = &Geom::setDBdy;
  v2p_DiagMap["d_B_d_x3"] = &Geom::setDBdz;
  v2p_DiagMap["x3"] = &Geom::setEta;

  d_Vec temp;
  n_dims = 1; 
  setGeomSize(data_size);
  temp.resize(data_size+1);
  c_data = new double[data_size+1];
  for (std::unordered_map<std::string,geomFuncPtr>::iterator iter=v2p_DiagMap.begin(); iter != v2p_DiagMap.end(); ++iter)
  {
    diag_str = iter->first;
    diag_name = &diag_str[0u];
    get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
    //for (auto i = 0; i < data_size; ++i) temp[i] = c_data[i];
    temp.assign(c_data,c_data+data_size+1);
    setGeomField(temp,iter->second);
  }
  delete[] c_data;
  setDEta((eta[1]-eta[0])/Utils::pi());
}
*/
