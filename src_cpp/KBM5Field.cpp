// STL includes
#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include <string>
#include <algorithm>
#include <utility>

// External includes
//#include "petsc.h"

// Local includes
#include "Definitions.hpp"
#include "KBM5Field.hpp"
#include "Geom.hpp"
#include "SlepcUtils.hpp"
#include "Utils.hpp"
#include "FluidModel.hpp"

/*
*/
KBM5Field::KBM5Field() : FluidModel{} {}

KBM5Field::KBM5Field(const int n_fields) : FluidModel{n_fields} {}

KBM5Field::~KBM5Field() {}

void KBM5Field::createLinearOperators(SlepcUtils::SlepcContext& sc)
{
  if (sc.probType == "eps") {
    // Set up the PETSc matrices for the EPS operators
    // For the 5-field system of equations discretized by N points
    // in the parallel direction, there are 5*N number of points in
    // the total system
    sc.nOps = 2;
    sc.opDims = sc.nFields*probSize;
  }
 
  // Allocate the pointer array for linOpArr
  linOpArr = new Mat[sc.nOps];
  // Setup the PETSc matrices for the linear operators
  for (int i=0; i<sc.nOps; i++)
  {
    MatCreate(PETSC_COMM_WORLD,&linOpArr[i]);
    MatSetSizes(linOpArr[i],PETSC_DECIDE,PETSC_DECIDE,sc.opDims,sc.opDims);
    MatSetUp(linOpArr[i]);
  }
  // Create the eigenvector with the same parallel layout as the linOpArrs
  MatCreateVecs(linOpArr[0],&initCondVec,NULL);
  petscVecs = new Vec[sc.nFields];
}

void KBM5Field::destroyLinearOperators(const SlepcUtils::SlepcContext& sc)
{
  // Destroy each linOpArr
  for (int i=0; i<sc.nOps; i++)
  {
    MatDestroy(&linOpArr[i]);
  }
  // Release the linOpArr pointer
  delete[] linOpArr;
  // Destory the PETSc initial condition vector
  VecDestroy(&initCondVec);
  delete[] petscVecs;
}

void KBM5Field::zeroLinearOperators(const SlepcUtils::SlepcContext& sc, const bool setVecToZero)
{
  for (auto i=0; i<sc.nOps; ++i) MatZeroEntries(linOpArr[i]);
  if (setVecToZero) {
    for (auto i=0; i<sc.nFields; ++i) VecSet(petscVecs[i],0);
    VecSet(initCondVec,0);
  }
}

void KBM5Field::buildLinearOperators(SlepcUtils::SlepcContext& sc,
                                     const double kx, const double ky)
{
  DebugMsg(std::cout << "Building EPS operators\n";);
  if (sc.probType == "eps") {
    this->buildEPSOperators(kx,ky);
  }
}

void KBM5Field::extractSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky)
{
  if (sc.probType == "eps") {
    this->extractEPSSolution(sc,kx,ky);
  }
}

/******************************************************************************
*                                                                             *
* Routines to solve the eigenvalue problem using the SLEPc EPS functions      *
*                                                                             *
******************************************************************************/

/*
  Solving the generalized eigenvalue problme A*x = lambda B*x for the eigenvector
  x = (n, phi, v_parallel, T, A_parallel)
  The EPS problem has operators laid out in block matrix form corresponding
  to the different fields.  The left-hand-side operator has the form

       n     phi   v     T     A_par
      | A0  | A1  | A2  | A3  | A4  | N rows 
  A = | A5  | A6  | A7  | A8  | A9  | N rows
      | A10 | A11 | A12 | A13 | A14 | N rows
      | A15 | A16 | A17 | A18 | A19 | N rows
      | A20 | A21 | A22 | A23 | A24 | N rows
 
  while the right hand side operator has the form

      | B0 | B1 | 0  | B2 | 0  | N rows
  B = | B3 | B4 | 0  | B5 | 0  | N rows
      | 0  | 0  | 1  | 0  | 0  | N rows
      | 0  | 0  | 0  | 1  | 0  | N rows
      | 0  | 0  | 0  | 0  | 1  | N rows
*/
void KBM5Field::buildEPSOperators(const double kx, const double ky)
{
  // Const definitions
  const PetscScalar piInv = 1.0/Utils::pi();
  const PetscReal dz = geomPtr->getDEta();
  const PetscInt one = 1;


  // PetscInt pointers for the array column indices used in SetMatValues
  PetscInt *colInds, *bcolInds, *colHypzInds, *bcolHypzInds;

  // PetscScalar pointers to the data structures used in SetMatValues
  PetscScalar *pdc, *pdc1, *pdsc, *bpdc, *bpdc1, *hypzc, *bhypzc, *ihypzc;

  // Integers controlling array bounds/indices
  PetscInt i, j, k, globalIdx, nHypzPts, nSplinePts, li1, li2, sBound, hypzBound, blockRow;
 
  // Internal variables
  PetscInt pdOrder;
  PetscScalar hypz, ijacBInv, iDk, iBk, igamma;

  // Vectors containing the finite difference spline coefficients
  c_Vec fdSpline, bfdSpline, hypzSpline;
  // Vectors containing the geometric factors Bk and Dk
  if (probSize != (zBoundUpper - zBoundLower + 1)) throw(std::runtime_error("PTSM3D Error! Vectors have incompatible sizes!"));
  d_Vec Bk(probSize), Dk(probSize);

  // Build the Bk and Dk vectors
  // TODO(bfaber): Compute the integer offset necessary for finite theta-k
  geomPtr->buildBk(kx,ky,zBoundLower,zBoundUpper,Bk);
  geomPtr->buildDk(kx,ky,zBoundLower,zBoundUpper,Dk);


  // TODO(bfaber): Move these constant definitions to read from input file
  pdOrder = 1;
  sBound = errOrder/2;
  hypzBound = hypzOrder/2;

  // Set the appropriate sizes for the vectors and resize
  nSplinePts = errOrder+1;
  nHypzPts = hypzOrder+1;
  fdSpline.resize(nSplinePts);
  hypzSpline.resize(nHypzPts);
  
  // Allocate memory for the Petsc pointers 
  colHypzInds = new PetscInt[nHypzPts];
  bcolHypzInds = new PetscInt[hypzOrder];
  colInds = new PetscInt[nSplinePts];
  bcolInds = new PetscInt[errOrder];
  pdc = new PetscScalar[nSplinePts];
  pdc1 = new PetscScalar[nSplinePts];
  pdsc = new PetscScalar[nSplinePts];
  bpdc = new PetscScalar[errOrder];
  bpdc1 = new PetscScalar[errOrder];
  hypzc = new PetscScalar[nHypzPts];
  bhypzc = new PetscScalar[hypzOrder];
  ihypzc = new PetscScalar[nHypzPts];
  
  // Compute the parallel hyperdiffusion coefficient 
  hypz = -(std::pow(PETSC_i,hypzOrder))*hypzEps*std::pow(0.5*dz,hypzOrder);

  // For the non-boundary matrix rows, no need to recompute the finite difference
  // spline, compute once here and reuse in the later loop 
  SlepcUtils::FDCoeffGen(-sBound,sBound,pdOrder,dz,fdSpline);
  SlepcUtils::FDCoeffGen(-hypzBound,hypzBound,hypzOrder,dz,hypzSpline);
  
  for (j=0; j<=errOrder; ++j)
  {
    pdsc[j] = fdSpline[j];
  }

  for (j=0; j<=hypzOrder; ++j)
  {
    ihypzc[j] = hypz*hypzSpline[j];
  }

  // Necessary for parallel eigenvalue calculations
  MatGetOwnershipRange(linOpArr[0],&li1,&li2);

  for (k=li1; k<li2; ++k) // Start global index loop
  {
    // Calculate the block index i for row k
    i = k % probSize;
    blockRow = k/probSize;
    globalIdx = zBoundLower+i;

    // Calculate these quantities once rather than reading from array each time
    ijacBInv = geomPtr->jacBInv[globalIdx];
    igamma = -piInv*ijacBInv;
    iDk = Dk[i];
    iBk = Bk[i];
   
    // Compute the entries for the right-hand-side operator
    // B0, B1, B2, B3, B4, B5, and B6 and the diagonal entries
    if (blockRow == 0)
    {
      MatSetValue(linOpArr[1],i,i,1.0+iBk*5.0/(3.0*tauTemp),ADD_VALUES);
      MatSetValue(linOpArr[1],i,probSize+i,iBk,ADD_VALUES);
      MatSetValue(linOpArr[1],i,3*probSize+i,iBk,ADD_VALUES);
    } else if (blockRow == 1) {
      MatSetValue(linOpArr[1],probSize+i,i,beta*iBk*5.0/(6.0*tauTemp),ADD_VALUES);
      MatSetValue(linOpArr[1],probSize+i,probSize+i,beta*iBk/2.0,ADD_VALUES);
      MatSetValue(linOpArr[1],probSize+i,3*probSize+i,beta*iBk/2.0,ADD_VALUES);
    }
    else
    {
      MatSetValue(linOpArr[1],k,k,1.0,ADD_VALUES);
    } 
    

    // Fill in the hyperdiffusions
    // This fills in the diagonal bands in blocks A0, A5 and A8
    if (i < hypzBound)
    {
      SlepcUtils::FDCoeffGen(-hypzBound+std::abs(hypzBound-i)-1,hypzOrder-i-1,hypzOrder,dz,hypzSpline);
      for (j=0; j<hypzOrder; ++j)
      {
        bcolHypzInds[j] = blockRow*probSize + i-hypzBound+std::abs(hypzBound-i)+j;
        bhypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*hypz*hypzSpline[j+1];
      }
      MatSetValues(linOpArr[0],one,&k,hypzOrder,bcolHypzInds,bhypzc,ADD_VALUES); 
    }
    else if (i >= probSize-hypzBound)
    {
      SlepcUtils::FDCoeffGen(probSize-1-i-hypzOrder,probSize-1-i,hypzOrder,dz,hypzSpline);
      for (j=0; j<hypzOrder; ++j)
      {
        bcolHypzInds[j] = blockRow*probSize + probSize-hypzOrder+j;
        bhypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*hypz*hypzSpline[j];
      }
      MatSetValues(linOpArr[0],one,&k,hypzOrder,bcolHypzInds,bhypzc,ADD_VALUES); 
    }
    else
    {
      for (j=0; j<=hypzOrder; ++j)
      {
        colHypzInds[j] = blockRow*probSize + i + j - hypzBound;
        hypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*ihypzc[j];
      }
      MatSetValues(linOpArr[0],one,&k,nHypzPts,colHypzInds,hypzc,ADD_VALUES);
    }


    // Fill linear operator
    // Deal with the boundary terms first
    if (i < sBound)
    {
      SlepcUtils::FDCoeffGen(-sBound+std::abs(sBound-i)-1,errOrder-i-1,pdOrder,dz,fdSpline);
      // Assign the entries for blocks A0, A1, A2, A3, and A4 (=0)
      if (blockRow == 0)
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = 2*probSize + i-sBound+std::abs(sBound-i)+j;
          bpdc[j] = -PETSC_i*igamma*fdSpline[j+1];
        } 
        // Block A2
        MatSetValues(linOpArr[0],one,&i,errOrder,bcolInds,bpdc,ADD_VALUES);
        // Block A0
        MatSetValue(linOpArr[0],i,i,iDk*5.0/(3.0*tauTemp),ADD_VALUES);
        // Block A1
        MatSetValue(linOpArr[0],i,probSize+i,ky*omn+iDk,ADD_VALUES);
	// Block A3
	MatSetValue(linOpArr[0],i,3*probSize+i,iDk,ADD_VALUES);
      }
      else if (blockRow == 1) // Assign the entries for blocks A5, A6, A7, A8, and A9
      {
	      c_double dBk_sum = 0.0;
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = 4*probSize + i-sBound+std::abs(sBound-i)+j;
          bpdc1[j] = -PETSC_i*igamma*iBk*fdSpline[j+1];
	        dBk_sum += -PETSC_i*igamma*fdSpline[j+1]*Bk[i];
        } 
        // Block A9
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc1,ADD_VALUES);
        MatSetValue(linOpArr[0],probSize+i,4*probSize+i,dBk_sum,ADD_VALUES);
        // Block A5
        MatSetValue(linOpArr[0],probSize+i,i,beta*iDk*(5.0/(3.0*tauTemp)-1.0)/2.0,ADD_VALUES);
	// Block A8
        MatSetValue(linOpArr[0],probSize+i,3*probSize+i,beta*iDk/2.0,ADD_VALUES); 
      }
      else if (blockRow == 2) // Assign the entries for blocks A10, A11, A12, A13, and A14
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = i-sBound+std::abs(sBound-i)+j;
          bpdc[j] = -PETSC_i*igamma*tauTemp*fdSpline[j+1];
          bpdc1[j] = -PETSC_i*igamma*(5.0/(3.0*tauTemp)+1.0)*fdSpline[j+1];
        } 
        // Block A10
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc1,ADD_VALUES);       
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = 3*probSize + i-sBound+std::abs(sBound-i)+j;
        } 
        // Block A13
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);
	// Fixing Final Column
	MatSetValue(linOpArr[0],2*probSize+i,5*probSize-1,0.0,ADD_VALUES);
	// Block A14
        MatSetValue(linOpArr[0],2*probSize+i,4*probSize+i,0.0-ky*(omn*(1.0+1.0/tauTemp)+omt/tauTemp),ADD_VALUES);
      }
      else if (blockRow == 3) // Assign the entries for blocks A15, A16, A17, A18, and A19
      {
	// Block A16
        MatSetValue(linOpArr[0],3*probSize+i,probSize+i,ky/tauTemp*(omt-2.0*omn/3.0),ADD_VALUES);
	// Block A17
	// MatSetValue(linOpArr[0],3*probSize+i,3*probSize+i,iDk*5.0/(3.0*tauTemp),ADD_VALUES);
      }
      else if (blockRow == 4) // Assign the entries for blocks A20, A21, A22, A23, and A24
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = i-sBound+std::abs(sBound-i)+j;
          bpdc[j] = PETSC_i*igamma*fdSpline[j+1];
        } 
        // Block A20
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);	
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = probSize+i-sBound+std::abs(sBound-i)+j;
          bpdc[j] = -PETSC_i*igamma*fdSpline[j+1];
        }
	// Block A21
	MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);
	// Fixing Final Column                                                                                                                                                          
        MatSetValue(linOpArr[0],4*probSize+i,5*probSize-1,0.0,ADD_VALUES);
	// Block A24
        MatSetValue(linOpArr[0],4*probSize+i,4*probSize+i,ky*omn,ADD_VALUES);
      }
    } 
    else if (i >= probSize-sBound) 
    {

      SlepcUtils::FDCoeffGen(probSize-1-i-errOrder,probSize-1-i,pdOrder,dz,fdSpline);
      // Assign the entries for blocks A0, A1 and A2
      if (blockRow == 0)
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = 2*probSize + probSize-errOrder+j;
          bpdc[j] = -PETSC_i*igamma*fdSpline[j];
        }
        // Block A2
        MatSetValues(linOpArr[0],one,&i,errOrder,bcolInds,bpdc,ADD_VALUES);
        // Block A0
        MatSetValue(linOpArr[0],i,i,iDk*5.0/(3.0*tauTemp),ADD_VALUES);
        // Block A1
        MatSetValue(linOpArr[0],i,probSize+i,ky*omn+iDk,ADD_VALUES);
	// Block A3
	MatSetValue(linOpArr[0],i,3*probSize+i,iDk,ADD_VALUES);
      }
      else if (blockRow == 1) // Assign the entries for blocks A5, A6, A7, A8, and A9
      {
        c_double dBk_sum = 0.0;
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = 4*probSize + probSize-errOrder+j;
          bpdc[j] = -PETSC_i*igamma*iBk*fdSpline[j];
	        dBk_sum += -PETSC_i*igamma*fdSpline[j]*Bk[i];
        }
        // Block A9
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);
        MatSetValue(linOpArr[0],probSize+i,4*probSize+i,dBk_sum,ADD_VALUES);
        // Block A5
        MatSetValue(linOpArr[0],probSize+i,i,beta*iDk*(5.0/(3.0*tauTemp)-1.0)/2.0,ADD_VALUES);
	// Block A8
        MatSetValue(linOpArr[0],probSize+i,3*probSize+i,beta*iDk/2.0,ADD_VALUES);
      }
      else if (blockRow == 2) // Assign the entries for blocks A10, A11, A12, A13, and A14
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = probSize-errOrder+j;
          bpdc[j] = -PETSC_i*igamma*fdSpline[j];
          bpdc1[j] = -PETSC_i*igamma*(5.0/(3.0*tauTemp)+1.0)*fdSpline[j];
        }
	// Block A10
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc1,ADD_VALUES);
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = 3*probSize + probSize-errOrder+j;
        } 
        // Block A13
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);
	// Fixing Final Column                                                                                                                                                          
        MatSetValue(linOpArr[0],2*probSize+i,5*probSize-1,0.0,ADD_VALUES);
	// Block A14
        MatSetValue(linOpArr[0],2*probSize+i,4*probSize+i,0.0-ky*(omn*(1.0+1.0/tauTemp)+omt/tauTemp),ADD_VALUES);
      }
      else if (blockRow == 3) // Assign the entries for blocks A15, A16, A17, A18, and A19
      {
	// Block A16
        MatSetValue(linOpArr[0],3*probSize+i,probSize+i,ky/tauTemp*(omt-2.0/3.0*omn),ADD_VALUES);
	// Block A17
	// MatSetValue(linOpArr[0],3*probSize+i,3*probSize+i,iDk*5.0/(3.0*tauTemp),ADD_VALUES);
      }
      else if (blockRow == 4) // Assign the entries for blocks A20, A21, A22, A23, and A24
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = probSize-errOrder+j;
          bpdc[j] = PETSC_i*igamma*fdSpline[j];
        } 
        // Block A20
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);	
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = probSize + probSize-errOrder+j;
          bpdc[j] = -PETSC_i*igamma*fdSpline[j];
        }
	// Block A21
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);
	// Fixing Final Column                                                                                                                                                          
        MatSetValue(linOpArr[0],4*probSize+i,5*probSize-1,0.0,ADD_VALUES);
	// Block A24
        MatSetValue(linOpArr[0],4*probSize+i,4*probSize+i,ky*omn,ADD_VALUES);
      }
    }
    else
    {
      // Assign the entries for blocks A0, A1, A2, A3, and A4
      if (blockRow == 0)
      {
        for (j=0; j<=errOrder; ++j)
        {
          colInds[j] = 2*probSize + i + j - sBound;
          pdc[j] = -PETSC_i*igamma*pdsc[j];
        }
        // Block A2
        MatSetValues(linOpArr[0],one,&i,nSplinePts,colInds,pdc,ADD_VALUES);
        // Block A0
        MatSetValue(linOpArr[0],i,i,iDk*5.0/(3.0*tauTemp),ADD_VALUES);
        // Block A1
        MatSetValue(linOpArr[0],i,probSize+i,ky*omn+iDk,ADD_VALUES);
	// Block A3
	MatSetValue(linOpArr[0],i,3*probSize+i,iDk,ADD_VALUES);
      }
      else if (blockRow == 1) // Assign the entries for blocks A5, A6, A7, A8, and A9
      {
        c_double dBk_sum = 0.0;
        for (j=0; j<=errOrder; ++j)
        {
          colInds[j] = 4*probSize + i + j - sBound;
          pdc[j] = -PETSC_i*igamma*iBk*pdsc[j];
	        dBk_sum += -PETSC_i*igamma*pdsc[j]*Bk[i];
        } 
        // Block A9
        MatSetValues(linOpArr[0],one,&k,nSplinePts,colInds,pdc,ADD_VALUES);
        MatSetValue(linOpArr[0],probSize+i,4*probSize+i,dBk_sum,ADD_VALUES);
        // Block A5
        MatSetValue(linOpArr[0],probSize+i,i,beta*iDk*(5.0/(3.0*tauTemp)-1.0)/2.0,ADD_VALUES);
	// Block A8
        MatSetValue(linOpArr[0],probSize+i,3*probSize+i,beta*iDk/2.0,ADD_VALUES);
      }
      else if (blockRow == 2) // Assign the entries for blocks A10, A11, A12, A13, and A14
      {
        for (j=0; j<errOrder; ++j)
        {
          colInds[j] = i + j -sBound;
          pdc[j] = -PETSC_i*igamma*pdsc[j];
          pdc1[j] = -PETSC_i*igamma*(5.0/(3.0*tauTemp)+1.0)*pdsc[j];
        }
	// Block A10
        MatSetValues(linOpArr[0],one,&k,nSplinePts,colInds,pdc1,ADD_VALUES);       
        for (j=0; j<errOrder; ++j)
        {
          colInds[j] = 3*probSize + i + j - sBound;
        } 
        // Block A13
        // MatSetValues(linOpArr[0],one,&k,nSplinePts,colInds,pdc,ADD_VALUES); Culprit for last column shenanigans in blockRow2
	// Fixing Final Column                                                                                                                                                          
        MatSetValue(linOpArr[0],2*probSize+i,5*probSize-1.0,0.0,ADD_VALUES);
	// Block A14
        MatSetValue(linOpArr[0],2*probSize+i,4*probSize+i,0.0-ky*(omn*(1.0+1.0/tauTemp)+omt/tauTemp),ADD_VALUES);
      }
      else if (blockRow == 3) // Assign the entries for blocks A15, A16, A17, A18, and A19
      {
	// Block A16
        MatSetValue(linOpArr[0],3*probSize+i,probSize+i,ky/tauTemp*(omt-2.0/3.0*omn),ADD_VALUES);
	// Block A17
	// MatSetValue(linOpArr[0],3*probSize+i,3*probSize+i,iDk*5.0/(3.0*tauTemp),ADD_VALUES);
      }
      else if (blockRow == 4) // Assign the entries for blocks A20, A21, A22, A23, and A24
      {
        for (j=0; j<errOrder; ++j)
        {
          colInds[j] = i + j - sBound;
          pdc[j] = PETSC_i*igamma*pdsc[j+1];
        } 
        // Block A20
        // MatSetValues(linOpArr[0],one,&k,nSplinePts,colInds,pdc,ADD_VALUES); Culprit 1/2 for last column shenanigans in blockRow4	
        for (j=0; j<errOrder; ++j)
        {
          colInds[j] = probSize + i + j - sBound;
          pdc[j] = -PETSC_i*igamma*pdsc[j];
        }
	// Block A21
	// MatSetValues(linOpArr[0],one,&k,nSplinePts,colInds,pdc,ADD_VALUES); Culprit 2/2 for last column shenanigans in blockRow4
	// Fixing Final Column                                                                                                                                                          
        MatSetValue(linOpArr[0],4*probSize+i,5*probSize-1,0.0,ADD_VALUES);
	// Block A24
        MatSetValue(linOpArr[0],4*probSize+i,4*probSize+i,ky*omn,ADD_VALUES);
      }   
    }
  } // End global loop
  // Assemble the operators
  for (auto n = 0; n<2 ; n++)
  {
    MatAssemblyBegin(linOpArr[n],MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(linOpArr[n],MAT_FINAL_ASSEMBLY);
  }

  MatView(linOpArr[0],PETSC_VIEWER_STDOUT_WORLD);
  MatView(linOpArr[1],PETSC_VIEWER_STDOUT_WORLD);
  
  // Clean up the pointers
  delete[] colHypzInds;
  delete[] colInds;
  delete[] bcolInds;
  delete[] pdc;
  delete[] pdc1;
  delete[] bpdc;
  delete[] bpdc1;
  delete[] pdsc;
  delete[] hypzc;
  delete[] bhypzc;
  delete[] ihypzc;

}

void KBM5Field::extractEPSSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky)
{
  PetscScalar kr, ki;
  Vec xr, xi;
  PetscInt vecSize, nConv;
  PetscReal err;
  const PetscScalar *v;  
  c_Vec ev, evals;
  cc_Vec evs;
  d_Vec kpar, distToTarget, distToSoln, modeFilter, ev2, a2;
  c_Vec soln;
  std::vector<bool> useMode;
  int evIdx;

  VecDuplicate(initCondVec,&xr);
  VecDuplicate(initCondVec,&xi);
  EPSGetConverged(eps,&nConv);
/*
  if (nConv >= 1) {
    soln.resize(5);
    kpar.resize(nConv);
    distToTarget.resize(nConv);
    distToSoln.resize(nConv);
    modeFilter.resize(nConv);
    evals.resize(nConv);
    //evs.resize(nConv);
    a2.resize(nConv);
    useMode.resize(nConv);
    for (int i=0; i<nConv; i++)
    {
      EPSGetEigenpair(eps,i,&kr,&ki,xr,xi);
      petscEigVal = kr;
      evals[i] = kr;
      if (std::abs(PetscImaginaryPart(petscEigVal)) >= 1e-4) {
        EPSGetErrorEstimate(eps,i,&err);
        VecGetSize(xr,&vecSize); 
        if (vecSize/sc.nFields != probSize) throw(std::runtime_error("PTSM3D Error! Vectors have incompatible sizes!"));
        ev.resize(probSize);
        ev2.resize(probSize);
        //evs[i].resize(probSize);
        
        SlepcUtils::extractSeqVecField(sc,petscVecs[0],xr,0);
        VecGetArrayRead(petscVecs[0],&v);
        for (int j=0; j < probSize; ++j)
        {
          ev[j] = v[j];
          ev2[j] = PetscRealPart(v[j]*std::conj(v[j]));
        }
        VecRestoreArrayRead(xr,&v);
        //evs[i] = ev;
        auto maxPhi2Iter = std::max_element(ev2.begin(),ev2.end());
        auto maxPhi2 = *maxPhi2Iter;
        
        if (ev2.front()/maxPhi2 <= 1e-8 && ev2.back()/maxPhi2 <= 1e-8) {
          //auto marginalMode = computeMarginalMode(kx,ky,zBoundLower,zBoundUpper,ev);
          //a2[i] = std::get<0>(marginalMode);
          //kpar[i] = std::get<1>(marginalMode);
          distToTarget[i] = std::abs(sc.target-petscEigVal);
          useMode[i] = true;
        } else {
          //a2[i] = 0;
          //kpar[i] = 1e8;
          distToTarget[i] = 1.0;
          useMode[i] = false;
        }
        VecDestroy(&petscVecs[0]);
      } else {
        //a2[i] = 0;
        //kpar[i] = 1e8;
        distToTarget[i] = 1.0;
        useMode[i] = false;
      }

      //modeFilter[i] = PetscImaginaryPart(sc.target) > 0 ?  std::pow(kpar[i],2)*std::pow(distToTarget[i],4) : std::pow(kpar[i],2)/std::abs(std::imag(soln[0]));
      modeFilter[i] = PetscImaginaryPart(sc.target) > 0 ?  std::pow(distToTarget[i],4) : std::abs(std::imag(soln[0]));

      // *
      PetscScalar val[1];
      PetscReal realView[3];
      realView[0] = kpar[i]*kpar[i];
      realView[1] = std::pow(distToTarget[i],4);
      realView[2] = modeFilter[i];
      PetscScalar scalView[3];
      scalView[0] = -a2[i];
      scalView[1] = -2*std::real(evals[i]);
      scalView[2] = -a2[i] - 2*std::real(evals[i]);

      DebugMsg(PetscIntView(1,&i,PETSC_VIEWER_STDOUT_WORLD););
      val[0] = evals[i];
      DebugMsg(PetscScalarView(1,val,PETSC_VIEWER_STDOUT_WORLD););
      DebugMsg(PetscRealView(3,realView,PETSC_VIEWER_STDOUT_WORLD););
      val[0] = -a2[i] - 2*std::real(evals[i]);
      DebugMsg(PetscScalarView(3,scalView,PETSC_VIEWER_STDOUT_WORLD););
      // *
      //DebugMsg(std::cout << i << '\t' << evals[i] << '\t' << kpar[i]*kpar[i] << '\t' << a2[i] << '\t' << std::pow(distToTarget[i],4)  << '\t' << modeFilter[i] << '\t' << -a2[i] - 2*std::real(evals[i]) << '\n';);
    }
//#if defined(DEBUG)
//    HDF5Utils::writeEigenspectrum(this->getHDF5Mid(),evals,evs);
//#endif
    auto minIter = std::min_element(modeFilter.begin(),modeFilter.end());
    evIdx = std::distance(modeFilter.begin(),std::find(modeFilter.begin(),modeFilter.end(),*minIter));
    //std::cout << evIdx << '\n';
    MPI_Bcast(&evIdx,1,MPI_INT,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);
    
    if (useMode[evIdx]) {
      //DebugMsg(std::cout << evIdx << '\n';);
      EPSGetEigenpair(eps,evIdx,&kr,&ki,xr,xi);
      petscEigVal = kr;
      // Copy the vector that will serve as the initial
      // condition for the next iteration
      VecCopy(xr,initCondVec);

      VecGetSize(initCondVec,&vecSize);
      if (vecSize/sc.nFields != probSize)
        throw(std::runtime_error("PTSM3D Error! Vectors have incompatible sizes!"));

      cc_Vec tempVec;
      tempVec.resize(5);
      for (auto j=0; j<5; ++j) {
        SlepcUtils::extractSeqVecField(sc,petscVecs[j],initCondVec,j);
        VecGetArrayRead(petscVecs[j],&v);
        tempVec[j].resize(probSize);
        tempVec[j].assign(v,v+probSize);
        VecRestoreArrayRead(petscVecs[j],&v);
      }
      if (PetscImaginaryPart(petscEigVal) >= 0) {
        eigVals[0] = petscEigVal;
        eigVals[1] = std::conj(petscEigVal);
      } else {
        eigVals[0] = std::conj(petscEigVal);
        eigVals[1] = petscEigVal;
      }
      //eigVals[2] = -a2[evIdx] - 2*std::real(petscEigVal);
      // Set the other eigenvalues to zero for now
      for (auto n = 2; n<5; ++n) eigVals[n] = 0.0;

      d_Vec Phi2(probSize);
      for (auto i = 0; i<probSize; ++i) Phi2[i] = std::pow(std::abs(tempVec[0][i]),2);
      auto maxPhi2Iter = std::max_element(Phi2.begin(),Phi2.end());
      auto maxPhi2 = *maxPhi2Iter;
      zIdx1 = 0;
      zIdx2 = probSize;
      for (auto i = 0; i<probSize; i++) {
        if (Phi2[i]/maxPhi2 > 1e-8) {
          zIdx1 = i;
          break;
        }
      }
      for (auto i=probSize-1; i>=0; i--) {
        if (Phi2[i]/maxPhi2 > 1e-8) {
          zIdx2 = i;
          break;
        }
      }
      for (auto i=0; i<5; ++i) {
        eigVec[i].resize(zIdx2-zIdx1+1);
        for (auto j=zIdx1; j <=zIdx2; ++j) {
          eigVec[i][j-zIdx1] = tempVec[i][j];
        }
      }
      //DebugMsg(std::cout << "zIdx1:" << '\t' << zIdx1 << '\t' << "zIdx2" << '\t' << zIdx2 << '\n';);

    } else {
      petscEigVal = 0.0;
      for (auto m = 0; m < nFields; ++m) eigVals[m] = petscEigVal;
    }

  } else {
    petscEigVal = 0.0;
    for (auto m = 0; m < nFields; ++m) eigVals[m] = petscEigVal;
  }

*/    
  VecDestroy(&xr);
  VecDestroy(&xi);
}


/******************************************************************************
*                                                                             *
* Solver independent routines                                                 *
*                                                                             *
******************************************************************************/


void KBM5Field::solve(SlepcUtils::SlepcContext& sc)
{
  if (sc.probType == "eps") {
    SlepcUtils::solve(eps,sc,linOpArr);
  }
}

PetscScalar KBM5Field::guessTarget(const double kx, const double ky)
{
  /*
  PetscScalar targetGuess;
  c_Vec phi;
  c_Vec roots(5);
  //const auto delta = geomPtr->getq0()/Utils::pi();
  const auto delta = geomPtr->getNfp() > 1 ? 1.0/(geomPtr->getNfp() - 1.0/geomPtr->getq0())/Utils::pi() : geomPtr->getq0()/Utils::pi();
  const auto dz = geomPtr->getDEta();
  const auto nz = geomPtr->getNPts();
  const auto shat = geomPtr->getShat();
  const int vecSize =  (int)(std::round(delta/dz*std::sqrt(2.0*35.0))+1) % 2 == 1 ? std::round(delta/dz*std::sqrt(2.0*35.0))+1 : std::round(delta/dz*std::sqrt(2.0*35.0))+2;
  //DebugMsg(std::cout << delta << '\t' << dz << '\t' << dz/delta << '\t' << shat << '\t' << vecSize << '\n';);
  
  phi = Utils::gaussianMode(vecSize,delta,dz);
  auto zeroInd = std::floor(-kx/(shat*ky)/Utils::pi()) + nz/2;
  auto iz1 = zeroInd - vecSize/2-1;
  auto iz2 = zeroInd + vecSize/2-1;
  //DebugMsg(std::cout << nz << '\t'<<  vecSize << '\t' << zeroInd << '\t' << iz1 << '\t' << iz2 << '\t' << iz2-iz1+1 <<'\n';);
  computeLinearRoots(kx,ky,iz1,iz2,phi,roots);

  targetGuess = roots[0];
  */
  PetscScalar targetGuess = 0.0;
  return targetGuess;
}

/*
  !!!! WARNING !!!!
  The formula for the cubic equation works best when coefficients are real.  This is 
  accomplished by substituting lambda = -i omega to make all the coefficients real.
  This is opposite of the GENE convention and the convention used in buildEPS(PEP)operators!
*/
double KBM5Field::computeLinearRoots(const double kx, const double ky, const int iz1, const int iz2, 
                                   const c_Vec& evec, c_Vec& roots)
{
  double kpar2_avg = 0.0;
  /*
  const auto piInv = 1.0/Utils::pi();
  // Constants necessary for solving the dispersion relation
  const c_double posRootUnity(-0.5,0.5*std::sqrt(3.0)), negRootUnity(-0.5,-0.5*std::sqrt(3.0));
  
  // A vector of vectors to hold the linear operator
  c_Vec Lavg(4,0);
  c_double iLavg3, iLavg2, iLavg1, iLavg0; 
  c_double jLavg3, jLavg2, jLavg1, jLavg0; 

  // Coefficient vectors for the interior points and the boundary points first derivatives
  c_Vec fdc1Interior, fdc1Boundary;
  c_Vec fdc2Interior, fdc2Boundary;

  // Vectors
  d_Vec evec2, FDSpline1, FDSpline2, Bk, Dk;
  // 1st and 2nd derivatives of the eigenvector along the field line 
  c_double idparEvec, idparEvecConj, idpar2Evec, dparEvecSum, dpar2EvecSum, ievec, ievecConj, ievec2Sum, jevec2Sum, evec2Sum;
  c_double a0, a1, a2, p, q, d, u, v, usign, vsign;

  // Running sums to compute averages 
  double iBk, iDk, ijac, imodB, ijacBInv, idparJacBInv;

  // Resize the vectors
  const auto vecSize = evec.size();

  const double dz = geomPtr->getDEta();
  FDSpline1.resize(errOrder+1);
  FDSpline2.resize(errOrder+1);
  evec2.resize(vecSize);
  Bk.resize(vecSize);
  Dk.resize(vecSize);
  geomPtr->buildBk(kx,ky,iz1,iz2,Bk);
  geomPtr->buildDk(kx,ky,iz1,iz2,Dk);

  // Compute the norm of the square of the eigenvector
  evec2Sum = 0;
   
  // Compute the 2nd order parallel derivative of the eigenfunction 
  // Compute the interior spline points first, so as to not repeat during
  // global loop
  auto sBound = errOrder/2;

  // Compute the elements in one global loop along the field line
  // Do the first element for the trapezoid rule 
  iBk = Bk[0];
  iDk = Dk[0];
  ijac = geomPtr->jac[iz1];
  imodB = geomPtr->modB[iz1];
  ijacBInv = geomPtr->jacBInv[iz1];
  idparJacBInv = geomPtr->dparJacBInv[iz1];
  ievec = evec[0];
  ievecConj = std::conj(ievec);
  Utils::FDCoeffGen(-sBound+sBound,errOrder,1,dz,FDSpline1);
  Utils::FDCoeffGen(-sBound+sBound,errOrder,2,dz,FDSpline2);
  
  dparEvecSum = 0.;
  dpar2EvecSum = 0.;
  for (auto j=0; j<=errOrder; j++)
  {
    auto idx = -sBound+sBound+j;
    dparEvecSum += FDSpline1[j]*evec[idx];
    dpar2EvecSum += FDSpline2[j]*evec[idx];
  }

  idparEvec = dparEvecSum;
  idparEvecConj = std::conj(idparEvec);
  idpar2Evec = dpar2EvecSum;

  ievec2Sum = imodB*ijac*ievecConj*ievec;

  Utils::FDCoeffGen(-sBound,sBound,1,dz,FDSpline1);
  Utils::FDCoeffGen(-sBound,sBound,2,dz,FDSpline2);
  
  fdc1Interior.resize(errOrder+1);
  fdc2Interior.resize(errOrder+1);
  for (auto i=0; i<=errOrder; i++)
  {
    fdc1Interior[i] = FDSpline1[i];
    fdc2Interior[i] = FDSpline2[i];
  }

  for (auto k=1; k<vecSize-1; k++)
  {
    auto i = iz1 + k;
    iBk = Bk[k];
    iDk = Dk[k];
    ijac = geomPtr->jac[i];
    imodB = geomPtr->modB[i];
    ijacBInv = geomPtr->jacBInv[i];
    idparJacBInv = geomPtr->dparJacBInv[i];
    ievec = evec[k];
    ievecConj = std::conj(ievec);

    dparEvecSum = 0.;
    dpar2EvecSum = 0.;
    
    // Deal with the lower boundary points
    if (k < sBound) {
      Utils::FDCoeffGen(-sBound+std::abs(sBound-k),errOrder-k,1,dz,FDSpline1);
      Utils::FDCoeffGen(-sBound+std::abs(sBound-k),errOrder-k,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = k-sBound+std::abs(sBound-k)+j;
        dparEvecSum += FDSpline1[j]*evec[idx];
        dpar2EvecSum += FDSpline2[j]*evec[idx];
      }
    // Deal with the upper boundary points
    } else if (k >= vecSize-sBound) { 
      Utils::FDCoeffGen(vecSize-1-k-errOrder,vecSize-1-k,1,dz,FDSpline1);
      Utils::FDCoeffGen(vecSize-1-k-errOrder,vecSize-1-k,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = vecSize-errOrder+j-1;
        dparEvecSum += FDSpline1[j]*evec[idx];
        dpar2EvecSum += FDSpline2[j]*evec[idx];
      }
        
    // Deal with the interior points
    } else {
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = k-sBound+j;
        dparEvecSum += fdc1Interior[j]*evec[idx];
        dpar2EvecSum += fdc2Interior[j]*evec[idx];
      }
    }
    idparEvec = dparEvecSum;
    idparEvecConj = std::conj(idparEvec);
    idpar2Evec = dpar2EvecSum;

    jLavg3 = iLavg3; 
    iLavg3 = imodB*ijac*ievecConj*(1.0 + iBk*(1.0+5.0/3.0*tauTemp))*ievec;
    Lavg[3] += 0.5*(iLavg3 + jLavg3);

    jLavg2 = iLavg2;
    iLavg2 = imodB*ijac*ievecConj*(ky*((omt-2.0/3.0*omn)*iBk*tauTemp-omn)-iDk*(1.0+5.0/3.0*tauTemp))*ievec;
    Lavg[2] += 0.5*(iLavg2 + jLavg2);

    jLavg1 = iLavg1;
    iLavg1 = -imodB*ijac*ievecConj*(iDk*ky*(omt-2.0/3.0*omn)*tauTemp)*ievec - 
              piInv*piInv*(1.0+5.0/3.0*tauTemp)*ijacBInv*idparEvecConj*idparEvec;
    Lavg[1] += 0.5*(iLavg1 + jLavg1);

    jLavg0 = iLavg0;
    iLavg0 = -ky*(omt-2.0/3.0*omn)*tauTemp*piInv*piInv*ijacBInv*idparEvecConj*idparEvec;
    Lavg[0] += 0.5*(iLavg0 + jLavg0);
    
    jevec2Sum = ievec2Sum;
    ievec2Sum = imodB*ijac*ievecConj*ievec;
    evec2Sum += 0.5*(ievec2Sum + jevec2Sum);
  } // End global loop

  // Normalize Lavg by the square of the eigenvector to actually compute the average
  const auto evec2Norm = 1.0/evec2Sum;
  for (auto i=0; i<4; i++)
  {
    Lavg[i] *= evec2Norm;
  }

  double kpar2_avg = -std::real(Lavg[0])/(ky*(omt-2.0/3.0*omn)*tauTemp); 

  a2 = Lavg[2]/Lavg[3];
  a1 = Lavg[1]/Lavg[3];
  a0 = Lavg[0]/Lavg[3];

  //dispersionCoeffs[0] = a0;
  //dispersionCoeffs[1] = a1;
  //dispersionCoeffs[2] = a2;

  p = 1.0/3.0*(3.0*a1 - std::pow(a2,2));
  q = 1.0/27.0*(9.0*a1*a2 - 27.0*a0 - 2.0*std::pow(a2,3));
  d = 1.0/27.0*std::pow(p,3) + 0.25*std::pow(q,2);


  if (std::real(d) > 0.) {
    usign = ((0.5*std::real(q) + std::sqrt(std::real(d))) > 0.) ? 1. : -1.;
    vsign = ((0.5*std::real(q) - std::sqrt(std::real(d))) < 0.) ? -1. : 1.;
    u = std::pow(std::abs(0.5*q + std::sqrt(d)),1.0/3.0);
    v = std::pow(std::abs(0.5*q - std::sqrt(d)),1.0/3.0);
    
    roots[0] = -1.0/3.0*a2 + posRootUnity*u*usign + negRootUnity*v*vsign;
    roots[1] = -1.0/3.0*a2 + posRootUnity*v*vsign + negRootUnity*u*usign;
    roots[2] = -1.0/3.0*a2 + usign*u + vsign*v;
  } else {
    u = std::pow(0.5*std::real(q) + std::sqrt(d),1.0/3.0);
    v = std::pow(0.5*std::real(q) - std::sqrt(d),1.0/3.0);

    roots[0] = std::real(-1.0/3.0*a2 + posRootUnity*u + negRootUnity*v);
    roots[1] = std::real(-1.0/3.0*a2 + posRootUnity*u + negRootUnity*v);
    roots[2] = std::real(-1.0/3.0*a2 + u + v);
  }

  //Diagnostic messages
  DebugMsg(std::cout << "====" << '\n';);
  DebugMsg(std::cout << Lavg[0] << '\t' << Lavg[1] << '\t' << Lavg[2] << '\t' << Lavg[3] << '\n';);
  DebugMsg(std::cout << p << '\t' << q << '\t' << d << '\t' << std::real(d) << '\n';);
  DebugMsg(std::cout << roots[0] << '\t' << roots[1] << '\t' << roots[2] << '\n';);
  // This should be zero for all modes
  DebugMsg(std::cout << roots[0]*roots[0]*roots[0] + roots[0]*roots[0]*a2 + roots[0]*a1 + a0 << '\n';);
  DebugMsg(std::cout << roots[1]*roots[1]*roots[1] + roots[1]*roots[1]*a2 + roots[1]*a1 + a0 << '\n';);
  DebugMsg(std::cout << roots[2]*roots[2]*roots[2] + roots[2]*roots[2]*a2 + roots[2]*a1 + a0 << '\n';);
  DebugMsg(std::cout << "====" << '\n';);

  //for (auto& root : roots) root = -PETSC_i*root;
  //return std::abs(Lavg[0]);
  */
  return kpar2_avg;

}

void KBM5Field::computeMassMatrix(const int modeID, const double ky, const c_Vec& dispRoots) {
  /*
  cc_Vec temp;
  temp.resize(3);
  for (auto& iter : temp) iter.resize(3);

  massMat[0][0] = 1.0;
  massMat[0][1] = 1.0;
  massMat[0][2] = 1.0;
  massMat[1][0] = omt*ky/dispRoots[0];
  massMat[1][1] = omt*ky/dispRoots[1];
  massMat[1][2] = omt*ky/dispRoots[2];
  massMat[2][0] = PETSC_i*(1.0+omt*ky/dispRoots[0])/dispRoots[0];
  massMat[2][1] = PETSC_i*(1.0+omt*ky/dispRoots[1])/dispRoots[1];
  massMat[2][2] = PETSC_i*(1.0+omt*ky/dispRoots[2])/dispRoots[2];

  Utils::matInverse(massMat,massMatInv);
  */
}
