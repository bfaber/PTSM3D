#ifndef MPIUTILS_HPP
#define MPIUTILS_HPP
#include <memory>

#include "mpi.h"

#include "Definitions.hpp"

class Geom;
class PTSM3D;

namespace MPIUtils {
  void passGeometry(const int mpiRank, std::shared_ptr<Geom> geom, MPI_Comm comm);
  void passParamMap(const int mpiRank, PTSM3D* model, MPI_Comm comm);
}

#endif
