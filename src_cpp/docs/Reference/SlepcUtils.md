The namespace SlepcUtils is a collection of utility functions for calling 
PETSc and SLEPc functions as well as interfacing between PETSc/SLEPc types
and STL types.

---
### FDCoeffGen()

```c++
void FDCoeffGen(const PetscInt sl, const PetscInt su, const PetscInt pdOrder, const PetscReal h, std::vector<std::complex<double>>& spline);
```

Converts from PETSc data types and calls [Utils::FDCoeffGen](Utils.md#fdcoeffgen) 

---

---

### solveEPS()

```c++
void solveEPS(SlepcContext sc, const Mat& A, const Mat& B, PetscScalar& eval, Vec& evec);
```

Solves the generalized eigenvalue equation \(\mathbf{A} \cdot \mathbf{x} = \lambda \mathbf{B}\cdot\mathbf{x}\)

**Parameters:**

  - *sc*: SlepcContext controlling the solver

  - *A*: the left-hand-side matrix operator

  - *B*: the right-hand-side matrix operator

  - *eval*: the solved for eigenvalue

  - *evec*: the solved for eigenvector

**Returns:**
  - *nothing*
---

---
### solvePEP()

```c++
void solvePEP(SlepcContext sc, Mat* const pepMatArr, const PetscInt pOrder, PetscScalar& eval, Vec& evec);
```

Solves the polynomical eigenvalue equation \(\sum\limits_{k=0}^{p} \lambda^k \mathbf{M}^k \cdot \mathbf{x} = 0\) for the the eigenvalue \(\lambda\) and the eigenvector \(\mathbf{x}\). \(\,\,\mathbf{M}^k\) is a differential operator of order \(k\).


**Parameters:**

  - *sc*: SlepcContext controlling the solver

  - *pepMatArr*: pointer to the array of matrices representing the linear operator

  - *pOrder*: order of the polynomial eigenvalue problem

  - *eval*: the solved for eigenvalue

  - *evec*: the solved for eigenvector

**Returns:**
  - *nothing*

---
