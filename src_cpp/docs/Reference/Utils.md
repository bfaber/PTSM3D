---
### pi()

Return the value of \(\pi\)

```c++
constexpr auto pi() { return std::acos(-1); };
```

---

---
###  factorial()

Compute the factorial of a number x.

```c++
int factorial(const int);
```

---

---
### FDCoeffGen()

Compute the spline coefficients for the finite-difference representation of a 1D derivative of order \(p\) with error accuracy \(e\)

```c++
void FDCoeffGen(const int, const int, const int, const double, std::vector<double>&);
```

---

---
### TaylorCoeffs()

Compute the Taylor coefficients necessary to compute the spline in FDCoeffGen.

```c++
void TaylorCoeffs(const int, const int, const double, std::vector<double>&);
```

---

---
### gaussianMode()

Creates a vector with Gaussian structure to represent a strongly ballooned mode along
a field line

**Parameters**

  - vecSize: the size of the vector desired

  - delta: the width of the Gaussian at full-width half-maximum

  - dz: the parallel resolution

**Returns**
  - a vector with double-type elements representing the square of the Gaussian mode

```c++
std::vector<double> gaussianMode(const int vecSize, const double delta, const double dz);
```

---
