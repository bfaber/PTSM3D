
---
### createEPSOperators()
```c++
void createEPSOperators(const int N);
```

**Parameters**

  - `const int N`: The number of grid points along the field line

**Returns**

  - `void`

---

---
### destroyEPSOperators()
```c++
void destroyEPSOperators();
```
**Parameters**

  - none

**Returns**

  - `void`

---

---
### buildEPSOperators()
```c++
void buildEPSOperators(const int N, const double kx, const double ky, const int iOffset)
```

**Parameters:**

  - `const int N`: Number of grid points along the field line
  - `const double kx`: The kx wavenumber
  - `const double ky`: The ky wavenumber
  - `const int iOffset`: The integer index along the field line where the mode is centered (theta\_k)

**Returns:**

  - `void`

---

---
### createPEPOperators()
```c++
void createEPSOperators(const int N, const int pOrder);
```

**Parameters:**

  - `const int N`: The number of grid points along the field line
  - `const int pOrder`: The order of polynomial eigenvalue problem

**Returns:**

  - `void`

---

---
### destroyPEPOperators()
```c++
void destroyEPSOperators(const int pOrder);
```

**Parameters:**

  - `const int pOrder`: The order of the polynomial eigenvalue problem

**Returns:**

  - `void`

---

---
### buildPEPOperators()
```c++
void buildEPSOperators(const int N, const double kx, const double ky, const int iOffset)
```

**Parameters:**

  - `const int N`: Number of grid points along the field line
  - `const double kx`: The kx wavenumber
  - `const double ky`: The ky wavenumber
  - `const int iOffset`: The integer index along the field line where the mode is centered (theta\_k)

**Returns:**

  - `void`

---

---
### solve()
```c++
void solve(SlepcUtils::SlepcContext& sc);
```

Invokes the solve routine on either EPS or PEP operators to solve for the
eigenvalues and eigenfunctions of the ITG 3-field problem.  Updates the
petscEigVal and petscEigVec variables with the results of the calculation.

**Parameters**

  - `SlepcContext sc`: the SLEPc context controlling the solver parameters (such as tolerance
and eigenvalue target)

**Returns**

  - `void`

---

---

### computeCubicITG()

```c++
void computeCubicITG(const double kx, const double ky, const std::vector<std::complex<double>>& eigVec, PetscScalar* roots);
```

Computes the analytic 3-field ITG dispersion relation using an eigenvector.  The roots of the dispersion relation are returned in the variable `roots`.

**Parameters:**

  - `const double kx`: the kx wavenumber
  - `const double ky`: the ky wavenumber
  - `const std::vector<std::complex<double>>& eigVec`: the vector containing the eigenvector
  - `PetscScalar* roots`: pointer to array to hold the solution to the dispersion relation

**Returns:**

  - `void`

---

---
### guessTarget()

```c++
PetscScalar guessTarget(const double kx, const double ky);
```

---

---

## Private Variables

  - `std::complex<double> eigVal`: Complex double representation of eigenvalue
  - `std::vector< std::complex<double> > eigVec`: complex double vector representation of eigenvector
  - `PetscScalar petscEigVal`: PETSc scalar representation for the eigenvalue
  - `Vec petscEigVec`:  PETSc vector for the eigenvalue
  - `Mat epsMatA`: Left-hand-side matrix operator in PETSc representation of the generalized eigenvalue problem \(\mathbf{A}\cdot\mathbf{x}=\lambda\mathbf{B}\cdot\mathbf{x}\)
  - `Mat epsMatB`: Right-hand-side matrix operator in PETSc representation of the generalized eigenvalue problem \(\mathbf{A}\cdot\mathbf{x}=\lambda\mathbf{B}\cdot\mathbf{x}\)
  - `Mat*  pepMatArr`: Pointer to array of PETSc matrices that represent the linear operators in the polynomial eigenvalue problem \(\sum\limits_{k=0}^{p} \lambda^k \mathbf{L}^k \cdot \mathbf{x} = 0\)

---
