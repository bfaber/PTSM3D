#include <iostream>
#include <map>
#include <memory>

#include "MPIUtils.hpp"
#include "Definitions.hpp"
#include "Geom.hpp"
#include "PTSM3D.hpp"

void MPIUtils::passGeometry(const int mpiRank, std::shared_ptr<Geom> geom, MPI_Comm comm) {

  int dataSize;
  if (mpiRank == 0) dataSize = geom->getNPts();
  MPI_Bcast(&dataSize,1,MPI_INT,0,comm);
  if (mpiRank != 0) geom->setGeomSize(dataSize);

  d_Vec geomData(dataSize);
  double* geomBuffer = new double[dataSize]; // This pointer points to data in the vector
                                             // thus you can't free it and then deallocate
                                             // the vector, hence no `delete[] geomBuffer`
                                             // at the end of the function
  std::map<std::string,geomDataPtr> geomDataMap;
  std::map<std::string,geomFuncPtr> geomFuncMap;
  geomDataMap["g11"] = &Geom::getGxx;
  geomDataMap["g12"] = &Geom::getGxy;
  geomDataMap["g22"] = &Geom::getGyy;
  geomDataMap["bmag"] = &Geom::getModB;
  geomDataMap["jac"] = &Geom::getJac;
  geomDataMap["curv_drift_x2"] = &Geom::getDBdx;
  geomDataMap["curv_drift_x1"] = &Geom::getDBdy;
  geomDataMap["d_B_d_x3"] = &Geom::getDBdz;
  geomDataMap["x3"] = &Geom::getEta;

  geomFuncMap["g11"] = &Geom::setGxx;
  geomFuncMap["g12"] = &Geom::setGxy;
  geomFuncMap["g22"] = &Geom::setGyy;
  geomFuncMap["bmag"] = &Geom::setModB;
  geomFuncMap["jac"] = &Geom::setJac;
  geomFuncMap["curv_drift_x2"] = &Geom::setDBdx;
  geomFuncMap["curv_drift_x1"] = &Geom::setDBdy;
  geomFuncMap["d_B_d_x3"] = &Geom::setDBdz;
  geomFuncMap["x3"] = &Geom::setEta;
  for (auto iter=geomDataMap.begin(); iter != geomDataMap.end(); ++iter) {
    // Head node passes the geometry
    if (mpiRank == 0) {
      geomData = geom->getGeomField(iter->second);
      geomBuffer = geomData.data();
    }

    MPI_Bcast(geomBuffer,dataSize,MPI_DOUBLE,0,comm);
    if (mpiRank != 0) {
    // Worker nodes receive geometry
      geomData.assign(geomBuffer,geomBuffer+dataSize);
      geom->setGeomField(geomData,geomFuncMap[iter->first]);
    }
    MPI_Barrier(comm);
  }

  double geomElem;
  if (mpiRank == 0) geomElem = geom->getq0();
  MPI_Bcast(&geomElem,1,MPI_DOUBLE,0,comm);
  if (mpiRank != 0) geom->setq0(geomElem);
  MPI_Barrier(comm);

  if (mpiRank == 0) geomElem = geom->getShat();
  MPI_Bcast(&geomElem,1,MPI_DOUBLE,0,comm);
  if (mpiRank != 0) geom->setShat(geomElem);
  MPI_Barrier(comm);

  if (mpiRank == 0) geomElem = geom->getInvEps();
  MPI_Bcast(&geomElem,1,MPI_DOUBLE,0,comm);
  if (mpiRank != 0) geom->setInvEps(geomElem);
  MPI_Barrier(comm);

  if (mpiRank == 0) geomElem = geom->getDEta();
  MPI_Bcast(&geomElem,1,MPI_DOUBLE,0,comm);
  if (mpiRank != 0) geom->setDEta(geomElem);
  MPI_Barrier(comm);

  int igeomElem;
  if (mpiRank == 0) igeomElem = geom->getNfp();
  MPI_Bcast(&igeomElem,1,MPI_INT,0,comm);
  if (mpiRank != 0) geom->setNfp(igeomElem);
  MPI_Barrier(comm);
}

void MPIUtils::passParamMap(const int mpiRank, PTSM3D* model, MPI_Comm comm) {
  int nEntries, keySize, valueSize;
  char *key, *value;
  int mpiSize;
  MPI_Comm_size(comm,&mpiSize);
  StringMap pMap = model->getParamMap();
  StringMap::iterator iter = pMap.begin();
  std::string keyString, valueString;
  if (mpiRank == 0) {
    nEntries = pMap.size();
  }
  MPI_Bcast(&nEntries,1,MPI_INT,0,comm);
  MPI_Barrier(comm);

  for (auto i=0; i<nEntries; ++i) {
    if (mpiRank == 0) {
      keySize = (iter->first).size();
      key = (char*)(iter->first).c_str();
    }
    MPI_Bcast(&keySize,1,MPI_INT,0,comm);
    MPI_Barrier(comm);
    if (mpiRank != 0) key = new char[keySize];
    MPI_Bcast(key,keySize,MPI_CHAR,0,comm);
    MPI_Barrier(comm);

    if (mpiRank == 0) {
      valueSize = (iter->second).size();
      value = (char*)(iter->second).c_str();
    }
    MPI_Bcast(&valueSize,1,MPI_INT,0,comm);
    MPI_Barrier(comm);
    if (mpiRank != 0) value = new char[valueSize];
    MPI_Bcast(value,valueSize,MPI_CHAR,0,comm);
    MPI_Barrier(comm);

    if (mpiRank != 0) {
      keyString.assign(key,key+keySize);
      valueString.assign(value,value+valueSize);
      model->setParamString(keyString,valueString);
    }
    if (mpiRank == 0)  ++iter;
    MPI_Barrier(comm);
  }

}
