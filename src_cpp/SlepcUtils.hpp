#ifndef SLEPCUTILS_HPP
#define SLEPCUTILS_HPP

#include <vector>
#include <complex>
#include <string>

#include "slepc.h"
#include "Definitions.hpp"
/*
    Namespace: SlepcUtils
*/
namespace SlepcUtils
{
  struct EPSContext
  {
    EPSType solver;
  };

  struct PEPContext
  {
    PEPType solver;
  };

  struct SlepcContext
  {
    std::string probType;
    PetscInt nFields;
    PetscInt opDims;
    PetscInt nOps;
    PetscInt nev;
    PetscInt ncv;
    PetscInt mpd;
    PetscInt maxIt;
    PetscReal tol;
    PetscScalar target;
    Vec* initCond;
    PetscInt scanDirection;
    PetscBool icFlag;
    EPSContext eps;
    PEPContext pep;
  };

/*
    Function: FDCoeffGen

    Generate finite difference spline coefficients for a specified order and specified nodes.
    Uses blaze-lib to perform the matrix inverse operation.

    Parameters:
      -sl: Lower spline node index

      -su: Upper spline node index

      -pdOrder: order of the partial derivative coefficients

      -h: the finite difference grid spacing

      -spline: a vector containing the spline coefficients

    Returns:
      -void

    Usage:
      To generate a 4th-order accurate 2nd derivative approximation, the following spline would be used

        [-2,-1,0,1,2]

      Thus the usage would be FDCoeffGen(-2,2,2,h,spline);
*/

  void FDCoeffGen(const PetscInt sl, const PetscInt su, const PetscInt pdOrder, const PetscReal h, std::vector<std::complex<double>>& spline);

/*
    Function: solveEPS

    Applies the EPS solver to the generalized eigenvalue problem A*x = u B*x for the
    eigenvalue u and the eigenvector x.

    Parameters:
      -sc: SlepcContext controlling the solver

      -A: the left-hand-side matrix operator

      -B: the right-hand-side matrix operator

      -eval: the solved for eigenvalue

      -evec: the solved for eigenvector

    Returns:
      -void
*/
  void solve(EPS eps, SlepcContext sc, Mat* const A);

/*
    Function: solvePEP

    Applies the PEP solver to the polynomial eigenvalue problem sum_n{L^n(x)u^n} = 0
    eigenvalue u and the eigenvector x.

    Parameters:
      -sc: SlepcContext controlling the solver

      -A: pointer to the array of matrices representing the linear operator

      -pOrder: order of the polynomial eigenvalue problem

      -eval: the solved for eigenvalue

      -evec: the solved for eigenvector

    Returns:
      -void
*/
  void solve(PEP pep, SlepcContext sc, Mat* const A);

  // Convert a std::vector<std::complex<double>> to a PETSc vector
  Vec convertToVec(const c_Vec& cVec);
  void convertToVec(const c_Vec& cVec, Vec& pVec);
  void convertCCVecToVec(const cc_Vec& ccVec, Vec& pVec);
  void extractSeqVecField(const SlepcContext& sc, Vec& fieldVec, Vec& globalVec, const int fieldIdx);
}
#endif
