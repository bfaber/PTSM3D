#ifndef KBM5FIELD_HPP
#define KBM5FIELD_HPP

// STL includes
#include <iostream>
#include <vector>
#include <complex>
#include <utility>
#include <memory>

// External includes
#include "slepc.h"
#include "hdf5.h"

// Local includes
#include "Definitions.hpp"
#include "SlepcUtils.hpp"
#include "HDF5Utils.hpp"
#include "FluidModel.hpp"

// Forward declarations of necessary classes
class Geom;
class DriftWave;

class KBM5Field : public FluidModel
{
  public:
    // Default constructor and destructor
    KBM5Field();
    KBM5Field(const int n_fields);
    virtual ~KBM5Field();

    /********************************************************************************
      SLEPc eigensolver functions
    ********************************************************************************/
    // Wrapper for solving the eigensystem
    virtual void solve(SlepcUtils::SlepcContext& sc);
    //virtual void createSlepcSolver();
    //virtual void destroySlepcSolver();

    // Provides an initial guess of the target
    virtual PetscScalar guessTarget(const double kx, const double ky);

    // Operations for building the linear operators
    virtual void createLinearOperators(SlepcUtils::SlepcContext& sc);
    virtual void destroyLinearOperators(const SlepcUtils::SlepcContext& sc);
    virtual void zeroLinearOperators(const SlepcUtils::SlepcContext& sc, const bool setVecToZero);
    virtual void buildLinearOperators(SlepcUtils::SlepcContext& sc, const double kx, const double ky);
    virtual void extractSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky);
    virtual void computeMassMatrix(const int modeID, const double ky, const c_Vec& dispRoots);

    // Quintic Cubic dispersion relation, does not require knowledge of the type of eigensolver
    double computeLinearRoots(const double kx, const double ky, const int iz1, const int iz2,
                            const c_Vec& evec, c_Vec& roots);
    std::pair<double,double> computeMarginalMode(const double kx, const double ky, const int iz1, const int iz2, const c_Vec& evec);
    
    // Function to build the EPS problem, only called by buildLinearOperators()
    void buildEPSOperators(const double kx, const double ky);
    void extractEPSSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky);
};

#endif
