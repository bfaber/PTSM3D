#ifndef DEFINITIONS_HPP
#define DEFINITIONS_HPP

#include <vector>
#include <complex>
#include <string>
#include <unordered_map>
#include <limits>
#include <typeinfo>

#include "petsc.h"

#if defined(DEBUG)
#define DebugMsg(x) do { x } while(0)
#else
#define DebugMsg(x) do { } while(0)
#endif

const double machEps = std::numeric_limits<double>::denorm_min();

// Define some quantities to save typing

using u_int = unsigned int;
using ul_int = long unsigned int;
using c_double = std::complex<double>;
using i_Vec = std::vector<int>;
using ii_Vec = std::vector<std::vector<int>>;
using d_Vec = std::vector<double>;
using c_Vec = std::vector<std::complex<double>>;
using p_Vec = std::vector<PetscScalar>;
using pVec_Vec = std::vector<Vec>;
using dd_Vec = std::vector<std::vector<double>>;
using cc_Vec = std::vector<std::vector<std::complex<double>>>;

// Use StringMap for the parameter strings
using StringMap = std::unordered_map<std::string,std::string>;
#endif
