#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include <string>
#include <algorithm>
#include <memory>

#include "mpi.h"
#include "hdf5.h"

#include "Definitions.hpp"
#include "Utils.hpp"
#include "SlepcUtils.hpp"
#include "HDF5Utils.hpp"
#include "MPIUtils.hpp"
#include "Interfaces.hpp"
#include "Geom.hpp"
#include "FluidModel.hpp"
#include "ITG3Field.hpp"
#include "KBM5Field.hpp"
#include "PTSM3D.hpp"

/*
 * PTSM3D::PTSM3D()
 *
 * Brief: default constructor, sets the MPI communicator variables and the geometry and model
 * objects for the calculation
 *
 */
PTSM3D::PTSM3D()
{
  // Obtain the size of the MPI communicator
  MPI_Comm_size(PETSC_COMM_WORLD,&mpiSize);
  MPI_Comm_rank(PETSC_COMM_WORLD,&mpiRank);
}

/*
 * PTSM3D::~PTSM3D()
 *
 * Brief: default destructor, only needs to be modified if non-STL objects, such as global pointers
 * are allocated
 *
 */
PTSM3D::~PTSM3D()
{
}

/******************************************************************************
 * Routines for dealing with file input and output                            *
 * ***************************************************************************/

/*
* PTSM3D::readInput
*
* Brief: read input from the PTSM3D.inp and set the parameter value
* based on the parameter string label
*
* Input parameters:
*  - const std::string inpFile: string representation of the input file
*
*/
void PTSM3D::readInput(const std::string inpFile)
{
  // Use this to only compute on the head node
  if (mpiRank == 0) {
    // Use std::map to map the input to variables
    std::string line, key, value;
    const std::string eqSign = "="; // The string character on which to split the strings
    std::fstream iFile;
    iFile.open(inpFile,std::ios_base::in); // Open the input file

    // Read the file line by line
    auto peekChar = iFile.peek();
    while ( peekChar != EOF)
    {
      // Read the file line
      std::getline(iFile,line);

      // Check for emtpy lines and discard
      if (!line.empty())
      {
        std::size_t eqPos = line.find(eqSign);
        // If line.find() cannot find eqSign, then it returns string::npos
        if (eqPos < std::string::npos)
        {
          // Make sure the trimmed string for the parameter string is not empty
          // Throw an exception and exit if empty parameter string encountered
          key = (eqPos > 0) ? !(Utils::trim_copy(line.substr(0,eqPos-1))).empty() ?
            Utils::trim_copy(line.substr(0,eqPos-1)) :
            throw(std::runtime_error("PTSM3D Error! Empty parameter key identfier in PTSM3D.inp")) :
            throw(std::runtime_error("PTSM3D Error! Empty parameter key identfier in PTSM3D.inp"));

          // Make sure the trimmed string for the parameter value is not empty
          // Throw an exception and exit if empty parameter value encountered
          value = (eqPos < line.length()-1) ?
            !(Utils::trim_copy(line.substr(eqPos+1,line.length()-1))).empty() ?
            Utils::trim_copy(line.substr(eqPos+1,line.length()-1)) :
            throw(std::runtime_error("PTSM3D Error! Empty parameter value in PTSM3D.inp")) :
            throw(std::runtime_error("PTSM3D Error! Empty parameter value in PTSM3D.inp"));

          // Set the map using the parameter key and value
          this->setParamString(key,value);
        }
      }

      // Look at the next line for EOF
      peekChar = iFile.peek();
    }
  }
}

/**************************************************************************************************
* PTSM3D::setParameters - setup the params map with input from the input file
*
* Input parameters:
*  - none
**************************************************************************************************/
void PTSM3D::setParameters()
{

  // Using a string map iterator allows us to set default values, as
  // we can check whether an entry exists in the params map and act accordingly
  StringMap::iterator iter;
  // Read from the StringMap params and set up all the quantities
  iter = params.find("nFields");
  nFields = iter != params.end() ? std::stoi(iter->second) : 3;
  sc.nFields = nFields;

  // Setup nkx
  iter = params.find("nkx");
  nkx = iter != params.end() ? std::stoi(iter->second) : 41;

  iter = params.find("nky");
  nky = iter != params.end() ? std::stoi(iter->second) : 21;

  iter = params.find("nzfp");
  nzfp = iter != params.end() ? std::stoi(iter->second) : 101;

  iter = params.find("errOrder");
  errOrder = iter != params.end() ? (u_int)std::stoi(iter->second) : 2;

  iter = params.find("dkx");
  dkx = iter != params.end() ? std::stod(iter->second) : 0.05;
  // Setup kxMap
  kxMap.resize(nkx);
  kxMap[0] = 0.0;
  for (int i = 1; i<=nkx/2;++i)
  {
    kxMap[i] = (double)i * dkx;
    kxMap[nkx-i] = -dkx * (double)i;
  }


  iter = params.find("dky");
  dky = iter != params.end() ? std::stod(iter->second) : 0.05;
  // Setup kyMap
  kyMap.resize(nky);
  for (int i = 0; i<nky; ++i)
  {
    kyMap[i] = (double)(i+1) * dky;
  }

  iter = params.find("kxSpecLimit");
  kxSpecLimit = iter != params.end() ? std::stod(iter->second) : 0.2;
  kxEnergySpec.resize(nkx);

  iter = params.find("kySpecLimit");
  kySpecLimit = iter != params.end() ? std::stod(iter->second) : 0.6;
  kyEnergySpec.resize(nky);

  iter = params.find("kxSpecPower");
  kxSpecPower = iter != params.end() ? std::stod(iter->second) : 2.0;

  iter = params.find("kySpecPower");
  kySpecPower = iter != params.end() ? std::stod(iter->second) : 4.0/3.0;

  iter = params.find("geomDir");
  geomDir = iter != params.end() ? iter->second : ".";

  iter = params.find("geomFile");
  geomFile = iter != params.end() ? iter->second : "";

  iter = params.find("tag");
  tag = iter != params.end() ? iter->second : "";

  iter = params.find("outDir");
  outDir = iter != params.end() ? iter->second : "";

  iter = params.find("geomType");
  geomType = iter != params.end() ? iter->second : geomFile != "" ? "eq" : "s_alpha";

  iter = params.find("optTarget");
  optTarget = iter != params.end() ? std::stoi(iter->second) : 0;

  iter = params.find("errOrder");
  errOrder = iter != params.end() ? (u_int)std::stoi(iter->second) : 2;

  iter = params.find("hypzOrder");
  hypzOrder = iter != params.end() ? (u_int)std::stoi(iter->second) : 2;

  iter = params.find("hypzEps");
  hypzEps = iter != params.end() ? std::stod(iter->second) : 0.25;

  iter = params.find("nev");
  sc.nev = iter != params.end() ? std::stoi(iter->second) : 1;

  iter = params.find("ncv");
  sc.ncv = iter != params.end() ? std::stoi(iter->second) > 4*sc.nev + 1 ? std::stoi(iter->second) : 4*sc.nev+1 : 4*sc.nev+1 > 64 ? 4*sc.nev+1 : 64;

  iter = params.find("mpd");
  sc.mpd = iter != params.end() ? std::stoi(iter->second) > sc.ncv ? std::stoi(iter->second) : sc.ncv : sc.ncv;

  iter = params.find("maxIt");
  sc.maxIt = iter != params.end() ? std::stoi(iter->second) > 10000 ? std::stoi(iter->second) : 10000 : 10000;

  iter = params.find("tol");
  sc.tol = iter != params.end() ? std::stod(iter->second) < 1e-4 ? std::stod(iter->second) : 1e-4 : 1e-4;

  iter = params.find("which_ev");
  which_ev = iter != params.end() ? iter->second : "eps";
  if (which_ev == "pep")
  {
    sc.probType = "pep";
    sc.pep.solver = PEPTOAR;
  } else {
    sc.probType = "eps";
    sc.eps.solver = EPSKRYLOVSCHUR;
  }

  if (geomType == "eq")
  {
    iter = params.find("geomFile");
    geomFile = iter != params.end() ? iter->second : throw(std::runtime_error("PTSM3D Error! Need to specify geometry file!"));
    iter = params.find("eqType");
    eqType = iter != params.end() ? iter->second : throw(std::runtime_error("PTSM3D Error! Need to specify equilibrium file type! (VMEC vs. GIST)!"));
  }

  iter = params.find("write_eigenspectrum");
  writeEigenspectrum = iter != params.end() ? iter->second == "true" ? true : false : false;

  iter = params.find("subdominant_coupling");
  subModeCoupling = iter != params.end() ? iter->second == "true" ? true : false : false;
}


void PTSM3D::writeModelData()
{
  if (mpiRank == 0) {
    hid_t fileID, groupID;
    std::string filename = Utils::trim_copy(outDir)+"spectrum_ptsm3d_"+Utils::trim_copy(tag)+".h5";
    fileID = HDF5Utils::openHDF5File(filename);
    HDF5Utils::writeLinearSpectrum(fileID,this);
    if (nkx > 2 && nky > 3) HDF5Utils::writeTauCC(fileID,this);
    H5Fclose(fileID);
  }
  MPI_Barrier(PETSC_COMM_WORLD);
}


/*
 * PTSM3D::setupPTSM3D()
 *
 * Brief: set up the data structures and the geometry vectors requried for
 * the saturation calculation.  The geometry can be set through three
 * different implementation:
 *
 * VMEC Equilibirum: calls the vmec2pest code from VMECTools to calculate
 * the required geometry quantities from a VMEC equilibrium
 *
 * PEST File: read the geometry elements along a field line from a geometry
 * file such as from the GIST code
 *
 * User-provided analytic equilibrium: This is required for analytic tokamak
 * equilibria such as s-alpha
 *
 */
void PTSM3D::setupPTSM3D()
{
  // Call setParameters to set the numerical quantities from the input file
  if (mpiSize > 1) MPIUtils::passParamMap(mpiRank,this,PETSC_COMM_WORLD);
  MPI_Barrier(PETSC_COMM_WORLD);
  setParameters();
  paramPtr = std::make_shared<StringMap>(params);
  MPI_Barrier(PETSC_COMM_WORLD);

  // Allocate the geom and model objects
  geom = std::make_shared<Geom>();
  if (nFields == 3) {
    modelITG3 = std::make_shared<ITG3Field>();
    satModel = modelITG3->getFluidPtr();
    satModel->setNFields(nFields);
    satModel->setModelPtr(modelITG3);
  } else if (nFields == 5) {
    modelKBM5 = std::make_shared<KBM5Field>();
    satModel = modelKBM5->getFluidPtr();
    satModel->setNFields(nFields);
    satModel->setModelPtr(modelKBM5);
  }
  satModel->setPtsm3dPtr(this);
  satModel->setParamMapPtr(paramPtr);
  satModel->setModelParameters();
  MPI_Barrier(PETSC_COMM_WORLD);

  if (mpiRank == 0) {
    if (geomType == "eq")
    {
      if (eqType == "vmec")
      {
        // Call the vmec2pest Fortran routine found in
        // the VMECTools library
        // MPI requirement: call on head node only!
        Interfaces::callVmec2Pest(geomFile,0.5,dky,nzfp,satModel->getBuffer(),geom);
        probSize = satModel->getBuffer()*nzfp*geom->getNfp()+1;
      }
      if (eqType == "pest") {
        Interfaces::readPESTFile(geomFile,this);
        probSize = satModel->getBuffer()*nzfp*geom->getNfp()+1;
      }
    }
    if (geomType == "s_alpha")
    {
      geom->setGeomSize(satModel->getBuffer()*nzfp);
      if (mpiRank == 0) geom->s_alpha(1.4,0.796,0.18,satModel->getBuffer());
      probSize = satModel->getBuffer()/2*nzfp+1;
    }
  }
  if (mpiSize > 1) MPIUtils::passGeometry(mpiRank,geom,PETSC_COMM_WORLD);
  geom->computeDerivedQuantities(errOrder);
  if (mpiSize > 1)
  {
    MPI_Bcast(&probSize,1,MPI_INT,0,PETSC_COMM_WORLD);
  }
  zeroIndex = geom->findZeroIndex();
  // Pass the geometry pointer to the drift wave model
  satModel->setGeomPtr(geom);

  kxEnergySpec[0] = 1.0;
  for (auto i = 1; i<=nkx/2; ++i) {
    if (kxMap[i] <= kxSpecLimit) {
     kxEnergySpec[i] = 1.0;
     kxEnergySpec[nkx-i] = 1.0;
    } else {
      kxEnergySpec[i] = std::pow(kxSpecLimit/kxMap[i],kxSpecPower);
      kxEnergySpec[nkx-i] = std::pow(kxSpecLimit/kxMap[i],kxSpecPower);
    }
  }
  for (auto i = 0; i<nky; ++i) {
    if (kyMap[i] <= kySpecLimit) {
      kyEnergySpec[i] = 1.0;
    } else {
      kyEnergySpec[i] = std::pow(kySpecLimit/kyMap[i],kySpecPower);
    }
  }

}

/*
 * PTSM3D::computeLinearSpectrum
 *
 * Brief: calculates the linear drift wave spectrum for a given equilibrium
 * and model system of equations.
 *
 * Eventually this will use a pointer to the model, rather than the currently
 * hard coded ITG3Field model.
 *
 */

void PTSM3D::computeLinearSpectrum()
{
  /* First guess a value */
  PetscScalar targetGuess = 0;
  const PetscScalar zero = 0;
  kyStart = 0;
  double targetGuessReal, targetGuessImag;
  if (mpiRank == 0) {
    if (nky > 1) {
      kyStart = nky/2;
      while (PetscImaginaryPart(targetGuess) <= 1e-8 && kyStart < nky) {
        auto kx = kxMap[0];
        auto ky = kyMap[kyStart];
        targetGuess = satModel->guessTarget(kx,ky);
        kyStart += 1;
      } ;
      DebugMsg(std::cout << kyStart << '\t' << targetGuess << '\n';);
      if (kyStart == nky) {
        kyStart = nky/2;
        while (PetscImaginaryPart(targetGuess) <= 1e-8 && kyStart >= 0) {
          auto kx = kxMap[0];
          auto ky = kyMap[kyStart];
          targetGuess = satModel->guessTarget(kx,ky);
          kyStart -= 1;
        } ;
      } else {
        kyStart -= 1;
      }
      if (kyStart < 0) {
        kyStart = nky/2;
      }
    } else {
      kyStart = nky-1;
    }
  }
  MPI_Bcast(&kyStart,1,MPI_INT,0,PETSC_COMM_WORLD);
  MPI_Barrier(PETSC_COMM_WORLD);

  satModel->createSlepcSolver();
  sc.scanDirection = -1;
  for (int k = kyStart; k >= 0; --k)
  {
    for (int j = 0; j <= nkx/2; ++j)
    {
      MPI_Barrier(PETSC_COMM_WORLD);
      linearSpectrumIndexMap[kInds(j,k)] = -1;

      auto skip = computeInitialCondition(j,k);
      callSlepcSolver(j,k,skip);
      MPI_Barrier(PETSC_COMM_WORLD);
    }
  }
  sc.scanDirection = 1;
  for (int k = kyStart+1; k < nky; ++k)
  {
    for (int j = 0; j <= nkx/2; ++j)
    {
      MPI_Barrier(PETSC_COMM_WORLD);
      linearSpectrumIndexMap[kInds(j,k)] = -1;

      auto skip = computeInitialCondition(j,k);
      callSlepcSolver(j,k,skip);
      MPI_Barrier(PETSC_COMM_WORLD);
    }
  }
  satModel->destroySlepcSolver();
}

void PTSM3D::callSlepcSolver(const int j, const int k, const bool skip)
{
  const auto kx = kxMap[j];
  const auto ky = kyMap[k];
  const int jkIdx = kInds(j,k);

  if (skip) {

    if (mpiRank == 0) std::cout << "Skipping due to insufficient instability criterion" << '\n';
    linearSpectrum.emplace_back(j,k,nFields,0,this);
    linearSpectrumIndexMap[jkIdx] = linearSpectrum.size()-1;

  } else {

    if (mpiRank == 0) DebugMsg(std::cout << "Initial Guess:" << '\t' << sc.target << '\n';);
    const double eta_k = -kx/(geom->getShat()*ky)/Utils::pi();
    const int etaKInd = std::round(eta_k/geom->getDEta()) + zeroIndex;

    if (etaKInd-probSize/2 > 0) {
      satModel->setZBoundLower(etaKInd-probSize/2);
    } else {
      satModel->setZBoundLower(0);
    }
    if (etaKInd+probSize/2 <= geom->getNPts()-1) {
      satModel->setZBoundUpper(etaKInd+probSize/2);
    } else {
      satModel->setZBoundUpper(geom->getNPts()-1);
    }
    satModel->setProbSize(satModel->getZBoundUpper()-satModel->getZBoundLower()+1);

    satModel->createLinearOperators(sc);
    MPI_Barrier(PETSC_COMM_WORLD);
    satModel->buildLinearOperators(sc,kx,ky);
    MPI_Barrier(PETSC_COMM_WORLD);
    satModel->solve(sc);
    MPI_Barrier(PETSC_COMM_WORLD);
    satModel->extractSolution(sc,kx,ky);
    MPI_Barrier(PETSC_COMM_WORLD);

    auto nModes = satModel->getNModes();
    if (nModes > 0) {
      d_Vec modeFilter(nModes);
      i_Vec modeIdx(nModes);
      int icIdx;
      if (nModes > 1) {
        DriftWave dwTemp(j,k,nFields,nModes,this); 
        // Perform filtering first to make sure selected mode is the "true" mode
        for (auto m=0; m<nModes; ++m) {
          dwTemp.setEigVec(m,satModel->getEigVec(m));
          dwTemp.setZIndices(m,satModel->getNz(m),etaKInd-probSize/2+satModel->getZIdx1(m),etaKInd-probSize/2+satModel->getZIdx2(m),0,0);
          dwTemp.computeAvgBkDk(m);
          modeFilter.at(m) = dwTemp.getAvgDk(m)/dwTemp.getAvgBk(m);
        }
        d_Vec sortFilter = modeFilter;
        auto minDkBk = std::min_element(modeFilter.begin(),modeFilter.end());
        auto minDkBkIdx = std::distance(modeFilter.begin(),std::find(modeFilter.begin(),modeFilter.end(),*minDkBk));
        icIdx = minDkBkIdx;
        std::sort(sortFilter.begin(),sortFilter.end());
        std::transform(sortFilter.begin(),sortFilter.end(),modeIdx.begin(),[&modeFilter](const double x){return std::distance(modeFilter.begin(),std::find(modeFilter.begin(),modeFilter.end(),x));});
      } else {
        modeIdx[0] = 0;
        icIdx = 0;
      }

      if (!subModeCoupling) nModes = 1;
      linearSpectrum.emplace_back(j,k,nFields,nModes,this);
      linearSpectrumIndexMap[jkIdx] = linearSpectrum.size()-1;
      auto& dw = linearSpectrum.back();
      satModel->duplicatePetscVec(dw.getInitCondVecAddr());
      satModel->extractInitCond(icIdx);
      dw.setInitCondVec(satModel->getInitCondVec());
      dw.setInitialGuess(satModel->getEigVal(icIdx,0));
      for (auto n=0; n<nModes; ++n) {
        auto m = modeIdx[n];
        dw.setEigVals(n,satModel->getEigVals(m));
        dw.setEigVec(n,satModel->getEigVec(m));
        dw.setZIndices(n,satModel->getNz(m),etaKInd-probSize/2+satModel->getZIdx1(m),etaKInd-probSize/2+satModel->getZIdx2(m),2*zeroIndex-etaKInd+probSize/2-satModel->getZIdx2(m),2*zeroIndex-etaKInd+probSize/2-satModel->getZIdx1(m));
        dw.computeAvgBkDk(n);
        //dw.computekPar2Avg(m);
        //dw.computeCrossBk(m);

        if (mpiRank == 0) {
          std::cout << "Mode " << std::to_string(n) << ":" << '\t';
          for (auto p=0; p<nFields; ++p) std::cout << dw.getEigVal(n,p) << '\t';
          std::cout << "Bk_avg:" << '\t' << dw.getAvgBk(n) << ", Dk_avg:" << '\t' << dw.getAvgDk(n) << ", Dk_avg/Bk_avg:" << '\t' << dw.getAvgDk(n)/dw.getAvgBk(n);
          std::cout << '\n';
        }
        satModel->computeMassMatrix(m,ky,dw.getEigVals(n));
        dw.setMassMat(n,satModel->getMassMat(m));
        dw.setMassMatInv(n,satModel->getMassMatInv(m));
        
      }
     
    } else {
      linearSpectrum.emplace_back(j,k,nFields,0,this);
      linearSpectrumIndexMap[jkIdx] = linearSpectrum.size()-1;
      if (mpiRank == 0) {
        std::cout << "Mode #"<< std::to_string(0) << ":" << '\t';
        for (auto n=0; n<nFields; ++n) std::cout << linearSpectrum.back().getEigVal(0,n) << '\t';
        std::cout << '\n';
      }      
      
    }
    bool setVecToZero = PetscImaginaryPart(linearSpectrum.back().getEigVal(0,0)) >= 1e-4 ? true : false;
    satModel->zeroLinearOperators(sc,setVecToZero);
  }
}


void PTSM3D::computeTauCC()
{

  if (mpiRank == 0) {
    int kxIdx1, kxIdx2, kxIdx3, kyIdx1, kyIdx2, kyIdx3;
    int idx1, idx2, idx3;
    const double kxMax = kxMap[nkx/2];
    bool negKx2, negKy2, negKx3, negKy3;

    for (kyIdx1 = 0; kyIdx1 < nky; ++kyIdx1) {
      for (kxIdx1 = 0; kxIdx1 <= nkx/2; ++kxIdx1) {
        idx1 = linearSpectrumIndexMap.at(kInds(kxIdx1,kyIdx1));
        DriftWave& dw1 = linearSpectrum.at(idx1);

        for (kyIdx3 = 0; kyIdx3 < nky; ++kyIdx3) {
          negKy3 = false;
          for (kxIdx3 = -nkx/2; kxIdx3 <= nkx/2; ++kxIdx3) {
            if (std::abs(kxMap[kxIdx3 < 0 ? nkx + kxIdx3 : kxIdx3] - kxMap[kxIdx1]) <= kxMax && std::abs(kxMap[kxIdx3 < 0 ? nkx + kxIdx3 : kxIdx3] + kxMap[kxIdx1]) > 2.0*machEps) {
              auto kPrimeTuple = computeKPrime(kxIdx1,kyIdx1,kxIdx3,kyIdx3);
              kxIdx2 = std::get<0>(kPrimeTuple);
              kyIdx2 = std::get<1>(kPrimeTuple);
              negKx2 = std::get<2>(kPrimeTuple);
              negKy2 = std::get<3>(kPrimeTuple);
              auto non_zonal = std::get<4>(kPrimeTuple);
              if (non_zonal) {
                if (kxIdx3 >= 0) {
                  idx3 = linearSpectrumIndexMap.at(kInds(kxIdx3,kyIdx3));
                  negKx3 = false;
                } else { idx3 = linearSpectrumIndexMap.at(kInds(-kxIdx3,kyIdx3));
                  negKx3 = true;
                }
                DriftWave& dw3 = linearSpectrum.at(idx3); 
                idx2 = linearSpectrumIndexMap.at(kInds(kxIdx2,kyIdx2));
                DriftWave& dw2 = linearSpectrum.at(idx2);
                DriftWave::computeNonZonalCoupling(dw1,dw2,dw3,std::make_tuple(negKx2,negKy2,negKx3,negKy3));
              } else {
                if (std::abs(kxMap[kxIdx1] - kxMap[kxIdx2]) > 2.0*machEps) {
                  idx2 = linearSpectrumIndexMap[kInds(kxIdx2,kyIdx1)];
                  DriftWave& dw2 = linearSpectrum.at(idx2);
                  DriftWave::computeZonalCoupling(dw1,dw2,kxIdx3 < 0 ? nkx+kxIdx3 : kxIdx3,negKx2);
                }
              }
            }
          }
        }
      }
    }
  }

  MPI_Barrier(PETSC_COMM_WORLD);
  
}

void PTSM3D::computeTauCCParallel()
{
  /*
  ii_Vec triplets(3);
  for (auto& trip : triplets) trip.resize(3);
  triplets[0][0] = 0;
  triplets[0][1] = 1;
  triplets[0][2] = 0;
  triplets[1][0] = 0;
  triplets[1][1] = 1;
  triplets[1][2] = 1;
  triplets[2][0] = 0;
  triplets[2][1] = 1;
  triplets[2][2] = 2;



  int kxIdx1, kxIdx2, kxIdx3, kyIdx1, kyIdx2, kyIdx3;
  DriftWave *dw1, *dw2, *dw3;
  for (auto idx1 : linearSpectrumPartInds) {
    dw1 = &linearSpectrum[idx1];
    kxIdx1 = dw1->getKxInd();
    kyIdx1 = dw1->getKyInd();
    c_Vec zTau;
    c_Vec zCC;
    d_Vec zTauCC;
    c_Vec nzTau;
    c_Vec nzCC;
    d_Vec nzTauCC;
    i_Vec kx2Vec;
    i_Vec ky2Vec;
    i_Vec kx3Vec;
    i_Vec ky3Vec;
    i_Vec zkx2Vec;
    ii_Vec maxTriplet;


    for (auto idx2 = 0; idx2 < nkx*nky; ++idx2) {
      dw2 = &linearSpectrum[idx2];
      kxIdx2 = dw2->getKxInd();
      kyIdx2 = dw2->getKyInd();

      if (kyIdx1 == kyIdx2) {
        if (kxIdx1 != kxIdx2 && kxIdx2 != nkx - kxIdx1) {
          if (std::abs(kxMap[kxIdx1]-kxMap[kxIdx2]) <= kxMap[nkx/2]) {
            if (kxIdx2 > kxIdx1) {
              kxIdx3 = nkx - (kxIdx2 - kxIdx1);
            } else {
              kxIdx3 = kxIdx1 - kxIdx2;
            }
            if (std::imag(dw1->getEigVal(0))*std::imag(dw2->getEigVal(0)) > 0) {
              zCC.push_back(kyMap[kyIdx1]*(kxMap[kxIdx1]-kxMap[kxIdx2])*
                  (dw1->getMassMatInvEntry(0,1)*dw2->getMassMatEntry(1,1) +
                   dw1->getMassMatInvEntry(0,2)*dw2->getMassMatEntry(2,1)));
              zTau.push_back(-PETSC_i/(dw2->getEigVal(1) - std::conj(dw1->getEigVal(0))));
              zTauCC.push_back(std::abs(zTau.back() * zCC.back())*
                  kxEnergySpec[kxIdx1]*kxEnergySpec[kxIdx2]*kxEnergySpec[kxIdx3]*
                  kyEnergySpec[kyIdx1]*kyEnergySpec[kyIdx1]);
              zkx2Vec.push_back(kxIdx2);
            }
          }
        }
      }


      if (kyIdx1 != kyIdx2 && kxIdx1 != kxIdx2) {
        if (std::abs(kyMap[kyIdx1]-kyMap[kyIdx2]) <= kyMap[nky-1]) {
          kyIdx3 = std::abs(kyIdx1-kyIdx2)-1;
          if (std::abs(kxMap[kxIdx1]-kxMap[kxIdx2]) <= kxMap[nkx/2]) {
            if (kxIdx2 > kxIdx1) {
              kxIdx3 = nkx - (kxIdx2 - kxIdx1);
            } else {
              kxIdx3 = kxIdx1 - kxIdx2;
            }
            if (kxIdx3 != kxIdx1) {
              dw3 = &linearSpectrum[kInds(kxIdx3,kyIdx3)];
              //DebugMsg(std::cout << kxIdx1 << '\t' << kxIdx2 << '\t' << kxIdx3 << '\t' << kyIdx1 << '\t' << kyIdx2 << '\t' << kyIdx3  << '\t' << std::imag(dw1->getEigVal(0))*std::imag(dw2->getEigVal(0))*std::imag(dw3->getEigVal(0)) << '\n';);
              if (std::imag(dw1->getEigVal(0))*std::imag(dw2->getEigVal(0))*std::imag(dw3->getEigVal(0)) > 0) {
                for (auto& triplet : triplets) {
                    auto x = triplet[0];
                    auto y = triplet[1];
                    auto z = triplet[2];

                    nzCC.push_back((kxMap[kxIdx1]*kyMap[kyIdx2]-kyMap[kyIdx1]*kxMap[kxIdx2])*
                      (dw1->getMassMatInvEntry(x,1)*dw3->getMassMatEntry(0,z)*dw2->getMassMatEntry(1,y) +
                      0.5*dw1->getMassMatInvEntry(x,2)*dw3->getMassMatEntry(0,z)*dw2->getMassMatEntry(2,y)));
                    nzTau.push_back(-PETSC_i*1.0/(dw3->getEigVal(z) + dw2->getEigVal(y) - std::conj(dw1->getEigVal(x))));
                    nzTauCC.push_back(std::abs(nzTau.back() * nzCC.back())*
                        kxEnergySpec[kxIdx1]*kxEnergySpec[kxIdx2]*kxEnergySpec[kxIdx3]*
                        kyEnergySpec[kyIdx1]*kyEnergySpec[kyIdx2]*kyEnergySpec[kyIdx3]);
                    kx2Vec.push_back(kxIdx2);
                    ky2Vec.push_back(kyIdx2);
                    kx3Vec.push_back(kxIdx3);
                    ky3Vec.push_back(kyIdx3);
                    maxTriplet.push_back(triplet);
                }
              }
            }
          }
        }
      }
    }

    auto maxIter = std::max_element(zTauCC.begin(),zTauCC.end());
    auto maxIdx = std::distance(zTauCC.begin(),std::find(zTauCC.begin(),zTauCC.end(),*maxIter));
    if (maxIter != zTauCC.end()) {
      maxIdx = std::distance(zTauCC.begin(),std::find(zTauCC.begin(),zTauCC.end(),*maxIter));
      dw1->setZonalTauCC(*maxIter);
      dw1->setZonalTau(zTau[maxIdx]);
      dw1->setZonalCC(zCC[maxIdx]);
      dw1->setMaxZonalKx2(zkx2Vec[maxIdx]);
    }


    maxIter = std::max_element(nzTauCC.begin(),nzTauCC.end());
    if (maxIter != nzTauCC.end()) {
      maxIdx = std::distance(nzTauCC.begin(),std::find(nzTauCC.begin(),nzTauCC.end(),*maxIter));
      dw1->setNonZonalTauCC(*maxIter);
      dw1->setNonZonalTau(nzTau[maxIdx]);
      dw1->setNonZonalCC(nzCC[maxIdx]);
      dw1->setMaxTriplet(maxTriplet[maxIdx]);
      dw1->setMaxNonZonalKx2(kx2Vec[maxIdx]);
      dw1->setMaxNonZonalKy2(ky2Vec[maxIdx]);
      dw1->setMaxNonZonalKx3(kx3Vec[maxIdx]);
      dw1->setMaxNonZonalKy3(ky3Vec[maxIdx]);
      dw1->setMaxEigVal1(dw1->getEigVal(dw1->getMaxTripletEntry(0)));
      dw1->setMaxEigVal2(linearSpectrum[kInds(dw1->getMaxNonZonalKx2(),dw1->getMaxNonZonalKy2())].getEigVal(dw1->getMaxTripletEntry(1)));
      dw1->setMaxEigVal3(linearSpectrum[kInds(dw1->getMaxNonZonalKx3(),dw1->getMaxNonZonalKy3())].getEigVal(dw1->getMaxTripletEntry(2)));
    } else {
      dw1->setNonZonalTauCC(0.0);
      dw1->setNonZonalTau(0.0);
      dw1->setNonZonalCC(0.0);
      dw1->setMaxNonZonalKx2(-1);
      dw1->setMaxNonZonalKy2(-1);
      dw1->setMaxNonZonalKx3(-1);
      dw1->setMaxNonZonalKy3(-1);
    }

  }
*/
}

std::tuple<int,int,bool,bool,bool> PTSM3D::computeKPrime(const int kxIdx1, const int kyIdx1, const int kxIdx3, const int kyIdx3) {
  bool negKx2, negKy2, non_zonal;
  int kxIdx2 = kxIdx3 - kxIdx1;
  int kyIdx2 = kyIdx3 - kyIdx1;
  if (kxIdx2 < 0)  {
    kxIdx2 = -kxIdx2;
    negKx2 = true;
  } else { 
    negKx2 = false;
  }
  if (kyIdx2 < 0) {
    kyIdx2 = -kyIdx2 - 1;
    negKy2 = true;
    non_zonal = true;
  } else if (kyIdx2 > 0) {
    kyIdx2 -= 1;
    negKy2 = false;
    non_zonal = true;
  } else {
    non_zonal = false;
    negKy2 = false;
  }
  return std::tuple(kxIdx2,kyIdx2,negKx2,negKy2,non_zonal);
}

bool PTSM3D::computeInitialCondition(const int j, const int k) {
  PetscScalar targetGuess, targetGuessReal, targetGuessImag;
  const PetscScalar zero = 0.0;
  int icIdx;
  auto kx = kxMap[j];
  auto ky = kyMap[k];
  bool icFlag, skip;
  if (sc.scanDirection == -1) {
    if (mpiRank == 0) {
      std::cout << "-------------------------" << '\n';
      std::cout << "Computing eigenvalues at (" << kxMap[j] << "," << kyMap[k] << ")" << '\n';
      if (k == kyStart && j == 0)
      {
        targetGuess = satModel->guessTarget(kx,ky);
        targetGuess = PetscRealPart(targetGuess) + 2.0*PETSC_i*PetscImaginaryPart(targetGuess);
        icFlag = false;
        skip = false;
        sc.target = PetscImaginaryPart(targetGuess) > 0 ? targetGuess : zero;

      } else {
        if (j == 0) {
          icIdx = linearSpectrumIndexMap[kInds(j,k+1)];
        } else {
          icIdx = linearSpectrumIndexMap[kInds(j-1,k)];
        }
        targetGuess = linearSpectrum[icIdx].getInitialGuess();
        if (PetscImaginaryPart(targetGuess) > 0) {
          targetGuess = PetscRealPart(targetGuess) + 1.1*PETSC_i*PetscImaginaryPart(targetGuess);
          sc.target = targetGuess;
          icFlag = true;
        } else {
          targetGuess = satModel->guessTarget(kx,ky);
          skip = PetscImaginaryPart(targetGuess) > 0 ? false : true;
          icFlag = true;
          sc.target = !skip > 0 ? targetGuess : zero;
        }
      }
    }

    if (mpiRank == 0) {
      targetGuessReal = PetscRealPart(sc.target);
      targetGuessImag = PetscImaginaryPart(sc.target);
    }
    
    MPI_Bcast(&targetGuessReal,1,MPI_DOUBLE,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);
    MPI_Bcast(&targetGuessImag,1,MPI_DOUBLE,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);

    if (mpiRank != 0) sc.target = targetGuessReal + PETSC_i*targetGuessImag;

    MPI_Bcast(&icFlag,1,MPIU_BOOL,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);
    MPI_Bcast(&skip,1,MPIU_BOOL,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);

    if (icFlag) {
      sc.icFlag = PETSC_TRUE;
    } else {
      sc.icFlag = PETSC_FALSE;
    }
    if (sc.icFlag) {
      bool icFound = false;
      auto localKIdx = k;
      while (!icFound && localKIdx < nky) {
        if (j > 0) {
          auto localJIdx = j-1;
          while (!icFound && localJIdx >= 0) { 
            if (mpiRank == 0) DebugMsg(std::cout << "Evaluating " << localJIdx << '\t' << localKIdx << '\n';);
            auto icIdx = linearSpectrumIndexMap[kInds(localJIdx,localKIdx)];
            if (PetscImaginaryPart(linearSpectrum[icIdx].getEigVal(0,0)) >= 1e-4) {
              sc.initCond = linearSpectrum[icIdx].getInitCondVecAddr();
              icFound = true;
              if (mpiRank == 0) DebugMsg(std::cout << "Initial condition found at (j,k) = (" << localJIdx << "," << localKIdx << ")" << '\n';);
            } else {
              --localJIdx;
            }
          };
        } else {
          if (mpiRank == 0) DebugMsg(std::cout << "Evaluating " << 0 << '\t' << localKIdx << '\n';);
          auto icIdx = linearSpectrumIndexMap[kInds(0,localKIdx)];
          if (icIdx > -1 && PetscImaginaryPart(linearSpectrum[icIdx].getEigVal(0,0)) >= 1e-4) {
            sc.initCond = linearSpectrum[icIdx].getInitCondVecAddr();
            icFound = true;
            if (mpiRank == 0) DebugMsg(std::cout << "Initial condition found at (j,k) = (" << 0 << "," << localKIdx << ")" << '\n';);
          }
        }

        if (!icFound) {
          ++localKIdx;
        }
      };
    }

  } else if (sc.scanDirection == 1) {
    if (mpiRank == 0) {
      std::cout << "-------------------------" << '\n';
      std::cout << "Computing eigenvalues at (" << kxMap[j] << "," << kyMap[k] << ")" << '\n';
      if (j == 0) { 
        icIdx = linearSpectrumIndexMap[kInds(j,k-1)];
      } else {
        icIdx = linearSpectrumIndexMap[kInds(j-1,k)];
      }
      targetGuess = linearSpectrum[icIdx].getInitialGuess();
      if (PetscImaginaryPart(targetGuess) > 0) {
        targetGuess = PetscRealPart(targetGuess) + 1.25*PETSC_i*PetscImaginaryPart(targetGuess);
        sc.target = targetGuess;
        icFlag = true;
      } else {
        targetGuess = satModel->guessTarget(kx,ky);
        skip = PetscImaginaryPart(targetGuess) > 0 ? false : true;
        icFlag = true;
        sc.target = !skip ? targetGuess : zero;
      }
    }
    if (mpiRank == 0) {
      targetGuessReal = PetscRealPart(sc.target);
      targetGuessImag = PetscImaginaryPart(sc.target);
    }
    MPI_Bcast(&targetGuessReal,1,MPI_DOUBLE,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);
    MPI_Bcast(&targetGuessImag,1,MPI_DOUBLE,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);

    if (mpiRank != 0) sc.target = targetGuessReal + PETSC_i*targetGuessImag;

    MPI_Bcast(&icFlag,1,MPIU_BOOL,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);
    MPI_Bcast(&skip,1,MPIU_BOOL,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);

    if (icFlag) {
      sc.icFlag = PETSC_TRUE;
    } else {
      sc.icFlag = PETSC_FALSE;
    }
    if (sc.icFlag) {
      bool icFound = false;
      auto localKIdx = k;
      while (!icFound && localKIdx >= 0) {
        if (j > 0) {
          auto localJIdx = j-1;
          while (!icFound && localJIdx >= 0) { 
            auto icIdx = linearSpectrumIndexMap[kInds(localJIdx,localKIdx)];
            if (mpiRank == 0) DebugMsg(std::cout << "Evaluating " << localJIdx << '\t' << localKIdx << '\n';);
            if (PetscImaginaryPart(linearSpectrum[icIdx].getEigVal(0,0)) >= 1e-4) {
              sc.initCond = linearSpectrum[icIdx].getInitCondVecAddr();
              icFound = true;
              if (mpiRank == 0) DebugMsg(std::cout << "Initial condition found at (j,k) = (" << localJIdx << "," << localKIdx << ")" << '\n';);
            } else {
              --localJIdx;
            }
          };

        } else {
          if (mpiRank == 0) DebugMsg(std::cout << "Evaluating " << 0 << '\t' << localKIdx << '\n';);
          auto icIdx = linearSpectrumIndexMap[kInds(0,localKIdx)];
          if (icIdx > -1 && PetscImaginaryPart(linearSpectrum[icIdx].getEigVal(0,0)) >= 1e-4) {
            sc.initCond = linearSpectrum[icIdx].getInitCondVecAddr();
            icFound = true;
            if (mpiRank == 0) DebugMsg(std::cout << "Initial condition found at (j,k) = (" << 0 << "," << localKIdx << ")" << '\n';);
          }
        }
        if (!icFound) {
          --localKIdx;
        }
      };
    }
  } else {
    throw(std::runtime_error("The scan direction must be up (+1) or down (-1)!"));
  }
  return skip;
}
