#ifndef INTERFACES_HPP
#define INTERFACES_HPP

#include <memory>

#include "mpi.h"

class Geom;
class PTSM3D;

typedef void * CObject;

namespace Interfaces {
  struct v2p_opts {
    char* vmec_file;
    char* grid_type;
    char* x3_coord;
    char* norm_type;
    int nx1, nx2, nx3;
    double* x1;
    double x2_center, x3_center, nfpi;
  };

  void PTSM3DStelloptInterface();
  void callVmec2Pest(std::string eqFile, double surface, const double dky,
                     const int nz0, const int buffer, std::shared_ptr<Geom>& geom);
  void readPESTFile(const std::string& filename, PTSM3D* satModel);
};

extern "C"  {
  CObject createPTSM3D(MPI_Comm);
  void updatePTSM3DParam(CObject,char**,char**);
  void callPTSM3D(CObject,char*);
  void destroyPTSM3D(CObject);

  // vmec2pest interface functions
  void vmec2pest_c_interface(Interfaces::v2p_opts*);
  void get_pest_data_c_interface(int*,int*,char**,int*,int*,double*);
}
#endif
