#include <iostream>
#include <vector>
#include <cmath>

#if defined(WITH_BLAZE)
#include <blaze/math/DynamicMatrix.h>
#else
#include "petsc.h"
#endif

#include "Definitions.hpp"
#include "Utils.hpp"

int Utils::factorial(const int x)
{
  switch(x) {
    case 0 :
      return 1;
    case 1 :
      return 1;
    case 2 :
      return 2;
    default :
      return x*factorial(x-1);
  }
}

void Utils::TaylorCoeffs(const int sl, const int sr, const double sc, d_Vec& coeffs)
{
  int n;
  double fk;

  n = sr - sl + 1;

  for (int i = 0; i<n; ++i)
  {
    fk = (double)Utils::factorial(i);
    coeffs[i] = std::pow(sc,i)/fk;
  }
}

c_Vec Utils::gaussianMode(const int vecSize, const double delta, const double dz)
{
  c_Vec phi;
  phi.resize(vecSize);

  const auto bound = vecSize/2;

  for (int i=0; i<vecSize; i++)
  {
    phi[i] = std::exp(-0.5*std::pow((dz/delta),2.0)*std::pow((double)(i-bound),2.0));
  }
  return phi;
}

void Utils::FDCoeffGen(const int sl, const int sr, const int pdo,
                  const double h, d_Vec& FDSpline)
{
  Utils::FDCoeffGen(sl,sr,pdo,h,false,FDSpline);
}

void Utils::FDCoeffGen(const int sl, const int sr, const int pdo, const double h, c_Vec& FDSpline)
{
  d_Vec tempSpline;
  const int n = FDSpline.size();
  tempSpline.resize(n);
  Utils::FDCoeffGen(sl,sr,pdo,h,false,tempSpline);
  for (int i=0; i<n; i++)
  {
    FDSpline[i] = c_double(tempSpline[i],0.0);
  }
}

#if defined(WITH_BLAZE)
void Utils::FDCoeffGen(const int sl, const int sr, const int pdo,
                       const double h, const bool neg, d_Vec& FDSpline)
{
  int j,k,n;
  double csign;
  d_Vec tCoeffs;
  blaze::DynamicMatrix<double> cMat, cMatInv;

  n = sr - sl + 1;
  tCoeffs.resize(n);
  cMat.resize(n,n);
  cMatInv.resize(n,n);

  if (neg)
  {
    csign = -1.0;
  } else {
    csign = 1.0;
  }

  for (k=0; k<n; ++k) 
  {
    Utils::TaylorCoeffs(sl,sr,(double)(sl+k),tCoeffs);
    for (j=0; j<n; ++j)
    {
      cMat(k,j) = tCoeffs[j];
    }
  }

  cMatInv = blaze::inv(cMat);

  for (j=0; j<n; ++j)
  {
    FDSpline[j] = csign*1.0/(std::pow(h,pdo))*cMatInv(pdo,j);
  } 
}
#else
void Utils::FDCoeffGen(const int sl, const int sr, const int pdo,
                       const double h, const bool neg, d_Vec& FDSpline)
{
  PetscInt j,k,n;
  PetscReal csign;
  d_Vec tCoeffs;
  Mat cMat, cMatFactor, cMatInv;
  const PetscScalar *matData;
  const PetscScalar one = 1.0, zero = 0.0;

  n = sr - sl + 1;
  tCoeffs.resize(n);

  MatCreateSeqDense(PETSC_COMM_SELF,n,n,NULL,&cMat);
  MatCreateSeqDense(PETSC_COMM_SELF,n,n,NULL,&cMatFactor);
  MatCreateSeqDense(PETSC_COMM_SELF,n,n,NULL,&cMatInv);

  if (neg)
  {
    csign = -1.0;
  } else {
    csign = 1.0;
  }

  for (k=0; k<n; ++k) 
  {
    Utils::TaylorCoeffs(sl,sr,(double)(sl+k),tCoeffs);
    for (j=0; j<n; ++j)
    {
      MatSetValue(cMat,j,k,(PetscScalar)tCoeffs[j],INSERT_VALUES);
      if (j ==k ) {
        MatSetValue(cMatFactor,j,k,one,INSERT_VALUES);
      } else {
        MatSetValue(cMatFactor,j,k,zero,INSERT_VALUES);
      }
    }
  }

  MatAssemblyBegin(cMat,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(cMat,MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(cMatFactor,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(cMatFactor,MAT_FINAL_ASSEMBLY);

  MatLUFactor(cMat,NULL,NULL,NULL);
  MatMatSolve(cMat,cMatFactor,cMatInv);

  MatDenseGetArrayRead(cMatInv,&matData);
  
  for (j=0; j<n; ++j)
  {
    FDSpline[j] = csign*1.0/(std::pow(h,pdo))*PetscRealPart(matData[pdo*n+j]);
  } 
  MatDenseRestoreArrayRead(cMatInv,&matData);

  MatDestroy(&cMatInv);
  MatDestroy(&cMatFactor);
  MatDestroy(&cMat);

}
#endif

#if defined(WITH_BLAZE)
void Utils::matInverse(const cc_Vec& matInput, cc_Vec& matInverse)
{
  blaze::DynamicMatrix<std::complex<double>,blaze::columnMajor> cMat, cMatInv;
  auto nRows = matInput.size();
  auto nCols = matInput[0].size();
  cMat.resize(nRows,nCols);
  cMatInv.resize(nRows,nCols);


  for (auto i = 0; i < nRows; ++i) {
    for (auto j = 0; j < nCols; ++j) {
      cMat(i,j) = matInput[i][j];
    }
  }

  cMatInv = blaze::inv(cMat);

  for (auto i = 0; i < nRows; ++i) {
    matInverse[i].resize(nCols);
    for (auto j = 0; j < nCols; ++j) {
      matInverse[i][j] = cMatInv(i,j);
    }
  }
}
#else
void Utils::matInverse(const cc_Vec& matInput, cc_Vec& matInverse)
{
  Mat cMat, cMatFactor, cMatInv;
  PetscInt nRows = matInput.size();
  PetscInt nCols = matInput[0].size();
  const PetscScalar one = 1.0;
  const PetscScalar* matData;

  MatCreateSeqDense(PETSC_COMM_SELF,nRows,nCols,NULL,&cMat);
  MatCreateSeqDense(PETSC_COMM_SELF,nRows,nCols,NULL,&cMatFactor);
  MatCreateSeqDense(PETSC_COMM_SELF,nRows,nCols,NULL,&cMatInv);

  for (auto i = 0; i < nRows; ++i) {
    MatSetValue(cMatFactor,i,i,one,INSERT_VALUES);
    for (auto j = 0; j < nCols; ++j) {
      MatSetValue(cMat,i,j,(PetscScalar)matInput[i][j],INSERT_VALUES);
    }
  }

  MatAssemblyBegin(cMat,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(cMat,MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(cMatFactor,MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(cMatFactor,MAT_FINAL_ASSEMBLY);

  MatLUFactor(cMat,NULL,NULL,NULL);
  MatMatSolve(cMat,cMatFactor,cMatInv);

  MatDenseGetArrayRead(cMatInv,&matData);

  for (auto i = 0; i < nRows; ++i) {
    matInverse[i].resize(nCols);
    for (auto j = 0; j < nCols; ++j) {
      matInverse[i][j] = matData[nRows*i+j];
    }
  }

  MatDenseRestoreArrayRead(cMatInv,&matData);

  MatDestroy(&cMatInv);
  MatDestroy(&cMatFactor);
  MatDestroy(&cMat);
}
#endif
