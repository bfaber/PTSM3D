#include <iostream>
#include <stdexcept>

#include <typeinfo>

#include "PTSM3D.hpp"
#include "DriftWave.hpp"
#include "Geom.hpp"
#include "HDF5Utils.hpp"

hid_t HDF5Utils::openHDF5File(const std::string filename) {
  hid_t fileID;
  fileID = H5Fcreate(filename.c_str(),H5F_ACC_TRUNC,H5P_DEFAULT,H5P_DEFAULT);
  return fileID;
}


void HDF5Utils::writeLinearSpectrum(const hid_t fileID, const PTSM3D* model) {
  herr_t status;
  hid_t groupID, modeID, evalID, dataID, dataspaceID, H5MemType;
  hsize_t dims[1];
  const std::vector<DriftWave>* spectrumPtr = model->getLinearSpectrumAddr();
  const auto spectrumSize = spectrumPtr->size();
  const auto nFields = spectrumPtr->at(0).getNFields();
  d_Vec kxMap = model->getKxMap();
  d_Vec kyMap = model->getKyMap();

  std::string groupStr = "/Spectrum";
  groupID = H5Gcreate(fileID,groupStr.c_str(),H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
  dims[0] = 1;

  const auto nkx = model->getNkx();
  dataspaceID = H5Screate_simple(1,dims,NULL);
  dataID = H5Dcreate(fileID,"Nkx",H5T_NATIVE_INT,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
  status = H5Dwrite(dataID,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&nkx);
  status = H5Dclose(dataID);

  const auto nky = model->getNky();
  dataID = H5Dcreate(fileID,"Nky",H5T_NATIVE_INT,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
  status = H5Dwrite(dataID,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&nky);
  status = H5Sclose(dataspaceID);
  status = H5Dclose(dataID);

  complex_t ctmp;
  hid_t complexType = H5Tcreate(H5T_COMPOUND,sizeof(ctmp));
  status = H5Tinsert(complexType,"r",HOFFSET(complex_t,re),H5T_NATIVE_DOUBLE);
  status = H5Tinsert(complexType,"i",HOFFSET(complex_t,im),H5T_NATIVE_DOUBLE);

  eval_t ev_temp;
  hid_t evalType = H5Tcreate(H5T_COMPOUND,sizeof(ev_temp));
  status = H5Tinsert(evalType,"u",HOFFSET(eval_t,unstable),complexType);
  status = H5Tinsert(evalType,"s",HOFFSET(eval_t,stable),complexType);
  status = H5Tinsert(evalType,"m",HOFFSET(eval_t,marginal),complexType);

  for (auto i=0; i<spectrumSize; ++i) {
    std::string modename = "kx_"+std::to_string(spectrumPtr->at(i).getKxInd())+"_ky_"+std::to_string(spectrumPtr->at(i).getKyInd());
    //DebugMsg(std::cout << "Opening group with mode name " << modename << '\n';);
    modeID = H5Gcreate(groupID,modename.c_str(),H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);

    dims[0] = 1;
    int kxInd = spectrumPtr->at(i).getKxInd();
    dataspaceID = H5Screate_simple(1,dims,NULL);
    dataID = H5Dcreate(modeID,"kxInd",H5T_NATIVE_INT,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    status = H5Dwrite(dataID,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&kxInd);
    status = H5Dclose(dataID);

    int kyInd = spectrumPtr->at(i).getKyInd();
    dataID = H5Dcreate(modeID,"kyInd",H5T_NATIVE_INT,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    status = H5Dwrite(dataID,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&kyInd);
    status = H5Dclose(dataID);

    int nModes = spectrumPtr->at(i).getNModes();
    dataID = H5Dcreate(modeID,"nModes",H5T_NATIVE_INT,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    status = H5Dwrite(dataID,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&nModes);
    status = H5Dclose(dataID);

    double kx = kxMap[kxInd];
    dataID = H5Dcreate(modeID,"kx",H5T_NATIVE_DOUBLE,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    status = H5Dwrite(dataID,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,&kx);
    status = H5Dclose(dataID);

    double ky = kyMap[kyInd];
    dataID = H5Dcreate(modeID,"ky",H5T_NATIVE_DOUBLE,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    status = H5Dwrite(dataID,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,&ky);
    status = H5Dclose(dataID);
    
    complex_t* initGuess = new complex_t;
    initGuess->re = std::real(spectrumPtr->at(i).getInitialGuess());
    initGuess->im = std::imag(spectrumPtr->at(i).getInitialGuess());
    dataID = H5Dcreate(modeID,"Initial Guess",complexType,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
    status = H5Dwrite(dataID,complexType,H5S_ALL,H5S_ALL,H5P_DEFAULT,initGuess);
    status = H5Sclose(dataspaceID);
    status = H5Dclose(dataID);
    delete initGuess;

    for (auto m = 0; m<nModes; ++m) {
      std::string evalnum = "Mode_"+std::to_string(m);
      evalID = H5Gcreate(modeID,evalnum.c_str(),H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
      dims[0] = 1;
      int nz = spectrumPtr->at(i).getNz(m);
      dataspaceID = H5Screate_simple(1,dims,NULL);
      dataID = H5Dcreate(evalID,"nz",H5T_NATIVE_INT,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
      status = H5Dwrite(dataID,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&nz);
      status = H5Dclose(dataID);

      auto  bk_avg = spectrumPtr->at(i).getAvgBk(m);
      dataID = H5Dcreate(evalID,"Bk",H5T_NATIVE_DOUBLE,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
      status = H5Dwrite(dataID,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,&bk_avg);
      status = H5Dclose(dataID);

      auto  Dk_avg = spectrumPtr->at(i).getAvgDk(m);
      dataID = H5Dcreate(evalID,"Dk",H5T_NATIVE_DOUBLE,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
      status = H5Dwrite(dataID,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,&bk_avg);
      status = H5Dclose(dataID);
 
      eval_t* eigVals = new eval_t;
      eigVals->unstable.re =std::real(spectrumPtr->at(i).getEigVal(m,0));
      eigVals->unstable.im =std::imag(spectrumPtr->at(i).getEigVal(m,0));
      eigVals->stable.re =std::real(spectrumPtr->at(i).getEigVal(m,1));
      eigVals->stable.im =std::imag(spectrumPtr->at(i).getEigVal(m,1));
      eigVals->marginal.re =std::real(spectrumPtr->at(i).getEigVal(m,2)); 
      eigVals->marginal.im =std::imag(spectrumPtr->at(i).getEigVal(m,2)); 
      dataID = H5Dcreate(evalID,"Eigenvalues",evalType,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
      status = H5Dwrite(dataID,evalType,H5S_ALL,H5S_ALL,H5P_DEFAULT,eigVals);
      status = H5Sclose(dataspaceID);
      status = H5Dclose(dataID);
      delete eigVals;

      double* eta = new double[nz];
      auto gZIdx1 = spectrumPtr->at(i).getGlobalZIdx1(m);
      auto gZIdx2 = spectrumPtr->at(i).getGlobalZIdx2(m);
      if (nz != (gZIdx2 - gZIdx1 + 1)) {
        status = H5Gclose(modeID);
        status = H5Gclose(groupID);
        status = H5Fclose(fileID);
        throw(std::runtime_error("PTMS3D Error! Vectors have incompatible sizes!"));
      }
      for (auto j = 0; j<nz; ++j) eta[j] = model->geom->eta[gZIdx1+j];
      dims[0] = nz;
      dataspaceID = H5Screate_simple(1,dims,NULL);
      dataID = H5Dcreate(evalID,"Eta",H5T_NATIVE_DOUBLE,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
      status = H5Dwrite(dataID,H5T_NATIVE_DOUBLE,H5S_ALL,H5S_ALL,H5P_DEFAULT,eta);
      status = H5Sclose(dataspaceID);
      status = H5Dclose(dataID);

      for (auto j=0; j<nFields; ++j) {
        complex_t* evec = new complex_t[nz];
        c_Vec eigVec = spectrumPtr->at(i).getEigVecField(m,j);
        for (auto k=0; k<nz; ++k) {
          evec[k].re = std::real(eigVec[k]);
          evec[k].im = std::imag(eigVec[k]);
        }
        dataspaceID = H5Screate_simple(1,dims,NULL);
        dataID = H5Dcreate(evalID,fieldnames[j].c_str(),complexType,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
        status = H5Dwrite(dataID,complexType,H5S_ALL,H5S_ALL,H5P_DEFAULT,evec);
        status = H5Sclose(dataspaceID);
        status = H5Dclose(dataID);
        delete[] evec;
      }

      status = H5Gclose(evalID);

      delete[] eta;
    }
    status = H5Gclose(modeID);
  } 
  status = H5Gclose(groupID);
  H5Tclose(complexType);
  H5Tclose(evalType);
} 

void HDF5Utils::writeTauCC(const hid_t fileID, const PTSM3D* model) {
  herr_t status;
  hid_t groupID, modeID, evalID, dataID, dataspaceID;
  hsize_t dims[1];
  const std::vector<DriftWave>* spectrumPtr = model->getLinearSpectrumAddr();
  const auto spectrumSize = spectrumPtr->size();
  const auto nFields = spectrumPtr->at(0).getNFields();
  d_Vec kxMap = model->getKxMap();
  d_Vec kyMap = model->getKyMap();

  std::string groupStr = "/Spectrum";
  groupID = H5Gopen(fileID,groupStr.c_str(),H5P_DEFAULT);
  dims[0] = 1;

  complex_t ctmp;
  hid_t complexType = H5Tcreate(H5T_COMPOUND,sizeof(ctmp));
  status = H5Tinsert(complexType,"r",HOFFSET(complex_t,re),H5T_NATIVE_DOUBLE);
  status = H5Tinsert(complexType,"i",HOFFSET(complex_t,im),H5T_NATIVE_DOUBLE);

  triplet_t trip;
  hid_t tripletType = H5Tcreate(H5T_COMPOUND,sizeof(trip));
  status = H5Tinsert(tripletType,"p",HOFFSET(triplet_t,mode1),H5T_NATIVE_INT);
  status = H5Tinsert(tripletType,"s",HOFFSET(triplet_t,mode2),H5T_NATIVE_INT);
  status = H5Tinsert(tripletType,"t",HOFFSET(triplet_t,mode3),H5T_NATIVE_INT);

  triplet_t sub_modes;
  hid_t subModeType = H5Tcreate(H5T_COMPOUND,sizeof(sub_modes));
  status = H5Tinsert(subModeType,"Mode1",HOFFSET(triplet_t,mode1),H5T_NATIVE_INT);
  status = H5Tinsert(subModeType,"Mode2",HOFFSET(triplet_t,mode2),H5T_NATIVE_INT);
  status = H5Tinsert(subModeType,"Mode3",HOFFSET(triplet_t,mode3),H5T_NATIVE_INT);

  kperp_t kperp_tuple;
  hid_t kPerpType = H5Tcreate(H5T_COMPOUND,sizeof(kperp_tuple));
  status = H5Tinsert(kPerpType,"kx1",HOFFSET(kperp_t,kx1),H5T_NATIVE_INT);
  status = H5Tinsert(kPerpType,"ky1",HOFFSET(kperp_t,ky1),H5T_NATIVE_INT);
  status = H5Tinsert(kPerpType,"kx2",HOFFSET(kperp_t,kx2),H5T_NATIVE_INT);
  status = H5Tinsert(kPerpType,"ky2",HOFFSET(kperp_t,ky2),H5T_NATIVE_INT);
  status = H5Tinsert(kPerpType,"kx3",HOFFSET(kperp_t,kx3),H5T_NATIVE_INT);
  status = H5Tinsert(kPerpType,"ky3",HOFFSET(kperp_t,ky3),H5T_NATIVE_INT);

  tauCC_t tauCC_tuple;
  hid_t tauCCType = H5Tcreate(H5T_COMPOUND,sizeof(tauCC_tuple));
  status = H5Tinsert(tauCCType,"tauCC",HOFFSET(tauCC_t,tauCC),H5T_NATIVE_DOUBLE);
  status = H5Tinsert(tauCCType,"tau",HOFFSET(tauCC_t,tau),complexType);
  status = H5Tinsert(tauCCType,"CC",HOFFSET(tauCC_t,CC),complexType);

  for (auto i=0; i<spectrumSize; ++i) {
    std::string modename = "kx_"+std::to_string(spectrumPtr->at(i).getKxInd())+"_ky_"+std::to_string(spectrumPtr->at(i).getKyInd());
    modeID = H5Gopen(groupID,modename.c_str(),H5P_DEFAULT);

    const int nModes = spectrumPtr->at(i).getNModes();
    if (nModes > 0) {
      for (auto m=0; m<nModes; ++m) {
        std::string evalnum = "Mode_"+std::to_string(m);
        evalID = H5Gopen(modeID,evalnum.c_str(),H5P_DEFAULT);
        triplet_t* triplet = new triplet_t;
        auto triple = spectrumPtr->at(i).getMaxTriplet(m);
        triplet->mode1 = std::get<0>(triple);
        triplet->mode2 = std::get<1>(triple);
        triplet->mode3 = std::get<2>(triple);
        dataspaceID = H5Screate_simple(1,dims,NULL);
        dataID = H5Dcreate(evalID,"Max Triplet",tripletType,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
        status = H5Dwrite(dataID,tripletType,H5S_ALL,H5S_ALL,H5P_DEFAULT,triplet);
        status = H5Dclose(dataID);

        triple = spectrumPtr->at(i).getSubModeTriple(m);
        triplet->mode1 = std::get<0>(triple);
        triplet->mode2 = std::get<1>(triple);
        triplet->mode3 = std::get<2>(triple);
        dataID = H5Dcreate(evalID,"Sub Mode Triple",subModeType,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
        status = H5Dwrite(dataID,subModeType,H5S_ALL,H5S_ALL,H5P_DEFAULT,triplet);
        status = H5Dclose(dataID);
        delete triplet;
     
        int zkx2 = spectrumPtr->at(i).getMaxZonalKx2(m);
        dataID = H5Dcreate(evalID,"Max Zonal Kx2",H5T_NATIVE_INT,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
        status = H5Dwrite(dataID,H5T_NATIVE_INT,H5S_ALL,H5S_ALL,H5P_DEFAULT,&zkx2);
        status = H5Dclose(dataID);
        
        kperp_t* kPerpTuple = new kperp_t;
        auto kPerpInds = spectrumPtr->at(i).getKPerpTriplet(m);
        kPerpTuple->kx1 = std::get<0>(kPerpInds);
        kPerpTuple->ky1 = std::get<1>(kPerpInds);
        kPerpTuple->kx2 = std::get<2>(kPerpInds);
        kPerpTuple->ky2 = std::get<3>(kPerpInds);
        kPerpTuple->kx3 = std::get<4>(kPerpInds);
        kPerpTuple->ky3 = std::get<5>(kPerpInds);
        dataID = H5Dcreate(evalID,"Max Non-Zonal K_perp",kPerpType,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
        status = H5Dwrite(dataID,kPerpType,H5S_ALL,H5S_ALL,H5P_DEFAULT,kPerpTuple);
        status = H5Dclose(dataID);
        delete kPerpTuple;
        
        tauCC_t* tauCCTuple = new tauCC_t;
        auto tauCC_vals = spectrumPtr->at(i).getZonalTauCCTuple(m);
        tauCCTuple->tauCC = std::get<0>(tauCC_vals);
        tauCCTuple->tau.re = std::real(std::get<1>(tauCC_vals));
        tauCCTuple->tau.im = std::imag(std::get<1>(tauCC_vals));
        tauCCTuple->CC.re = std::real(std::get<2>(tauCC_vals));
        tauCCTuple->CC.im = std::imag(std::get<2>(tauCC_vals));
        dataID = H5Dcreate(evalID,"Zonal TauCC",tauCCType,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
        status = H5Dwrite(dataID,tauCCType,H5S_ALL,H5S_ALL,H5P_DEFAULT,tauCCTuple);
        status = H5Dclose(dataID);

        tauCC_vals = spectrumPtr->at(i).getNonZonalTauCCTuple(m);
        tauCCTuple->tauCC = std::get<0>(tauCC_vals);
        tauCCTuple->tau.re = std::real(std::get<1>(tauCC_vals));
        tauCCTuple->tau.im = std::imag(std::get<1>(tauCC_vals));
        tauCCTuple->CC.re = std::real(std::get<2>(tauCC_vals));
        tauCCTuple->CC.im = std::imag(std::get<2>(tauCC_vals));
        dataID = H5Dcreate(evalID,"Non-Zonal TauCC",tauCCType,dataspaceID,H5P_DEFAULT,H5P_DEFAULT,H5P_DEFAULT);
        status = H5Dwrite(dataID,tauCCType,H5S_ALL,H5S_ALL,H5P_DEFAULT,tauCCTuple);
        status = H5Dclose(dataID);
        status = H5Sclose(dataspaceID);
        delete tauCCTuple;

        status = H5Gclose(evalID);
      }
    }
    status = H5Gclose(modeID);
  } 
  status = H5Gclose(groupID);
  H5Tclose(complexType);
  H5Tclose(tripletType);
  H5Tclose(subModeType);
  H5Tclose(kPerpType);
  H5Tclose(tauCCType);
} 
