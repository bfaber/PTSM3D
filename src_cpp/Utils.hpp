#ifndef UTILS_HPP
#define UTILS_HPP

#include <vector>
#include <complex>
#include <cmath>
#include <algorithm>
#include <cctype>
#include <locale>

#include "Definitions.hpp"

namespace Utils
{
  constexpr auto pi() { return 3.1415926535897932846264338327950; };
  int factorial(const int);
  void FDCoeffGen(const int, const int, const int, const double, const bool, d_Vec&);
  void FDCoeffGen(const int, const int, const int, const double, d_Vec&);
  void FDCoeffGen(const int, const int, const int, const double, c_Vec&);
  void TaylorCoeffs(const int, const int, const double, d_Vec&);
  void matInverse(const cc_Vec& matInput, cc_Vec& matInverse);

/*
### gaussianMode()

Creates a vector with Gaussian structure to represent a strongly ballooned mode along
a field line

**Parameters**
  - vecSize: the size of the vector desired
  - delta: the width of the Gaussian at full-width half-maximum (in units of pi)
  - dz: the parallel resolution (in units of pi)

**Returns**
  - a vector with complex double-type elements representing the real part of the Gaussian mode

```c++
d_Vec gaussianMode(const int vecSize);
```

*/
    c_Vec gaussianMode(const int vecSize, const double delta, const double dz);


    // Useful functions for dealing with strings
    // These functions were written by Evan Teran and found at
    // https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
    // trim from start (in place)
    static inline void ltrim(std::string &s) {
        s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
            return !std::isspace(ch);
        }));
    }

    // trim from end (in place)
    static inline void rtrim(std::string &s) {
        s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
            return !std::isspace(ch);
        }).base(), s.end());
    }

    // trim from both ends (in place)
    static inline void trim(std::string &s) {
        ltrim(s);
        rtrim(s);
    }

    // trim from start (copying)
    static inline std::string ltrim_copy(std::string s) {
        ltrim(s);
        return s;
    }

    // trim from end (copying)
    static inline std::string rtrim_copy(std::string s) {
        rtrim(s);
        return s;
    }

    // trim from both ends (copying)
    static inline std::string trim_copy(std::string s) {
        trim(s);
        return s;
    }
}
#endif
