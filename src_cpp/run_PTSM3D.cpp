#include <string>
#include <iostream>

#include "slepc.h"

#include "PTSM3D.hpp"

int main(int argc, char *argv[])
{
  //DebugMsg(std::cout << "Initializing\n";);
  std::string inFile("PTSM3D.inp");
  SlepcInitialize(&argc,&argv,(char*)0,(char*)0);
  PTSM3D SatModel;
  //DebugMsg(std::cout << "Reading input file\n";);
  SatModel.readInput(argv[1]);
  //DebugMsg(std::cout << "Finished reading input file\n";);
  MPI_Barrier(PETSC_COMM_WORLD);
  //DebugMsg(std::cout << "Setting up PTSM3D\n";);
  SatModel.setupPTSM3D();
  //DebugMsg(std::cout << "Finished setting up PTSM3D\n";);
  MPI_Barrier(PETSC_COMM_WORLD);
  SatModel.computeLinearSpectrum();
  
  if (SatModel.getNkx() > 2 && SatModel.getNky() > 3) SatModel.computeTauCC();
 
  SatModel.writeModelData();
  
  SlepcFinalize();
  //DebugMsg(std::cout << "Finalized" << '\n';);
  return 0;
}
