#ifndef DRIFTWAVE_HPP
#define DRIFTWAVE_HPP

#include <string>
#include <exception>
#include <tuple>
#include <utility>

#include "Definitions.hpp"

class PTSM3D;

class DriftWave
{
  public:
    inline DriftWave() {};
    ~DriftWave();
    DriftWave(PTSM3D* satModel);
    DriftWave(const int kxInd, const int kyInd, const int nModes, const int nFields, PTSM3D* const ptsm3dPtr);
    DriftWave copy(const DriftWave& dw);
    DriftWave& operator=(const DriftWave& dw);
    void addEmptyEntry();
    void mirrorOverZero(const int modeID);
    static void computeZonalCoupling(DriftWave& dw1, const DriftWave& dw2, const int kxIdx3, const bool negKx2);
    static void computeNonZonalCoupling(DriftWave& dw1, const DriftWave& dw2, const DriftWave& dw3, const std::tuple<bool,bool,bool,bool> negKSign);

    inline void setSatModel(PTSM3D* const extPtsm3dPtr) { _satModel = extPtsm3dPtr; };
    inline PTSM3D* getSatModel() const { PTSM3D* ptsm3dPtr = _satModel; return ptsm3dPtr; };
    inline const c_Vec* getEigVecAddr(const int modeID) const { const c_Vec* ptr = &_eigVecs.at(modeID); return ptr; };
    inline void setInitCondVec(const Vec in_vec) { VecCopy(in_vec,_initCondVec); };
    inline Vec* getInitCondVecAddr() { return &_initCondVec; };
    inline const Vec* getInitCondVecConstAddr() const { const Vec* ptr = &_initCondVec; return ptr; };

    static c_double convolve(const DriftWave& dw1, const int modeID1, const DriftWave& dw2, const int modeID2, const int fieldID, const bool negKx2);
    static c_double convolve(const DriftWave& dw1, const int modeID1, const DriftWave& dw2, const int modeID2, const DriftWave& dw3, const int modeID3, const int fieldID, const bool negKx2, const bool negKx3);
    c_double fieldlineAverage(const c_Vec& vec, const int modeID, const int fieldID);
    c_double fieldlineAverage(const d_Vec& vec, const int modeID, const int fieldID);
    void computeAvgBkDk(const int modeID);
    void computeCrossBk(const int modeID);

    // Inline functions for dealing with setting and getting variables
    void setEigVal(const int modeID, const int fieldID, const c_double& eval);
    void setEigVecField(const int modeID, const int fieldID, const c_Vec& evec);

    c_double getEigVal(const int modeID, const int fieldID) const;
    void getEigVal(const int modeID, const int fieldID, c_double& eval);
    c_Vec getEigVecField(const int modeID, const int fieldID) const;
    void getEigVecField(const int modeID, const int fieldID, c_Vec& evec);
    const c_double* getEigVecFieldData(const int fieldID) const;
    const c_double* getEigValData() const;

    inline void setKxInd(const int x) { _kxInd = x; };
    inline void setKyInd(const int x) { _kyInd = x; };
    inline void setNFields(const int x) { _nFields = x; };
    void setNModes(const int x);

    int kInds(const int ix, const int iy);

    inline void setZIndices(const int modeID, const std::tuple<int,int,int,int,int> x) { _zIndices.at(modeID) = x; };
    inline void setZIndices(const int modeID, const int nz, const int lz1, const int lz2, const int gz1, const int gz2) { _zIndices.at(modeID) = std::make_tuple(nz,lz1,lz2,gz1,gz2); };
    inline void setInitialGuess(const c_double x) { _initialGuess = x; };
    inline void setEigVals(const int modeID, const c_Vec& x) { _eigVals.at(modeID) = x; };
    inline void setEigVec(const int modeID, const c_Vec& x) { _eigVecs.at(modeID) = x; };
    inline void setAvgBkDk(const int modeID, const std::tuple<double,double> x) { _avgBkDk.at(modeID) = x; };
    inline void setAvgBkDk(const int modeID, const double BkAvg, const double DkAvg) { _avgBkDk.at(modeID) = std::make_tuple(BkAvg,DkAvg); };
    inline void setMassMat(const int modeID, const c_Vec& x) { _massMat.at(modeID) = x; };
    inline void setMassMatInv(const int modeID, const c_Vec& x) { _massMatInv.at(modeID) = x; };
    inline void setResonantKxZonal(const int modeID, const int x) { _resonantKxZonal.at(modeID) = x; };
    inline void setResonantKNonZonal(const int modeID, const std::tuple<int,int,int,int> x) { _resonantKNonZonal.at(modeID) = x; };
    inline void setResonantKNonZonal(const int modeID, const int kx2, const int ky2, const int kx3, const int ky3) { _resonantKNonZonal.at(modeID) = std::make_tuple(kx2,ky2,kx3,ky3); };
    inline void setMaxTriplet(const int modeID, const std::tuple<int,int,int>& x) { _maxTriplet.at(modeID) = x; };
    inline void setSubModeTriple(const int modeID, const std::tuple<int,int,int>& x) { _subModeTriple.at(modeID) = x; };
    void setZonalTauCC(const int modeID, const std::tuple<double,c_double,c_double> x);
    void setZonalTauCC(const int modeID, const double x1, const c_double x2, const c_double x3);
    void setNonZonalTauCC(const int modeID, const std::tuple<double,c_double,c_double> x);
    void setNonZonalTauCC(const int modeID, const double x1, const c_double x2, const c_double x3);
    void setKPerpTriplet(const int modeID, const std::tuple<int,int,int,int,int,int> x);

    inline auto getKxInd() const { return _kxInd; };
    inline auto getKyInd() const { return _kyInd; };
    inline auto getNFields() const { return _nFields; };
    inline auto getNModes() const { return _nModes; };
    inline auto getGlobalZIdx1(const int modeID) const { return std::get<1>(_zIndices.at(modeID)); };
    inline auto getGlobalZIdx2(const int modeID) const { return std::get<2>(_zIndices.at(modeID)); };
    inline auto getMirrorGlobalZIdx1(const int modeID) const { return std::get<3>(_zIndices.at(modeID)); };
    inline auto getMirrorGlobalZIdx2(const int modeID) const { return std::get<4>(_zIndices.at(modeID)); };
    inline auto getNz(const int modeID) const { return std::get<0>(_zIndices.at(modeID)); };
    inline auto getZIndices(const int modeID) const { return _zIndices.at(modeID); };
    inline auto getInitialGuess() const { return _initialGuess; };
    inline auto getAvgBk(const int modeID) const { return std::get<0>(_avgBkDk.at(modeID)); };
    inline auto getAvgDk(const int modeID) const { return std::get<1>(_avgBkDk.at(modeID)); };
    inline auto getAvgBkDk(const int modeID) const { return _avgBkDk.at(modeID); };
    inline auto getEigVals(const int modeID) const { return _eigVals.at(modeID); };
    inline auto getEigVec(const int modeID) const { return _eigVecs.at(modeID); };
    inline auto getMassMat(const int modeID) const { return _massMat.at(modeID); };
    inline auto getMassMatInv(const int modeID) const { return _massMatInv.at(modeID); };
    inline auto getMassMatEntry(const int modeID,const int idx1, const int idx2) const { return _massMat.at(modeID).at(idx1*_nFields+idx2); };
    inline auto getMassMatInvEntry(const int modeID, const int idx1, const int idx2) const { return _massMatInv.at(modeID).at(idx1*_nFields+idx2); };
    

    double getZonalTauCC(const int modeID) const;
    c_double getZonalTau(const int modeID) const;
    c_double getZonalCC(const int modeID) const;
    double getNonZonalTauCC(const int modeID) const;
    c_double getNonZonalTau(const int modeID) const;
    c_double getNonZonalCC(const int modeID) const;
    int getMaxNonZonalKx2(const int modeID) const;
    int getMaxNonZonalKx3(const int modeID) const;
    int getMaxNonZonalKy2(const int modeID) const;
    int getMaxNonZonalKy3(const int modeID) const;
    int getMaxZonalKx2(const int modeID) const;
    std::tuple<int,int,int> getMaxTriplet(const int modeID) const;
    std::tuple<int,int,int> getSubModeTriple(const int modeID) const;
    std::tuple<int,int,int,int,int,int> getKPerpTriplet(const int modeID) const;
    std::tuple<double,c_double,c_double> getZonalTauCCTuple(const int modeID) const;
    std::tuple<double,c_double,c_double> getNonZonalTauCCTuple(const int modeID) const;

    inline auto triplets() const { return _triplets; };
  private:
    PTSM3D* _satModel;
    int _kxInd;
    int _kyInd;
    int _nFields;
    int _nModes;
    std::vector<std::tuple<int,int,int>> _triplets;
    std::vector<std::tuple<int,int,int,int,int>> _zIndices;
    c_double _initialGuess;
    Vec _initCondVec;
    std::vector<std::tuple<double,double>> _avgBkDk;
    c_Vec _crossBk;
    cc_Vec _eigVals;
    cc_Vec _eigVecs;
    cc_Vec _massMat;
    cc_Vec _massMatInv;
    std::vector<std::tuple<int,int,int>> _maxTriplet;
    std::vector<std::tuple<int,int,int>> _subModeTriple;
    std::vector<std::tuple<int,int,int,int,int,int>> _kPerpTriplet;
    i_Vec _resonantFreqsZonal;
    i_Vec _resonantKxZonal;
    std::vector<std::tuple<double,c_double,c_double>> _zonalTauCC;
    std::vector<std::tuple<double,double>> _resonantFreqsNonZonal;
    std::vector<std::tuple<int,int,int,int>> _resonantKNonZonal;
    std::vector<std::tuple<double,c_double,c_double>> _nonZonalTauCC;

};

#endif
