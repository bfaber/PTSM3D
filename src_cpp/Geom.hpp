#ifndef GEOM_HPP
#define GEOM_HPP

#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

#include "Definitions.hpp"

class Geom;
// Define a function pointer to Geom class functions that takes a vector of doubles and returns void
using geomFuncPtr = void(Geom::*)(const d_Vec&);
using geomDataPtr = d_Vec(Geom::*)();

class Geom
{
  public:
    // Public member variables
    d_Vec gxx, gxy, gyy, modB, jac, dBdx, dBdy, dBdz, eta, modBInv, jacBInv,
          dparModBInv, dpar2ModBInv, dparJacBInv;
    Geom();
    void setGeomSize(const int nz);

    inline void setGeomField(const d_Vec& fieldData, geomFuncPtr f) { (this->*f)(fieldData); };
    inline void setGxx(const d_Vec& x) { this->gxx = x; };
    inline void setGxy(const d_Vec& x) { this->gxy = x; };
    inline void setGyy(const d_Vec& x) { this->gyy = x; };
    inline void setModB(const d_Vec& x) { this->modB = x; };
    inline void setJac(const d_Vec& x) { this->jac = x; };
    inline void setDBdx(const d_Vec& x) { this->dBdx = x; };
    inline void setDBdy(const d_Vec& x) { this->dBdy = x; };
    inline void setDBdz(const d_Vec& x) { this->dBdz = x; };
    inline void setEta(const d_Vec& x) { this->eta = x; };

    inline d_Vec getGeomField(geomDataPtr f) { return (this->*f)(); };
    inline d_Vec getGxx() { return this->gxx; };
    inline d_Vec getGxy() { return this->gxy; };
    inline d_Vec getGyy() { return this->gyy; };
    inline d_Vec getModB() { return this->modB; };
    inline d_Vec getJac() { return this->jac; };
    inline d_Vec getDBdx() { return this->dBdx; };
    inline d_Vec getDBdy() { return this->dBdy; };
    inline d_Vec getDBdz() { return this->dBdz; };
    inline d_Vec getEta() { return this->eta; };


    // Functions for computing model quantites, like Bk, Dk and the parallel derivatives of B

    // buildBk: compute the polarization term Bk for a given (kx,ky) over a parallel range
    // specific by the indices idx1 (low) and idx2(high)
    // Fills the result in the preallocated vector vecBk
    void buildBk(const double kx, const double ky, const int idx1, const int idx2, d_Vec& vecBk);
    void buildCrossBk(const double kx1, const double ky1, const double kx2, const double ky2, const int idx1, const int idx2, d_Vec& vecBkpk);

    // buildDk: compute the curvature term Dk for a given (kx,ky) over a parallel range
    // specific by the indices idx1 (low) and idx2(high)
    // Fills the result in the preallocated vector vecDk
    void buildDk(const double kx, const double ky, const int idx1, const int idx2,
                 d_Vec& vecDk);
    int findZeroIndex();

    void computeDerivedQuantities(const int errOrder);
   // void callVmec2Pest(std::string eqFile, double surface, const double dky, const int nz0, const int buffer);

    // Function for computing the s-alpha geometry of np poloidal turns
    void s_alpha(const double q0, const double shat, const double invEps, const double np);

    // Routines for accessing private variables
    inline void setq0(const double x) { q0 = x; };
    inline void setShat(const double x) { shat = x; };
    inline void setInvEps(const double x) { invEps = x; };
    inline void setDEta(const double x) { dEta = x; };
    inline void setNPts(const int x) { nPts = x; };
    inline void setNfp(const int x) { nfp = x; };
    inline auto getq0() const { return q0; };
    inline auto getShat() const { return shat; };
    inline auto getInvEps() const { return invEps; };
    inline auto getNPts() const { return nPts; } ;
    inline auto getDEta() const { return dEta; } ;
    inline auto getNfp() const { return nfp; };

  private:
    double q0, shat, invEps, dEta;
    int nPts, nfp;
    std::string geomFile;

    // This map maps string values that are needed to get data from vmec2pest
    // with function pointers pointing to the function necessary to set the data
    std::unordered_map<std::string,geomFuncPtr> v2p_DiagMap;
};
#endif
