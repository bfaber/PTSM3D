#include <iostream>
#include <memory>

#include "FluidModel.hpp"
#include "Definitions.hpp"
#include "ITG3Field.hpp"
#include "KBM5Field.hpp"
#include "Geom.hpp"

FluidModel::FluidModel() {}

FluidModel::FluidModel(const int n_fields) : nFields{n_fields}
{
}

FluidModel::~FluidModel() {}

void FluidModel::setNFields(const int x) { 
  if (x > 0) { 
    nFields = x; 
  } else { 
    throw std::domain_error("nFields <= 0!");
  }
}

std::shared_ptr<FluidModel> FluidModel::getFluidPtr() {
  return shared_from_this();
}

void FluidModel::setModelPtr(const std::shared_ptr<ITG3Field>& ptr) {
  modelITG3 = ptr;
}


void FluidModel::setModelPtr(const std::shared_ptr<KBM5Field>& ptr) {
  modelKBM5 = ptr;
}

c_double FluidModel::getEigVal(const int modeID, const int fieldID) const
{
  return eigVals.at(modeID).at(fieldID);
}


void FluidModel::getEigVal(const int modeID, const int fieldID, c_double& ev)
{
  ev = eigVals.at(modeID).at(fieldID);
}

c_Vec FluidModel::getEigVecField(const int modeID, const int fieldID) const
{
  c_Vec evec(nz.at(modeID));
  std::copy(eigVecs.at(modeID).begin()+fieldID*nz.at(modeID),eigVecs.at(modeID).begin()+(fieldID+1)*nz.at(modeID),evec.begin());
  return evec;
}

void FluidModel::getEigVecField(const int modeID, const int fieldID, c_Vec& evec)
{
  evec.resize(nz.at(modeID));
  std::copy(eigVecs.at(modeID).begin()+fieldID*nz.at(modeID),eigVecs.at(modeID).begin()+(fieldID+1)*nz.at(modeID),evec.begin());
}

c_Vec FluidModel::getEigVals(const int modeID) const
{
  return eigVals.at(modeID);
}

void FluidModel::getEigVals(const int modeID, c_Vec& evs)
{
  evs = eigVals.at(modeID);
}

c_Vec FluidModel::getEigVec(const int modeID) const
{
  return eigVecs.at(modeID);
}

void FluidModel::getEigVec(const int modeID, c_Vec& evec)
{
  evec = eigVecs.at(modeID);
}

c_Vec FluidModel::getMassMat(const int modeID) const
{
  return massMat.at(modeID);
}

c_Vec FluidModel::getMassMatInv(const int modeID) const
{
  return massMatInv.at(modeID);
}

void FluidModel::getMassMat(const int modeID, c_Vec& mat)
{
  mat = massMat.at(modeID);
}

void FluidModel::getMassMatInv(const int modeID, c_Vec& mat)
{
  mat = massMatInv.at(modeID);
}

c_Vec FluidModel::getDispersionCoeffs(const int modeID) const {
  return dispersionCoeffs.at(modeID);
}

void FluidModel::getDispersionCoeffs(const int modeID, c_Vec& vec) {
  vec.resize(nFields);
  std::copy(dispersionCoeffs.at(modeID).begin(),dispersionCoeffs.at(modeID).end(),vec.begin()); 
}

void FluidModel::setModelParameters()
{
  StringMap::iterator iter;
  
  iter = params->find("omt");
  omt = iter != params->end() ? std::stod(iter->second) : 3.0;

  iter = params->find("omn");
  omn = iter != params->end() ? std::stod(iter->second) : 0.0;

  iter = params->find("tauTemp");
  tauTemp = iter != params->end() ? std::stod(iter->second) : 1.0;

  iter = params->find("beta");
  beta = iter != params->end() ? std::stod(iter->second) : 0.0;

  iter = params->find("buffer");
  buffer = iter != params->end() ? std::stoi(iter->second) : 20;

  iter = params->find("errOrder");
  errOrder = iter != params->end() ? std::stoi(iter->second) : 2;

  iter = params->find("hypzOrder");
  hypzOrder = iter != params->end() ? std::stoi(iter->second) : 2;

  iter = params->find("hypzEps");
  hypzEps = iter != params->end() ? std::stod(iter->second) : 0.25;
}

void FluidModel::duplicatePetscVec(Vec* newVec) {
  VecDuplicate(initCondVec,newVec);
}

Vec FluidModel::getInitCondVec() const {
  return initCondVec;
}
void FluidModel::solve(SlepcUtils::SlepcContext& sc)
{
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
    ptr->solve(sc);
  } else if (nFields == 5) {
    auto ptr = modelKBM5.lock();
    ptr->solve(sc);
  }
}

void FluidModel::createSlepcSolver() {
  EPSCreate(PETSC_COMM_WORLD,&eps);
  PEPCreate(PETSC_COMM_WORLD,&pep);
}

void FluidModel::destroySlepcSolver() {
  EPSDestroy(&eps);
  PEPDestroy(&pep);
}


void FluidModel::createLinearOperators(SlepcUtils::SlepcContext& sc) {
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
    ptr->createLinearOperators(sc);
  } else if (nFields == 5) {
    auto ptr = modelKBM5.lock();
    ptr->createLinearOperators(sc);
  }
}

void FluidModel::destroyLinearOperators(const SlepcUtils::SlepcContext& sc) {
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
    ptr->destroyLinearOperators(sc);
  } else if (nFields == 5) {
    auto ptr = modelKBM5.lock();
    ptr->destroyLinearOperators(sc);
  }
}
void FluidModel::zeroLinearOperators(const SlepcUtils::SlepcContext& sc, const bool setVecToZero) {
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
    ptr->zeroLinearOperators(sc, setVecToZero);
  } else if (nFields == 5) {
    auto ptr = modelKBM5.lock();
    ptr->zeroLinearOperators(sc, setVecToZero);
  }
}
void FluidModel::buildLinearOperators(SlepcUtils::SlepcContext& sc, const double kx, const double ky) {
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
    ptr->buildLinearOperators(sc, kx, ky);
  } else if (nFields == 5) {
    auto ptr = modelKBM5.lock();
    ptr->buildLinearOperators(sc, kx, ky);
  }
}
void FluidModel::extractSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky) {
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
    ptr->extractSolution(sc, kx, ky);
  } else if (nFields == 5) {
    auto ptr = modelKBM5.lock();
    ptr->extractSolution(sc, kx, ky);
  }
}

PetscScalar FluidModel::guessTarget(const double kx, const double ky) {
  PetscScalar targetGuess;
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
     targetGuess = ptr->guessTarget(kx, ky);
  } else if (nFields == 5) {
    auto ptr = modelKBM5.lock();
    targetGuess = ptr->guessTarget(kx, ky);
  }
  return targetGuess;
}

void FluidModel::extractInitCond(const int idx) {
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
    return ptr->extractInitCond(idx);
  }
}

void FluidModel::computeMassMatrix(const int modeID, const double ky, const c_Vec& dispRoots) {
  if (nFields == 3) {
    auto ptr = modelITG3.lock();
    ptr->computeMassMatrix(modeID, ky, dispRoots);
  } else if (nFields == 5) {
    auto ptr = modelKBM5.lock();
    ptr->computeMassMatrix(modeID, ky, dispRoots);
  }
}
