#ifndef FLUIDMODEL_HPP
#define FLUIDMODEL_HPP
#include <memory>

#include "blaze/math/DynamicMatrix.h"

#include "Definitions.hpp"
#include "SlepcUtils.hpp"
#include "HDF5Utils.hpp"

class PTSM3D; class Geom; class ITG3Field; class KBM5Field;

class FluidModel : public std::enable_shared_from_this<FluidModel>
{
public:
  FluidModel();
  FluidModel(const int nFields);
  virtual ~FluidModel();

  //virtual void setup();
  void setModelPtr(const std::shared_ptr<ITG3Field>& ptr);
  void setModelPtr(const std::shared_ptr<KBM5Field>& ptr);
  std::shared_ptr<FluidModel> getFluidPtr();
  virtual void setModelParameters();
/********************************************************************************
  SLEPc eigensolver functions
********************************************************************************/
  // Wrapper for solving the eigensystem
  virtual void solve(SlepcUtils::SlepcContext& sc);
  virtual void createSlepcSolver();
  virtual void destroySlepcSolver();

  // Provides an initial guess of the target
  virtual PetscScalar guessTarget(const double kx, const double ky);
  virtual void extractInitCond(const int idx);
  //virtual PetscScalar guessTarget(const Vec& initVec);

  // Operations for building the linear operators
  virtual void createLinearOperators(SlepcUtils::SlepcContext& sc);
  virtual void destroyLinearOperators(const SlepcUtils::SlepcContext& sc);
  virtual void zeroLinearOperators(const SlepcUtils::SlepcContext& sc, const bool setVecToZero);
  virtual void buildLinearOperators(SlepcUtils::SlepcContext& sc, const double kx, const double ky);
  virtual void extractSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky);
  virtual void computeMassMatrix(const int modeID, const double ky, const c_Vec& dispRoots);

  // Function to build the EPS problem, only called by buildLinearOperators()
  //virtual void buildEPSOperators(const double kx, const double ky);
  //virtual void extractEPSSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky);

  void duplicatePetscVec(Vec* vec);
  Vec getInitCondVec() const;

  c_double getEigVal(const int modeID, const int fieldID) const; // Access the eigenvalue, returns complex<double>
  void getEigVal(const int modeID, const int fieldID, c_double& ev);

  c_Vec getEigVals(const int modeID) const;
  void getEigVals(const int modeID, c_Vec& evs);

  c_Vec getEigVecField(const int modeID, const int fieldID) const; // Access the eigenvector, returns c_Vec
  void getEigVecField(const int modeID, const int fieldID, c_Vec& vec);

  c_Vec getEigVec(const int modeID) const;
  void getEigVec(const int modeID, c_Vec& vec);

  c_Vec getMassMat(const int modeID) const;
  c_Vec getMassMatInv(const int modeID) const;
  void getMassMat(const int modeID, c_Vec& massMat);
  void getMassMatInv(const int modeID, c_Vec& massMatInv);

  c_Vec getDispersionCoeffs(const int modeID) const;
  void getDispersionCoeffs(const int modeID, c_Vec& vec);

  inline void setErrOrder(const int x) { if (x > 0) { errOrder = x; } else { throw std::domain_error("errOrder <= 0!");} };
  inline void setHypzOrder(const int x) { if (x > 0) { hypzOrder = x; } else {throw std::domain_error("hypzOrder <= 0!");} };
  inline void setHypzEps(const double x) { hypzEps = x; };

  void setNFields(const int x);
  inline void setZBoundUpper(const int x) { if (x >= 0) { zBoundUpper = x; } else { throw std::domain_error("zBoundUpper < 0!");} };
  inline void setZBoundLower(const int x) { if (x >= 0) { zBoundLower = x; } else { throw std::domain_error("zBoundLower < 0!");} };
  inline void setBuffer(const int x) { if (x > 0) { buffer = x; } else { throw std::domain_error("buffer < 0!"); } };
  inline void setProbSize(const int x) { if (x > 0) { probSize = x; } else { throw std::domain_error("probSize <= 0!"); } };
  inline void setNz(const int modeID, const int x) { if (x > 0) { nz[modeID] = x; } else { throw std::domain_error("vecSize <= 0!"); } };
  inline void setEvIdx(const int modeID, const int x) { evIndices.at(modeID) = x; };

  inline auto getZBoundUpper() const { return zBoundUpper; };
  inline auto getZBoundLower() const { return zBoundLower; };
  inline auto getProbSize() const { return probSize; };
  inline auto getBuffer() const { return buffer; };

  inline auto getNFields() const { return nFields; };
  inline auto getNModes() const { return nModes; };
  inline auto getErrOrder() const { return errOrder; };
  inline auto getHypzOrder() const { return hypzOrder; };
  inline auto getHypzEps() const { return hypzEps; };

  inline auto getZIdx1(const int modeID) const { return zIdx1[modeID]; };
  inline auto getZIdx2(const int modeID) const { return zIdx2[modeID]; };
  inline auto getNz(const int modeID) const { return nz[modeID]; };

  inline auto getEvIdx(const int modeID) const { return evIndices[modeID]; };

  inline void setTempGrad(const double x) { omt = x; };
  inline void setDensGrad(const double x) { omn = x; };
  inline void setTempRatio(const double x) { tauTemp = x; };
  inline void setBeta(const double x) { beta = x; };

  inline auto getTempGrad() const { return omt; };
  inline auto getDensGrad() const { return omn; };
  inline auto getTempRatio() const { return tauTemp; };
  inline auto getBeta() const { return beta; };

  inline void setGeomPtr(const std::shared_ptr<Geom>& extGeomPtr) { geomPtr = extGeomPtr; };
  inline std::shared_ptr<Geom> getGeomPtr() { return geomPtr; };
  inline void setPtsm3dPtr(PTSM3D * const extPtsm3dPtr) { ptsm3dPtr = extPtsm3dPtr; };
  inline void setParamMapPtr(const std::shared_ptr<StringMap>& paramMapPtr) { params = paramMapPtr; };
// These members will be accessible by the derived model classes
protected:
  // Pointer to the geometry data, this can only be set to an external pointer and cannot
  // modify or delete the pointer
  PTSM3D *ptsm3dPtr;
  std::shared_ptr<Geom> geomPtr;
  std::shared_ptr<StringMap> params;
  cc_Vec eigVecs; // Standard complex double vector for eigenvector
  cc_Vec eigVals; // Standard complex double type for the eigenvalue
  cc_Vec dispersionCoeffs; // Vector holding the values of the dispersion coefficients

  int nFields;
  int nModes;
  cc_Vec massMat;
  cc_Vec massMatInv;

  int probSize;
  double omt;  // The normalized temperature gradient
  double omn;  // The normalized density gradient
  double tauTemp;  // The electron to ion temperature ratio
  double beta; // Normalized plasma pressure
  int buffer; // The size of the buffer region on either side of the center in field periods

  int errOrder; // The error order of the derivative calculation
  int hypzOrder; // The order of the parallel hyperdiffusion operator
  double hypzEps; // The coefficient for the hyperdiffusion operator

  i_Vec zIdx1;
  i_Vec zIdx2;
  int zBoundLower;
  int zBoundUpper;
  i_Vec nz;
  i_Vec evIndices;

/********************************************************************************
SLEPc eigensolver private variables
********************************************************************************/
  EPS eps; // The EPS object
  PEP pep; // The PEP object
  Mat* linOpArr; // Pointer to the array for the linear operators
  Vec initCondVec; // PETSc vector for the eigenvector
  Vec* petscVecs; // Pointer to the array of eigenvectors
  ISLocalToGlobalMapping lgMap;

private:
  std::weak_ptr<ITG3Field> modelITG3;
  std::weak_ptr<KBM5Field> modelKBM5;

};

#endif
