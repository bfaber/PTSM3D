#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <string>

#include "Definitions.hpp"
#include "SlepcUtils.hpp"
#include "Utils.hpp"

void SlepcUtils::FDCoeffGen(const PetscInt sl, const PetscInt sr, const PetscInt pdo, const PetscReal h, std::vector<std::complex<double>>& FDSpline)
{
  const double slDbl = sl;
  const double srDbl = sr;
  const int pdoInt = pdo;
  const double hDbl = h;
  Utils::FDCoeffGen(slDbl,srDbl,pdoInt,hDbl,FDSpline);
} 

void SlepcUtils::solve(EPS eps,SlepcUtils::SlepcContext sc, Mat* const epsMatArr)
{
  const PetscScalar zero = 0;
  ST st;

  EPSSetOperators(eps,epsMatArr[0],epsMatArr[1]);
  EPSSetDimensions(eps,sc.nev,sc.ncv,sc.mpd);
  EPSSetTolerances(eps,sc.tol,sc.maxIt);
  EPSGetST(eps,&st);
  //EPSSetWhichEigenpairs(eps,EPS_LARGEST_IMAGINARY);

  
  
  if (sc.target != zero) {
    STSetType(st,STSINVERT);
    STSetShift(st,sc.target);
    EPSSetTarget(eps,sc.target);
    EPSSetWhichEigenpairs(eps,EPS_TARGET_MAGNITUDE);
  } else {
    STSetType(st,STSHIFT);
    STSetShift(st,zero);
    EPSSetWhichEigenpairs(eps,EPS_LARGEST_IMAGINARY);
  }
  
 
  
  if (sc.icFlag == PETSC_TRUE) EPSSetInitialSpace(eps,1,sc.initCond);
  //if (sc.icFlag == PETSC_TRUE && sc.target != zero) DebugMsg(std::cout << "Starting eigenvalue calculation with an initial condition and target value" << '\n';);
  //if (sc.icFlag == PETSC_FALSE && sc.target == zero) DebugMsg(std::cout << "Starting eigenvalue calculation with no initial condition and target value" << '\n';);

  EPSSetType(eps,sc.eps.solver);

  EPSSolve(eps);
}

void SlepcUtils::solve(PEP pep, SlepcUtils::SlepcContext sc, Mat* const pepMatArr)
{
  const PetscScalar zero = 0;
  ST st;

  PEPSetOperators(pep,sc.nOps,pepMatArr);
  PEPSetProblemType(pep,PEP_GENERAL);
  PEPSetType(pep,sc.pep.solver);

  PEPSetDimensions(pep,sc.nev,sc.ncv,sc.mpd);
  PEPSetTolerances(pep,sc.tol,sc.maxIt);

  if (sc.target != zero) {
    PEPGetST(pep,&st);
    STSetType(st,STSINVERT);
    PEPSetTarget(pep,sc.target);
    PEPSetWhichEigenpairs(pep,PEP_TARGET_MAGNITUDE);
  } else {
    PEPSetWhichEigenpairs(pep,PEP_LARGEST_IMAGINARY);
  }

  PEPSolve(pep);
}

Vec SlepcUtils::convertToVec(const c_Vec& cVec)
{
  Vec pVec;
  
  return pVec;
}

void SlepcUtils::convertToVec(const c_Vec& cVec, Vec& pVec)
{
  PetscInt pSize;
  PetscInt* inds;
  VecGetSize(pVec,&pSize);
  if (cVec.size() != pSize) throw(std::runtime_error("PTSM3D Error! Petsc Vec and STD Vec have incompatible sizes!"));
  inds = new PetscInt[pSize];
  for (auto i=0; i<pSize; ++i) inds[i] = i;

  VecSetValues(pVec,pSize,inds,cVec.data(),INSERT_VALUES);
  VecAssemblyBegin(pVec);
  VecAssemblyEnd(pVec);
  delete[] inds;
}

void SlepcUtils::convertCCVecToVec(const cc_Vec& ccVec, Vec& pVec)
{
  PetscInt pSize;
  PetscInt* inds;
  VecGetSize(pVec,&pSize);
  auto ccSize1 = ccVec.size();
  auto ccSize2 = ccVec[0].size();
  if (ccSize1*ccSize2 != pSize) throw(std::runtime_error("PTSM3D Error! Petsc Vec and STD Vec have incompatible sizes!"));
  inds = new PetscInt[pSize];

  for (auto i = 0; i<ccSize1; ++i) {
    //const PetscScalar* vals = ccVec[i].data(); 
    for (auto j=0; j<ccSize2; ++j) inds[j] = ccSize2*i + j;
    VecSetValues(pVec,ccSize2,inds,ccVec[i].data(),INSERT_VALUES);
    //delete[] vals;
  }

  VecAssemblyBegin(pVec);
  VecAssemblyEnd(pVec);
  delete[] inds;
}

void SlepcUtils::extractSeqVecField(const SlepcContext& sc, Vec& fieldVec,
                                 Vec& globalVec, const int fieldIdx) {
  Vec localVec;
  VecScatter gatherCtx;
  IS gatherIS;
  const PetscScalar *v;
  PetscScalar *vecValues;
  PetscInt *vecInds;
  PetscInt globalVecSize, fieldVecSize, localIdx, globalIdx;

  VecGetSize(globalVec,&globalVecSize);

  vecInds = new PetscInt[globalVecSize];
  for (localIdx = 0; localIdx < globalVecSize; ++localIdx) vecInds[localIdx] = localIdx;
  ISCreateGeneral(PETSC_COMM_SELF,globalVecSize,vecInds,PETSC_COPY_VALUES,&gatherIS);

  VecCreateSeq(PETSC_COMM_SELF, globalVecSize, &localVec);

  VecScatterCreate(globalVec,NULL,localVec,gatherIS,&gatherCtx);
  VecScatterBegin(gatherCtx,globalVec,localVec,INSERT_VALUES,SCATTER_FORWARD);
  VecScatterEnd(gatherCtx,globalVec,localVec,INSERT_VALUES,SCATTER_FORWARD);
  VecScatterDestroy(&gatherCtx);
  ISDestroy(&gatherIS);

  fieldVecSize = globalVecSize/sc.nFields;
  VecCreateSeq(PETSC_COMM_SELF,fieldVecSize,&fieldVec); 
   
  vecValues = new PetscScalar[fieldVecSize];
  vecInds = new PetscInt[fieldVecSize];
  VecGetArrayRead(localVec,&v); 
  for (localIdx = 0; localIdx < fieldVecSize; ++localIdx) {
    globalIdx = fieldIdx*fieldVecSize + localIdx;
    vecValues[localIdx] = v[globalIdx];
    vecInds[localIdx] = localIdx;
  }
    
  VecSetValues(fieldVec,fieldVecSize,vecInds,vecValues,INSERT_VALUES);
  VecAssemblyBegin(fieldVec);
  VecAssemblyEnd(fieldVec);
  VecDestroy(&localVec);
  delete[] vecValues;
  delete[] vecInds;
}

/*
void SlepcUtils::buildParallelVec(const SlepcContext& sc, const c_Vec& stdVec, Vec& globalVec) {
  Vec localVec;
  VecScatter gatherCtx;
  IS gatherIS;
  const PetscScalar *v;
  PetscScalar *vecValues;
  PetscInt *vecInds;
  PetscInt globalVecSize, fieldVecSize, localIdx, globalIdx;

  VecGetSize(globalVec,&globalVecSize);

  vecInds = new PetscInt[globalVecSize];
  for (localIdx = 0; localIdx < globalVecSize; ++localIdx) vecInds[localIdx] = localIdx;

  ISCreateGeneral(PETSC_COMM_SELF,globalVecSize,vecInds,PETSC_COPY_VALUES,&gatherIS);

  VecSetValues(fieldVec,fieldVecSize,vecInds,vecValues,INSERT_VALUES);
  VecAssemblyBegin(fieldVec);
  VecAssemblyEnd(fieldVec);
  VecDestroy(&localVec);
  delete[] vecValues;
  delete[] vecInds;
}
*/
