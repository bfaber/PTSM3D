/******************************************************************************
* This is the base class for PTSM3D that other classes inherit
******************************************************************************/
#ifndef PTSM3D_HPP
#define PTSM3D_HPP

#include <string>
#include <iostream>
#include <unordered_map>
#include <functional>
#include <memory>

#include "petsc.h"

#include "Definitions.hpp"
#include "SlepcUtils.hpp"
#include "HDF5Utils.hpp"
#include "DriftWave.hpp"
#include "Geom.hpp"
#include "FluidModel.hpp"

// TODO: Make a SatModel to encapsulate different models like ITG 3-field, KBM 5-field or TEM
class KBM5Field;
class Geom;

class PTSM3D
{
  public:

    PTSM3D();
    ~PTSM3D();
    std::shared_ptr<FluidModel> satModel;
    std::shared_ptr<ITG3Field> modelITG3;
    std::shared_ptr<KBM5Field> modelKBM5;
    std::shared_ptr<Geom> geom;

    void setParameters();
    void setupPTSM3D();
    void computeLinearSpectrum();
    void computeTauCC();
    void computeTauCCParallel();
    bool computeInitialCondition(const int j, const int k);
    void callSlepcSolver(const int j, const int k, const bool skip);
    void writeModelData();
    void readInput(const std::string inpFile);
    std::tuple<int,int,bool,bool,bool> computeKPrime(const int kxIdx1, const int kyIdx1, const int kxIdx3, const int kyIdx3);
    inline int kInds(const int ix, const int iy) { return (nkx/2+1)*iy + ix; };
    inline const std::vector<DriftWave>* getLinearSpectrumAddr() const { const std::vector<DriftWave>* ptr = &linearSpectrum; return ptr; };

    inline auto getKxMap() const { return kxMap; };
    inline auto getKyMap() const { return kyMap; };
    inline auto getKxEnergySpec() const { return kxEnergySpec; };
    inline auto getKyEnergySpec() const { return kyEnergySpec; };

    inline auto getParamMap() const { return params; };
    inline void setParamString(const std::string key, const std::string value) { params[key] = value; };
    inline std::string getParamValue(const std::string key) { return params[key]; };
    inline void viewParamString(const std::string key) { std::cout << "Parameter with key " << key << " is " << params[key] << '\n'; };
    inline auto getNkx() const { return nkx; };
    inline auto getNky() const { return nky; };

    inline auto getMPIRank() const { return mpiRank; };
    inline auto getNMPIProcs() const { return mpiSize; };

    inline auto writeESpec() const { return writeEigenspectrum; };

    inline void setNzfp(const int x) { nzfp = x; };

  private:
    StringMap params; // String to string map with parameter keys and values
    std::shared_ptr<StringMap> paramPtr;
    std::string geomDir; // Directory where the geometry file sits
    std::string geomFile; // The input geometry file
    std::string tag; // The tag to assign to all output files
    std::string geomType; // The geometry type (analytic vs. equilibrium)
    std::string eqType; // The equilibrium type (GIST vs. VMEC)
    std::string outDir;
    bool optTarget; // Determine if it's being called as part of an optimization loop
    bool writeEigenspectrum;
    bool subModeCoupling;
    int nFields;
    int nkx; // Number of kx modes used
    int nky; // Number of ky modes used
    int kyStart;
    int nzfp; // Number of points in the parallel direction per field period
    int zeroIndex;
    int probSize;
    double dkx; // Spacing between \f$k_x\f$ modes in units of \f$\rho_\mathrm{s}\f$
    double dky; // Spacing between \f$k_y\f$ modes in units of \f$\rho_\mathrm{s}\f$
    // TODO: Define the data structures used to compute the linear instability spectrum, triplet correlation times, coupling coefficients and targets
    d_Vec kxMap;
    d_Vec kyMap;
    d_Vec kxEnergySpec;
    d_Vec kyEnergySpec;
    double kxSpecLimit, kySpecLimit;
    double kxSpecPower, kySpecPower;

    int errOrder;
    int hypzOrder;
    double hypzEps;

    std::unordered_map<int,int> linearSpectrumIndexMap;
    std::vector<DriftWave> linearSpectrum;
    i_Vec linearSpectrumPartInds;
    int linearSpectrumPartSize;
    ii_Vec triplets;

    // SLEPc specific variables
    std::string which_ev;
    SlepcUtils::SlepcContext sc;

    // HDF5 output file
    hid_t fileID;

    // MPI variables
    PetscMPIInt mpiRank;
    PetscInt mpiSize;
};
#endif
