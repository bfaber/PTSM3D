#ifndef ITG3FIELD_HPP
#define ITG3FIELD_HPP

// STL includes
#include <iostream>
#include <vector>
#include <complex>
#include <utility>
#include <memory>
#include <tuple>

// External includes
#include "slepc.h"
#include "hdf5.h"

// Local includes
#include "Definitions.hpp"
#include "SlepcUtils.hpp"
#include "HDF5Utils.hpp"
#include "FluidModel.hpp"

// Forward declarations of necessary classes
class Geom;
class DriftWave;

//class ITG3Field : public FluidModel
class ITG3Field : public FluidModel
{
  public:
    //Constructor and destructor
    ITG3Field();
    ITG3Field(const int n_fields);
    virtual ~ITG3Field();

    /********************************************************************************
      SLEPc eigensolver functions
    ********************************************************************************/
    // Wrapper for solving the eigensystem
    virtual void solve(SlepcUtils::SlepcContext& sc);

    // Provides an initial guess of the target
    virtual PetscScalar guessTarget(const double kx, const double ky);
    virtual void extractInitCond(const int idx);

    // Operations for building the linear operators
    virtual void createLinearOperators(SlepcUtils::SlepcContext& sc);
    virtual void destroyLinearOperators(const SlepcUtils::SlepcContext& sc);
    virtual void zeroLinearOperators(const SlepcUtils::SlepcContext& sc, const bool setVecToZero);
    virtual void buildLinearOperators(SlepcUtils::SlepcContext& sc, const double kx, const double ky);
    virtual void extractSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky);
    virtual void computeMassMatrix(const int modeID, const double ky, const c_Vec& dispRoots);


    // Cubic dispersion relation, does not require knowledge of the type of eigensolver
    virtual double computeLinearRoots(const double kx, const double ky, const int iz1, const int iz2,
                            const c_Vec& evec, c_Vec& roots);
    std::tuple<double,double,double,double> computeMarginalMode(const double kx, const double ky, const int iz1, const int iz2, const c_Vec& evec);

    //void setup();
    //void setModelParameters();

    // Function to build the EPS problem, only called by buildLinearOperators()
    void buildEPSOperators(const double kx, const double ky);
    void extractEPSSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky);

    // Function to build the PEP problem, only called by buildLinearOperators()
    void buildPEPOperators(const double kx, const double ky);
    void extractPEPSolution(const SlepcUtils::SlepcContext& sc, const double kx, const double ky);

  private:
};

#endif
