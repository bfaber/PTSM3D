#ifndef HDF5UTILS_HPP
#define HDF5UTILS_HPP

#include <vector>
#include <typeinfo>
#include <string>

#include "hdf5.h"

#include "Definitions.hpp"

class PTSM3D;

namespace HDF5Utils {
  using HDF5Map = std::unordered_map<std::string,hid_t>;

  typedef struct {
    double re;
    double im;
  } complex_t;

  typedef struct {
    complex_t unstable;
    complex_t stable;
    complex_t marginal;
  } eval_t;
  
  typedef struct {
    int mode1;
    int mode2;
    int mode3;
  } triplet_t;

  typedef struct {
    double tauCC;
    complex_t tau;
    complex_t CC;
  } tauCC_t;

  typedef struct {
    int kx1;
    int ky1;
    int kx2;
    int ky2;
    int kx3;
    int ky3;
  } kperp_t;
  
  const std::vector<std::string> fieldnames{"Phi","V_parallel","Temperature","Density","A_parallel"};

  hid_t openHDF5File(const std::string filename);
  void writeLinearSpectrum(const hid_t fileID, const PTSM3D* model);
  void writeTauCC(const hid_t fileID, const PTSM3D* model);
}
#endif
