#include <iostream>
#include <string>
#include <unordered_map>
#include <complex>
#include <experimental/filesystem>
#include <sstream>
#include <fstream>

#include "slepc.h"

#include "Interfaces.hpp"
#include "Utils.hpp"
#include "PTSM3D.hpp"
#include "Geom.hpp"

// CObject defined in Interfaces.hpp

CObject createPTSM3D(MPI_Comm externalComm)
{
  PETSC_COMM_WORLD = externalComm;
  SlepcInitialize(0,(char***)0,(char*)0,(char*)0);
  PTSM3D *SatModel = new PTSM3D;
  std::cout << "Creating PTSM3D" << '\n';
  return (CObject)SatModel;
}

void destroyPTSM3D(CObject CObj)
{
  PTSM3D *SatModel = (PTSM3D *)CObj;
  std::cout << "Destorying PTSM3D" << '\n';
  delete(SatModel);
  SlepcFinalize();
  return;
}

void updatePTSM3DParam(CObject cPtr, char** key, char** val)
{
  std::string keyString, valString;
  PTSM3D *SatModel = (PTSM3D *)cPtr;
  keyString = *key;
  valString = *val;
  SatModel->setParamString(keyString,valString);
  SatModel->viewParamString(keyString);
  return;
}

void callPTSM3D(CObject cPtr, char* filename)
{
  PTSM3D *SatModel = (PTSM3D *)cPtr;
  SatModel->readInput(filename);
  SatModel->setupPTSM3D();
  SatModel->computeLinearSpectrum();
  SatModel->computeTauCC();
  SatModel->writeModelData();
  return;
}

void Interfaces::callVmec2Pest(std::string eqFile, double surface, const double dky,
                               const int nz0, const int buffer, std::shared_ptr<Geom>& geom)
{
  std::unordered_map<std::string,geomFuncPtr> v2p_DiagMap;
  char *diag_name;
  std::string diag_str;
  int x1, x2, data_size, n_dims;
  double* c_data;

  v2p_opts vmec2pest_options;
  char *file = &eqFile[0u];
  char gridType[] = "gene";
  char x3Coord[] = "zeta";
  char normType[] = "minor_r";
  vmec2pest_options.vmec_file = file;
  vmec2pest_options.grid_type = gridType;
  vmec2pest_options.x3_coord = x3Coord;
  vmec2pest_options.norm_type = normType;

  vmec2pest_options.x1 = &surface;
  vmec2pest_options.nx1 = 1;
  vmec2pest_options.nx2 = 1;
  vmec2pest_options.nx3 = 1;

  vmec2pest_options.nfpi = 1.0;
  vmec2pest_options.x2_center = 0.0;
  vmec2pest_options.x3_center = 0.0;
  DebugMsg(std::cout << "Calling vmec2pest_c_interface" << '\n';);
  vmec2pest_c_interface(&vmec2pest_options);

  diag_str = "nfp";
  diag_name = &diag_str[0u];
  x1 = 0;
  x2 = 0;
  n_dims = -1;
  data_size = 1;
  c_data = new double[data_size];
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  geom->setNfp((int)c_data[0]);

  diag_str = "shat";
  diag_name = &diag_str[0u];
  n_dims = 0;
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  geom->setShat(c_data[0]);

  diag_str = "safety_factor_q";
  diag_name = &diag_str[0u];
  n_dims = 0;
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  geom->setq0(c_data[0]);

  delete[] c_data;

  //Compute the maximum distance needed to compute along a field line
  auto max_z = 1.0/(std::abs(geom->getShat())*dky);
  data_size = (std::ceil(max_z/Utils::pi()) + buffer)*geom->getNfp()*nz0;
  vmec2pest_options.nx3 = data_size;
  vmec2pest_options.nfpi = (double)((std::ceil(max_z/Utils::pi())+buffer)*geom->getNfp());
  data_size += 1;
  vmec2pest_c_interface(&vmec2pest_options);

  v2p_DiagMap["g11"] = &Geom::setGxx;
  v2p_DiagMap["g12"] = &Geom::setGxy;
  v2p_DiagMap["g22"] = &Geom::setGyy;
  v2p_DiagMap["bmag"] = &Geom::setModB;
  v2p_DiagMap["jac"] = &Geom::setJac;
  v2p_DiagMap["curv_drift_x2"] = &Geom::setDBdx;
  v2p_DiagMap["curv_drift_x1"] = &Geom::setDBdy;
  v2p_DiagMap["d_B_d_x3"] = &Geom::setDBdz;
  v2p_DiagMap["x3"] = &Geom::setEta;

  d_Vec temp;
  n_dims = 1;
  geom->setGeomSize(data_size);
  temp.resize(data_size+1);
  c_data = new double[data_size+1];
  for (std::unordered_map<std::string,geomFuncPtr>::iterator iter=v2p_DiagMap.begin(); iter != v2p_DiagMap.end(); ++iter)
  {
    diag_str = iter->first;
    diag_name = &diag_str[0u];
    get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
    for (auto i = 0; i < data_size; ++i) temp[i] = c_data[i];
    geom->setGeomField(temp,iter->second);
  }
  delete[] c_data;
  geom->setDEta((geom->eta[1]-geom->eta[0])/Utils::pi());
}


void Interfaces::readPESTFile(const std::string& filename, PTSM3D* satModel) {
  std::string line, key, value;
  std::fstream geomFile;
  const std::string eqSign = "=";
  geomFile.open(filename,std::ios_base::in);  
  int nz0, n_pol, nfp;
  double shat, q0;
  
  while ( line != "/" ) {
    std::getline(geomFile,line);
    if (!line.empty()) {
      std::size_t eqPos = line.find(eqSign);
      // If line.find() cannot find eqSign, then it returns string::npos
      if (eqPos < std::string::npos)
      {
        // Make sure the trimmed string for the parameter string is not empty
        // Throw an exception and exit if empty parameter string encountered
        key = (eqPos > 0) ? !(Utils::trim_copy(line.substr(0,eqPos-1))).empty() ?
          Utils::trim_copy(line.substr(0,eqPos-1)) :
          throw(std::runtime_error("PTSM3D Error! Empty parameter key identfier in geometry input file")) :
          throw(std::runtime_error("PTSM3D Error! Empty parameter key identfier in geometry input file"));

        // Make sure the trimmed string for the parameter value is not empty
        // Throw an exception and exit if empty parameter value encountered
        value = (eqPos < line.length()-1) ?
          !(Utils::trim_copy(line.substr(eqPos+1,line.length()-1))).empty() ?
          Utils::trim_copy(line.substr(eqPos+1,line.length()-1)) :
          throw(std::runtime_error("PTSM3D Error! Empty parameter value in geometry input file")) :
          throw(std::runtime_error("PTSM3D Error! Empty parameter value in geometry input file"));

        if (key == "n_pol") {
          n_pol = std::stoi(value);
        }
        if (key == "gridpoints") {
          nz0 = std::stoi(value);  
        }
        if (key == "shat") {
          shat = std::stod(value);
          satModel->geom->setShat(shat);
        }
        if (key == "q0") {
          q0 = std::stod(value);
          satModel->geom->setq0(q0);
        }
        if (key == "nfp") {
          nfp = std::stoi(value);
        }
      }
    }
  }

  d_Vec gxx(nz0), gxy(nz0), gyy(nz0), modB(nz0), jac(nz0), K2(nz0), K1(nz0), dBdz(nz0), theta(nz0);
  int i = 0;
  auto peekChar = geomFile.peek();
  while ( peekChar != EOF ) {
    std::getline(geomFile,line);
    if (!line.empty()) {
      std::istringstream lineStream(line);
      lineStream >> gxx.at(i) >> gxy.at(i) >> gyy.at(i) >> modB.at(i) >> jac.at(i) >> K2.at(i) >> K1.at(i) >> dBdz.at(i) >> theta.at(i);
      ++i;
      peekChar = geomFile.peek();
    }
  }

  auto buffer = std::stoi(satModel->getParamValue("buffer"));
  auto max_theta = 1.0/std::abs(std::stod(satModel->getParamValue("dky"))*shat*Utils::pi()) + buffer;

  int nIntervals = (int)std::ceil((max_theta - n_pol)/(2*n_pol))+1; 
  int nPoints = (2*nIntervals+1)*nz0 + 1;
  d_Vec gxx_ext(nPoints), gxy_ext(nPoints), gyy_ext(nPoints), modB_ext(nPoints), jac_ext(nPoints), K2_ext(nPoints), K1_ext(nPoints), dBdz_ext(nPoints), theta_ext(nPoints); 
  int globalZeroIndex = nIntervals*nz0 + nz0/2;
  double dz = 2.0*n_pol/nz0;

  for (int j = nIntervals; j >= 1; --j) {
    int theta_k = -2*n_pol*j;
    for (i = 0; i<nz0; ++i) {
      auto k = globalZeroIndex + i - nz0/2 + theta_k*nz0/2;
      theta_ext.at(k) = (i - nz0/2)*dz + theta_k;
      gxx_ext.at(k) = gxx.at(i);
      gxy_ext.at(k) = gxy.at(i) + shat*theta_k*Utils::pi()*gxx.at(i);
      gyy_ext.at(k) = gyy.at(i) + 2*shat*theta_k*Utils::pi()*gxy.at(i) + shat*shat*theta_k*theta_k*Utils::pi()*Utils::pi()*gxx.at(i);
      modB_ext.at(k) = modB.at(i);
      jac_ext.at(k) = jac.at(i);
      dBdz_ext.at(k) = dBdz.at(i);
      K2_ext.at(k) = K2.at(i) + shat*theta_k*Utils::pi()*K1.at(i); 
      K1_ext.at(k) = K1.at(i);
    }
  }

  for (i = 0; i<nz0; ++i) {
    auto k = globalZeroIndex + i - nz0/2;
    theta_ext.at(k) = (i - nz0/2)*dz;
    gxx_ext.at(k) = gxx.at(i);
    gxy_ext.at(k) = gxy.at(i);
    gyy_ext.at(k) = gyy.at(i);
    modB_ext.at(k) = modB.at(i);
    jac_ext.at(k) = jac.at(i);
    dBdz_ext.at(k) = dBdz.at(i);
    K2_ext.at(k) = K2.at(i); 
    K1_ext.at(k) = K1.at(i);
  }

  for (int j = 1; j <= nIntervals; ++j) {
    int theta_k = 2*n_pol*j;
    for (i = 0; i<nz0; ++i) {
      auto k = globalZeroIndex + i - nz0/2 + theta_k*nz0/2;
      theta_ext.at(k) = (i - nz0/2)*dz + theta_k;
      gxx_ext.at(k) = gxx.at(i);
      gxy_ext.at(k) = gxy.at(i) + shat*theta_k*Utils::pi()*gxx.at(i);
      gyy_ext.at(k) = gyy.at(i) + 2*shat*theta_k*Utils::pi()*gxy.at(i) + shat*shat*theta_k*theta_k*Utils::pi()*Utils::pi()*gxx.at(i);
      modB_ext.at(k) = modB.at(i);
      jac_ext.at(k) = jac.at(i);
      dBdz_ext.at(k) = dBdz.at(i);
      K2_ext.at(k) = K2.at(i) + shat*theta_k*Utils::pi()*K1.at(i); 
      K1_ext.at(k) = K1.at(i);
    }
  }

  satModel->setNzfp(nz0/n_pol);
  satModel->geom->setNfp(nfp);
  satModel->geom->setGeomSize(nPoints);
  satModel->geom->setGxx(gxx_ext);
  satModel->geom->setGxy(gxy_ext);
  satModel->geom->setGyy(gyy_ext);
  satModel->geom->setModB(modB_ext);
  satModel->geom->setJac(jac_ext);
  satModel->geom->setDBdx(K1_ext);
  satModel->geom->setDBdy(K2_ext);
  satModel->geom->setDBdz(dBdz);
  satModel->geom->setEta(theta_ext);
  satModel->geom->setDEta(dz);
}
