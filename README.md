# Plasma Turbulence Saturation Model - 3D (PTSM3D)

PTSM3D computes different metrics to evaluate the saturation of drift-wave-driven microturbulence without performing a full nonlinear simulation.  This model is the numerical implementation of the theory described in [Hegna, Terry and Faber, Phys. Plasmas, **25**, 022511 (2018)](https://doi.org/10.1063/1.5018198).

PTSM3D algorithm:

1. Compute eigenvalues $\omega_i$ from a fluid model for drift-wave instabilities (like Ion Temperature Gradient (ITG) modes and Kinetic Ballooning Modes (KBM)) along a flux tube from a stellarator configuration
2. Compute the *three-wave interaction time* 
```math
\tau_{pst}(k,k') = \frac{1}{\mathrm{i}\left(\omega(k'')_t + \omega(k')_s - \omega(k)^\ast_p\right)}
```
and *complex coupling coefficients* 
```math
C(k,k';\omega(k),\omega(k'))
```
from the linear eigenvalue spectra

3. Compute which combination of triplets and wavenumbers maximizes these terms at each *(k_x,k_y)* between unstable and stable modes

## Dependencies
PTSM3D relies on the following libraries:
- MPI: [MPICH](https://www.mpich.org), [OpenMPI](https://www.open-mpi.org)
- Optimized BLAS/LAPACK: [OpenBLAS](https://www.openblas.net), Intel MKL
- [Blaze linear algebra library](https://bitbucket.org/blaze-lib/blaze)
- [Parallel HDF5](https://www.hdfgroup.org/solutions/hdf5)
- [Parallel NetCDF](https://www.unidata.ucar.edu/software/netcdf)
- [Parallel NetCDF-Fortran](https://www.unidata.ucar.edu/software/netcdf)
- [PETSc](https:/www.mcs.anl.gov/petsc)
- [SLEPc](https://slepc.upv.es)
- [Plasma Equilibrium Toolkit](https://gitlab.com/wistell/PET)

## Compiling PTSM3D
To compile PTSM3D, following libraries must be accessible by GNU make, either through $LD_LIBRARY_PATH or hard coding the paths into the Makefile:
- `-lnetcdff -lhdf5_hl_fortran -lhdf5_fortran -lhdf5_hl -lhdf5`
- Furthermore the environmental variables PETSC_DIR and SLEPC_DIR must set to the appropriate path

## Running PTSM3D
PTSM3D operates best with multiple processors.  This allows SLEPc to effectively leverage multiple processors for eigenvalue calculations.  In order to use PTSM3D with a [VMEC](https://princetonuniversity.github.io/STELLOPT/VMEC) equilibrium, PTSM3D must be linked to a compiled version of [VMECTools](https://github.com/benjaminfaber/VMECTools.git).  To run PTSM3D on $N$ processors, use the following command
`mpiexec -n N ./PTSM3D_C PTSM3D.inp`

The structure of the PTSM3D input file "PTSM3D.inp" has the following structure:
```
#PTSM3D Input/Output options
geomType = eq   #Can be 'eq' or 's_alpha'
eqType = vmec   #Can be 'vmec' or 'pest'
geomFile = /home/bfaber/data/equilibria/HSX/wout_QHS_good.nc
outDir = /home/bfaber/data/PTSM3D/
tag = QHS_good #Labels the output files

#Spectrum parameters
nkx = 41    # Number of kx modes (pos + neg + zero)
nky = 20    # Number of positive ky modes
nzfp = 50   # Number of points per field period
dkx = 0.05  # kx mode spacing
dky = 0.05  # ky mode spacing

#Plasma parameters
omt = 3.00      # -a/L_Ti: temperature gradient length scale normalized to average minor radius
omn = 1.00      # -a/L_n: density gradient length scale normalized to average minor radius
tauTemp = 1     # temperature ratio between ions and electrons

#Eigenvalue options
which_ev = eps   # Which eigenvalue solver to use, default is eps
errOrder = 6    # Error order for parallel derivative finite difference splines
hypzOrder = 2   # Order of parallel hyperdiffusion operator
hypzEps = 0.25  # Parallel hyperdiffusion coefficient
nev = 5    # Number eigenvalues to compute per spectral point
tol = 1e-12     # Eigenvalue solver tolerance
buffer = 16 #Number of transits along field line in units of 2pi over which to compute the eigenvalue problem
```

PTSM3D will then produce an HDF5 output file: `$outDir/spectrum_ptsm3d_tag.h5`

These results can then be analyzed with the [Julia](https://julialang.org) tool [PTSM3DTools.jl](https://gitlab.com/bfaber/PTSM3DTools.jl).