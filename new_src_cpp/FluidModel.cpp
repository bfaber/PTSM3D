#include <iostream>
#include <memory>

#include "blaze/math/Row.h"
#include "slepc.h"

#include "FluidModel.hpp"
#include "Definitions.hpp"
#include "PTSM3DControl.hpp"
#include "Utils.hpp"
#include "ITG3Field.hpp"

FluidModel::FluidModel() {}

FluidModel::FluidModel(const int n_fields) : nFields{n_fields}
{
}

FluidModel::~FluidModel()
{
}

std::weak_ptr<FluidModel> FluidModel::getFluidModel() {
  return weak_from_this();
}

void FluidModel::initialize(const std::shared_ptr<PTSM3DControl> control) {
  controller = control;
  model3Field = std::make_shared<ITG3Field>();
  model3Field->setModelPtr(getFluidModel());
}

void FluidModel::createSlepcSolver()
{
  EPSCreate(PETSC_COMM_WORLD,&eps);
  PEPCreate(PETSC_COMM_WORLD,&pep);
}

void FluidModel::destroySlepcSolver()
{
  EPSDestroy(&eps);
  PEPDestroy(&pep);
}

void FluidModel::createLinearOperators(SlepcSolver::SolverContext& sc) {
  if (sc.probType == "eps") {
    // Set up the PETSc matrices for the EPS operators
    // For the 3-field system of equations discretized by N points
    // in the parallel direction, there are 3*N number of points in
    // the total system
    sc.nOps = 2;
    sc.opDims = sc.nFields*probSize;
  } else if (sc.probType == "pep") {
    sc.nOps = sc.nFields+1;
    sc.opDims = probSize;
  }

  // Allocate the pointer array for linOpArr
  linOpArr = new Mat[5]; 
  // Setup the PETSc matrices for the linear operators
  for (int i=0; i<sc.nOps; i++)
  {
    MatCreate(PETSC_COMM_WORLD,&linOpArr[i]);
    MatSetSizes(linOpArr[i],PETSC_DECIDE,PETSC_DECIDE,sc.opDims,sc.opDims);
    MatSetUp(linOpArr[i]);
  }
  // Create the eigenvector with the same parallel layout as the linOpArrs
  MatCreateVecs(linOpArr[0],&petscEigVec,NULL);
}

void FluidModel::destroyLinearOperators(const SlepcSolver::SolverContext& sc)
{
  //Destroy each linOpArr
  for (int i=0; i<sc.nOps; i++)
  {
    MatDestroy(&linOpArr[i]);
  }
  // Release the linOpArr pointer
  delete[] linOpArr;
  // Destory the PETSc eigenvector
  VecDestroy(&petscEigVec);
  //for (int i=0; i<sc.nFields; i++) VecDestroy(&petscEigVecs[i]);
  //delete[] petscEigVecs;
}

void FluidModel::zeroLinearOperators(const SlepcSolver::SolverContext& sc, const bool setVecToZero)
{
  for (auto i=0; i<sc.nOps; ++i) MatZeroEntries(linOpArr[i]);
  if (setVecToZero) VecSet(petscEigVec,0);
}

void FluidModel::buildLinearOperators(SlepcSolver::SolverContext& sc,
                                     const double kx, const double ky)
{
  if (nFields == 3) {
    model3Field->buildLinearOperators(sc, kx, ky);
  }
}

void FluidModel::extractSolution(const SlepcSolver::SolverContext& sc,
                                const double kx, const double ky)
{
  if (nFields == 3) {
    model3Field->extractSolution(sc, kx, ky);
  }
}

void FluidModel::setModelParameters()
{
  std::shared_ptr<StringPairMap> paramInfoPtr = controller->getParamInfoPtr();
  Utils::getParameterValue(nFields,"nFields",paramInfoPtr);
  Utils::getParameterValue(errOrder,"errOrder",paramInfoPtr);
  Utils::getParameterValue(hypzOrder,"hypzOrder",paramInfoPtr);
  Utils::getParameterValue(hypzEps,"hypzEps",paramInfoPtr);
  Utils::getParameterValue(omt,"omt",paramInfoPtr);
  Utils::getParameterValue(omn,"omn",paramInfoPtr);
  Utils::getParameterValue(tauTemp,"tauTemp",paramInfoPtr);
}

/*
 * This function needs to return a row view of the complex matrix
 */
cmplxVec FluidModel::getEigVecField(const int fieldID) const {
  if (fieldID >= 0 && fieldID < nFields) { 
    // Do something with rows 
  } else {
    throw std::runtime_error("FieldID "+std::to_string(fieldID)+" not in range 0 <= fieldID < nFields");
  }
}

/*
 * This function needs to return a row view of the complex matrix
 */
void FluidModel::getEigVecField(const int fieldID, cmplxVec& vec) {
  if (fieldID >= 0 && fieldID < nFields) { 
    // Do something with rows 
  } else {
    throw std::runtime_error("FieldID "+std::to_string(fieldID)+" not in range 0 <= fieldID < nFields");
  }
}
 
