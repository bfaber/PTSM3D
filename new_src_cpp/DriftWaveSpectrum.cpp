#include <iostream>

#include "DriftWaveSpectrum.hpp"
#include "Definitions.hpp"

DriftWaveSpectrum::DriftWaveSpectrum() {
}

DriftWaveSpectrum::DriftWaveSpectrum(const int nkx, const int nky, const int nz, const int nf) :
  nKxModes(nkx), nKyModes(nky), probSize(nz), nFields(nf) {
  setSpectrumSize();
}

void DriftWaveSpectrum::setSpectrumSize(const int nkx, const int nky, const int nz, const int nf) {
  nKxModes = nkx;
  nKyModes = nky;
  probSize = nz;
  nFields = nf;
  setSpectrumSize();
}

void DriftWaveSpectrum::setSpectrumSize() {
  nModes = nKxModes*nKxModes;

  std::unique_ptr<cmplxMat[]> tempVecs = std::make_unique<cmplxMat[]>(nFields);
  for (auto i = 0; i < nFields; ++i) {
    tempVecs[i].resize(nModes,probSize);
  }
  eigVecs = std::move(tempVecs);

  std::unique_ptr<cmplxMat> tempMat1 = std::make_unique<cmplxMat>();
  tempMat1->resize(nModes,nFields);
  eigVals = std::move(tempMat1);

  std::unique_ptr<cmplxMat> tempMat2 = std::make_unique<cmplxMat>();
  tempMat2->resize(nModes,nFields*nFields);
  massMatrix = std::move(tempMat2);
}
