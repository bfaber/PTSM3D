#ifndef _SLEPCSOLVER_HPP_
#define _SLEPCSOLVER_HPP_

#include <vector>
#include <complex>
#include <string>
#include <memory>

#include "slepc.h"

#include "Definitions.hpp"
/*
    Namespace: SlepcUtils
*/
namespace SlepcSolver
{
  struct EPSContext
  {
    EPSType solver;
  };

  struct PEPContext
  {
    PEPType solver;
  };

  struct SolverContext
  {
    std::string probType;
    PetscInt nFields;
    PetscInt opDims;
    PetscInt nOps;
    PetscInt nev;
    PetscInt ncv;
    PetscInt mpd;
    PetscInt maxIt;
    PetscReal tol;
    PetscScalar target;
    std::shared_ptr<Vec> initCond;
    PetscInt scanDirection;
    PetscBool icFlag;
    EPSContext eps;
    PEPContext pep;
  };

/*
    Function: solve

    Applies the EPS solver to the generalized eigenvalue problem A*x = u B*x for the
    eigenvalue u and the eigenvector x.

    Parameters:
      -solver: SolverContext controlling the solver

      -linearOperators: the left-hand-side matrix operator

    Returns:
      -void
*/
  void solve(EPS eps, SolverContext solver, const std::shared_ptr<Mat> linearOperators);
  void solve(PEP pep, SolverContext solver, const std::shared_ptr<Mat> linearOperators);
  void extractSolution(EPS eps, SolverContext solver);
  void extractSolution(PEP pep, SolverContext solver);

  void extractSeqVecField(const SolverContext& solver, Vec& fieldVec, Vec& globalVec, const int fieldIdx);
}
#endif
