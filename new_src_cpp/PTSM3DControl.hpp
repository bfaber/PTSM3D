/*
 * This module is the controller for the saturation model calculation.  It should
 * control the following aspects of the calculation:
 *  1. Control the parallelization
 *   - The eigenvalue calculation with SLEPc requires MPI, so this class will be in control
 *   of splitting and keeping track for the communicators
 *  2. Control of the input and output of the simulation
 *   - Input is controlled by reading into a map of strings
 *   - Output is controlled by HDF5
 */

#ifndef _PTSM3DCONTROL_HPP_
#define _PTSM3DCONTROL_HPP_

#include <iostream>
#include <memory>
#include <string>

#include "petsc.h"
#include "mpi.h"
#include "hdf5.h"

#include "Definitions.hpp"
#include "Geom.hpp"
#include "DriftWaveSpectrum.hpp"


class PTSM3DControl : std::enable_shared_from_this<PTSM3DControl>
{
  public:
    PTSM3DControl();
    PTSM3DControl(MPI_Comm comm);
    ~PTSM3DControl() { }
    std::shared_ptr<PTSM3DControl> getController();

    /*
     * Functions for MPI communicator management
     */
    int partitionMPIComm(const int n_split_groups);
    inline auto getNeighborBelow(const int ky) const { return std::get<0>(neighborMap.at(ky)); };
    inline auto getNeighborAbove(const int ky) const { return std::get<1>(neighborMap.at(ky)); };
    void testVectorPass(const int n);

    void setGlobalComm(const MPI_Comm& comm);
    inline void getGlobalComm(MPI_Comm& comm) { comm = globalComm; };
    inline void getLocalComm(MPI_Comm& comm) { comm = localComm; };
    inline auto getLocalSize() const { return mpiLocalSize; };
    inline auto getGlobalSize() const { return mpiGlobalSize; };
    inline auto getGlobalRank() const { return mpiGlobalRank; };
    inline auto getLocalRank() const { return mpiLocalRank; };
    inline auto getGlobalMaster() const { return globalMaster; };
    inline auto getLocalMaster() const { return localMaster; };

    /*
     *  Functions for PTSM3D specific work
     */
    // Read the input file
    void readInputFile(const std::string inpFile);
    void setParameterInfo();
    void viewParameterInfo();
    inline auto getParamInfoPtr() const { return paramInfoPtr; }; 
    
    // Set the keys and values in the parameters StringMap
    inline void setInputParameters(const std::string key, const std::string value) { inputParameters[key] = value; };

    // Functions for dealing with the geometry
    void initializeGeometry();


    // Write the saturation data to HDF5 file(s)
    void writeSaturationDataHDF5(const std::string outFile);

  private:
    //MPICommMap commMap; // Map between global ranks and the local MPI communicators
    std::unordered_map<int,std::pair<int,int>> neighborMap;

    StringMap inputParameters;
    StringPairMap parameterInfo;
    std::shared_ptr<StringPairMap> paramInfoPtr;
    
    Geom geom;
    std::shared_ptr<Geom> geomPtr; 

    DriftWaveSpectrum spectrum;
    DriftWaveSpectrum localSpectrum;

    MPI_Comm globalComm;
    MPI_Comm localComm;
    int mpiGlobalSize; // Size of globalComm
    int mpiGlobalRank; // Rank in globalComm
    int mpiLocalSize; // Upon splitting, size of local communicator
    int mpiLocalRank; // Upon splitting, rank within the local communicator
    int splitColor; // Color for the splitting
    int splitKey; // Key for the splitting
    bool globalMaster; // Tells us if we are on the global master or not
    bool localMaster; // Tells us if we are on the local master or not

    // Variables controlling the size of the computation
    int nFields;
    int nKx;
    int nKy;
    double dKx;
    double dKy;

};

#endif
