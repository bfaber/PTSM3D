#ifndef _DEFINITIONS_HPP_
#define _DEFINITIONS_HPP_

#include <vector>
#include <complex>
#include <string>
#include <unordered_map>
#include <limits>
#include <utility>
#include <typeinfo>
#include <typeindex>

#include "mpi.h"
#include "blaze/math/DynamicMatrix.h"
#include "blaze/math/DynamicVector.h"

#if defined(DEBUG)
#define DebugMsg(x) do { x } while(0)
#else
#define DebugMsg(x) do { } while(0)
#endif

const double machEps = std::numeric_limits<double>::denorm_min();

// Define some quantities to save typing

using u_int = unsigned int;
using ul_int = long unsigned int;
using c_double = std::complex<double>;
using intVec = blaze::DynamicVector<int,blaze::columnVector>;
using intVec_row = blaze::DynamicVector<int,blaze::rowVector>;
using intMat = blaze::DynamicMatrix<int,blaze::rowMajor>;
using dblVec = blaze::DynamicVector<double,blaze::columnVector>;
using dblVec_row = blaze::DynamicVector<double,blaze::rowVector>;
using cmplxVec = blaze::DynamicVector<c_double,blaze::columnVector>;
using cmplxVec_row = blaze::DynamicVector<c_double,blaze::rowVector>;
using dblMat = blaze::DynamicMatrix<double,blaze::rowMajor>;
using dblMat_col = blaze::DynamicMatrix<double,blaze::columnMajor>;
using cmplxMat = blaze::DynamicMatrix<c_double,blaze::rowMajor>;
using cmplxMat_col = blaze::DynamicMatrix<c_double,blaze::columnMajor>;

// Use StringMap for the parameter strings
using StringPair = std::pair<std::string,char>;
using StringMap = std::unordered_map<std::string,std::string>;
using StringPairMap = std::unordered_map<std::string,StringPair>;

// Map for the MPI partitioning
//using MPICommMap = std::unordered_map<const int,const MPI_Comm>;
#endif
