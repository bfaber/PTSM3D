#include <iostream>
#include <map>
#include <memory>
#include <utility>
#include <algorithm>
#include <complex>
#include <string>

#include "MPIUtils.hpp"
#include "Utils.hpp"
#include "Definitions.hpp"
#include "Geom.hpp"
#include "PTSM3DControl.hpp"

void MPIUtils::passGeometry(const int mpiRank, std::shared_ptr<Geom>& geom, PTSM3DControl* const controller) {

  MPI_Comm comm;
  controller->getGlobalComm(comm);
  int dataSize;
  if (mpiRank == 0) dataSize = geom->getNPts();
  MPI_Bcast(&dataSize,1,MPI_INT,0,comm);
  if (mpiRank != 0) geom->setGeomSize(dataSize);

  dblVec geomData(dataSize);
  double* geomBuffer = new double[dataSize]; // This pointer points to data in the vector
                                             // thus you can't free it and then deallocate
                                             // the vector, hence no `delete[] geomBuffer`
                                             // at the end of the function

  std::map<std::string,geomDataPtr> geomDataMap;
  std::map<std::string,geomFuncPtr> geomFuncMap;
  geomDataMap["g11"] = &Geom::getGxx;
  geomDataMap["g12"] = &Geom::getGxy;
  geomDataMap["g22"] = &Geom::getGyy;
  geomDataMap["bmag"] = &Geom::getModB;
  geomDataMap["jac"] = &Geom::getJac;
  geomDataMap["curv_drift_x2"] = &Geom::getDBdx;
  geomDataMap["curv_drift_x1"] = &Geom::getDBdy;
  geomDataMap["d_B_d_x3"] = &Geom::getDBdz;
  geomDataMap["x3"] = &Geom::getEta;

  geomFuncMap["g11"] = &Geom::setGxx;
  geomFuncMap["g12"] = &Geom::setGxy;
  geomFuncMap["g22"] = &Geom::setGyy;
  geomFuncMap["bmag"] = &Geom::setModB;
  geomFuncMap["jac"] = &Geom::setJac;
  geomFuncMap["curv_drift_x2"] = &Geom::setDBdx;
  geomFuncMap["curv_drift_x1"] = &Geom::setDBdy;
  geomFuncMap["d_B_d_x3"] = &Geom::setDBdz;
  geomFuncMap["x3"] = &Geom::setEta;
  for (auto iter=geomDataMap.begin(); iter != geomDataMap.end(); ++iter) {
    // Head node passes the geometry
    if (mpiRank == 0) {
      geomData = geom->getGeomField(iter->second);
      geomBuffer = geomData.data();
    }

    MPI_Bcast(geomBuffer,dataSize,MPI_DOUBLE,0,comm);
    if (mpiRank != 0) {
    // Worker nodes receive geometry
      for (auto i = 0; i < dataSize; ++i) geomData[i] = geomBuffer[i];
      //geomData = geomBuffer;
      geom->setGeomField(geomData,geomFuncMap[iter->first]);
    }
    MPI_Barrier(comm);
  }

  double geomElem;
  if (mpiRank == 0) geomElem = geom->getq0();
  MPI_Bcast(&geomElem,1,MPI_DOUBLE,0,comm);
  if (mpiRank != 0) geom->setq0(geomElem);
  MPI_Barrier(comm);

  if (mpiRank == 0) geomElem = geom->getShat();
  MPI_Bcast(&geomElem,1,MPI_DOUBLE,0,comm);
  if (mpiRank != 0) geom->setShat(geomElem);
  MPI_Barrier(comm);

  if (mpiRank == 0) geomElem = geom->getInvEps();
  MPI_Bcast(&geomElem,1,MPI_DOUBLE,0,comm);
  if (mpiRank != 0) geom->setInvEps(geomElem);
  MPI_Barrier(comm);

  if (mpiRank == 0) geomElem = geom->getDEta();
  MPI_Bcast(&geomElem,1,MPI_DOUBLE,0,comm);
  if (mpiRank != 0) geom->setDEta(geomElem);
  MPI_Barrier(comm);

  int igeomElem;
  if (mpiRank == 0) igeomElem = geom->getNfp();
  MPI_Bcast(&igeomElem,1,MPI_INT,0,comm);
  if (mpiRank != 0) geom->setNfp(igeomElem);
  MPI_Barrier(comm);
}

void MPIUtils::passParamMap(const int mpiRank, PTSM3DControl* controller) {
  int nEntries, keySize, value0Size, value1Size;
  char *key, *value0, *value1;
  MPI_Comm comm;
  controller->getGlobalComm(comm);
  int mpiSize = controller->getGlobalSize();
  std::shared_ptr<StringPairMap> paramInfoPtr(controller->getParamInfoPtr());
  StringPairMap::iterator iter = paramInfoPtr->begin();
  std::string keyString, valueString;
  StringPair valuePair = std::make_pair(std::string(),'\0');
  if (mpiRank == 0) {
    nEntries = paramInfoPtr->size();
  }
  MPI_Bcast(&nEntries,1,MPI_INT,0,comm);
  MPI_Barrier(comm);

  for (auto i=0; i<nEntries; ++i) {
    if (mpiRank == 0) {
      keySize = Utils::trim_copy((iter->first)).size();
      key = (char*)Utils::trim_copy((iter->first)).c_str();
    }
    MPI_Bcast(&keySize,1,MPI_INT,0,comm);
    MPI_Barrier(comm);
    if (mpiRank != 0) key = new char[keySize];
    MPI_Bcast(key,keySize,MPI_CHAR,0,comm);
    MPI_Barrier(comm);

    if (mpiRank == 0) {
      value0Size = Utils::trim_copy(std::get<0>(iter->second)).size();
      value0 = (char*)std::get<0>(iter->second).c_str();
      value1 = &std::get<1>(iter->second);
      value1Size = strlen(value1);
    }
    MPI_Bcast(&value0Size,1,MPI_INT,0,comm);
    MPI_Barrier(comm);
    MPI_Bcast(&value1Size,1,MPI_INT,0,comm);
    MPI_Barrier(comm);
    if (mpiRank != 0) value0 = new char[value0Size];
    MPI_Bcast(value0,value0Size,MPI_CHAR,0,comm);
    MPI_Barrier(comm);
    if (mpiRank != 0) value1 = new char[value1Size];
    MPI_Bcast(value1,value1Size,MPI_CHAR,0,comm);
    MPI_Barrier(comm);

    if (mpiRank != 0) {
      keyString.assign(key,key+keySize);
      valueString.assign(value0,value0+value0Size);
      valuePair = std::make_pair(valueString,value1[0]);
      paramInfoPtr->at(keyString) = valuePair;
    }
    if (mpiRank == 0)  ++iter;
    
    MPI_Barrier(comm);
  }

}


void MPIUtils::passCmplxVec(cmplxVec& vec, const int mpiSourceRank,
                            const int mpiTargetRank, PTSM3DControl* controller, bool global) {
  MPI_Comm comm;
  int rank;
  if (global) {
    controller->getGlobalComm(comm);
    rank = controller->getGlobalRank();
  } else {
    controller->getLocalComm(comm);
    rank = controller->getLocalRank();
  }
  if (rank == mpiSourceRank || rank == mpiTargetRank) {
    int dataSize;
    std::complex<double>* dataBuffer;

    auto tag = (mpiSourceRank+1)*(mpiTargetRank+1);
    if (rank == mpiSourceRank) {
      dataSize = vec.size(); 
      MPI_Send(&dataSize,1,MPI_INT,mpiTargetRank,0,comm);
      dataBuffer = vec.data();
      MPI_Send(dataBuffer,dataSize,MPI_C_DOUBLE_COMPLEX,mpiTargetRank,tag,comm);
    } else {
      MPI_Recv(&dataSize,1,MPI_INT,mpiSourceRank,0,comm,MPI_STATUS_IGNORE);
      MPI_Recv(dataBuffer,dataSize,MPI_C_DOUBLE_COMPLEX,mpiSourceRank,tag,comm,MPI_STATUS_IGNORE);
      vec.resize(dataSize);
      for (auto i = 0; i < dataSize; ++i) vec[i] = dataBuffer[i];
    }
  }
} 

void MPIUtils::passDblVec(dblVec& vec, const int mpiSourceRank,
                          const int mpiTargetRank, PTSM3DControl* controller, bool global) {
  MPI_Comm comm;
  int rank;
  if (global) {
    controller->getGlobalComm(comm);
    rank = controller->getGlobalRank();
  } else {
    controller->getLocalComm(comm);
    rank = controller->getLocalRank();
  }
  if (rank == mpiSourceRank || rank == mpiTargetRank) {
    int dataSize;
    if (rank == mpiSourceRank) {
      dataSize = vec.size(); 
      MPI_Send(&dataSize,1,MPI_INT,mpiTargetRank,0,comm);
    } else {
      MPI_Recv(&dataSize,1,MPI_INT,mpiSourceRank,0,comm,MPI_STATUS_IGNORE);
    }

    double* dataBuffer = new double[dataSize];

    auto tag = (mpiSourceRank+1)*(mpiTargetRank+1);
    if (rank == mpiSourceRank) {
      dataBuffer = vec.data();
      MPI_Send(dataBuffer,dataSize,MPI_DOUBLE,mpiTargetRank,tag,comm);
    } else {
      MPI_Recv(dataBuffer,dataSize,MPI_DOUBLE,mpiSourceRank,tag,comm,MPI_STATUS_IGNORE);
      for (auto i = 0; i < dataSize; ++i) vec[i] = dataBuffer[i];
    }
  }
}
