#include <fstream>
#include <typeinfo>
#include <typeindex>
#include <sstream>
#include <random>

#include "PTSM3DControl.hpp"
#include "Utils.hpp"
#include "MPIUtils.hpp"

PTSM3DControl::PTSM3DControl() {
  setParameterInfo();
  paramInfoPtr = std::make_shared<StringPairMap>(parameterInfo);
  globalComm = MPI_COMM_NULL;
  geomPtr = std::make_shared<Geom>(geom);
  //spectrumPtr = std::make_shared<DriftWaveSpectrum>(spectrum);
}

PTSM3DControl::PTSM3DControl(MPI_Comm comm) : globalComm(comm) {
  setParameterInfo();
  paramInfoPtr = std::make_shared<StringPairMap>(parameterInfo);
  MPI_Comm_size(globalComm,&mpiGlobalSize);
  MPI_Comm_rank(globalComm,&mpiGlobalRank);
  globalMaster = mpiGlobalRank != 0 ? false : true;
  geomPtr = std::make_shared<Geom>(geom);
  //spectrumPtr = std::make_shared<DriftWaveSpectrum>(spectrum);
}

std::shared_ptr<PTSM3DControl> PTSM3DControl::getController() {
  return shared_from_this();
}

/*
 * setParameterInfo
 *
 * Called on the PTSM3DControl constructor and initializes the default values
 * and types for all of the contolling variables
 */
void PTSM3DControl::setParameterInfo() {
  parameterInfo["nkx"] = std::make_pair("21",'i');
  parameterInfo["nky"] = std::make_pair("21",'i');
  parameterInfo["dkx"] = std::make_pair("0.05",'d');
  parameterInfo["dky"] = std::make_pair("0.05",'d');
  parameterInfo["nFields"] = std::make_pair("3",'i');
  parameterInfo["nzfp"] = std::make_pair("101",'i');
  parameterInfo["errOrder"] = std::make_pair("2",'i');
  parameterInfo["kxSpecLimit"] = std::make_pair("0.2",'d');
  parameterInfo["kySpecLimit"] = std::make_pair("0.6",'d');
  parameterInfo["kxSpecPower"] = std::make_pair("2.0",'d');
  parameterInfo["kySpecPower"] = std::make_pair("1.3333333333",'d');
  parameterInfo["geomDir"] = std::make_pair(".",'s');
  parameterInfo["geomFile"] = std::make_pair("",'s');
  parameterInfo["tag"] = std::make_pair("",'s');
  parameterInfo["outDir"] = std::make_pair(".",'s');
  parameterInfo["geomType"] = std::make_pair("eq",'s');
  parameterInfo["eqType"] = std::make_pair("vmec",'s');
  parameterInfo["optTarget"] = std::make_pair("0",'i');
  parameterInfo["nev"] = std::make_pair("1",'i');
  parameterInfo["ncv"] = std::make_pair("64",'i');
  parameterInfo["mpd"] = std::make_pair("64",'i');
  parameterInfo["maxIt"] = std::make_pair("100000",'i');
  parameterInfo["tol"] = std::make_pair("1e-4",'d');
  parameterInfo["which_ev"] = std::make_pair("eps",'s');
  parameterInfo["buffer"] = std::make_pair("10",'i');
  parameterInfo["hypzEps"] = std::make_pair("2.00",'d');
  parameterInfo["hypzOrder"] = std::make_pair("2",'i');
  parameterInfo["tauTemp"] = std::make_pair("1.0",'d');
  parameterInfo["omt"] = std::make_pair("3.0",'d');
  parameterInfo["omn"] = std::make_pair("0.0",'d');
  parameterInfo["s0"] = std::make_pair("0.5",'d');
  

  // s-alpha parameters
  parameterInfo["q0_salpha"] = std::make_pair("0.796",'d');
  parameterInfo["shat_salpha"] = std::make_pair("1.4",'d');
  parameterInfo["invEps_salpha"] = std::make_pair("0.18",'d');

}

/*
* PTSM3D::readInputFile
*
* Brief: read input from the PTSM3D.inp and set the parameter value
* based on the parameter string label
*
* Input parameters:
*  - const std::string inpFile: string representation of the input file
*
*/
void PTSM3DControl::readInputFile(const std::string inpFile)
{
  if (globalMaster) {
    // Use std::map to map the input to variables
    std::string line, key, value;
    const std::string eqSign = "="; // The string character on which to split the strings
    std::fstream iFile;
    iFile.open(inpFile,std::ios_base::in); // Open the input file

    // Read the file line by line
    auto peekChar = iFile.peek();
    while ( peekChar != EOF)
    {
      // Read the file line
      std::getline(iFile,line);

      // Check for emtpy lines and discard
      if (!line.empty())
      {
        std::size_t eqPos = line.find(eqSign);
        // If line.find() cannot find eqSign, then it returns string::npos
        if (eqPos < std::string::npos)
        {
          // Make sure the trimmed string for the parameter string is not empty
          // Throw an exception and exit if empty parameter string encountered
          key = (eqPos > 0) ? !(Utils::trim_copy(line.substr(0,eqPos-1))).empty() ?
            Utils::trim_copy(line.substr(0,eqPos-1)) :
            throw(std::runtime_error("PTSM3D Error! Empty parameter key identfier in PTSM3D.inp")) :
            throw(std::runtime_error("PTSM3D Error! Empty parameter key identfier in PTSM3D.inp"));

          // Make sure the trimmed string for the parameter value is not empty
          // Throw an exception and exit if empty parameter value encountered
          value = (eqPos < line.length()-1) ?
            !(Utils::trim_copy(line.substr(eqPos+1,line.length()-1))).empty() ?
            Utils::trim_copy(line.substr(eqPos+1,line.length()-1)) :
            throw(std::runtime_error("PTSM3D Error! Empty parameter value in PTSM3D.inp")) :
            throw(std::runtime_error("PTSM3D Error! Empty parameter value in PTSM3D.inp"));

          // Set the map using the parameter key and value this->setInputParameters(key,value);
          this->setInputParameters(key,value);
        }
      }

      // Look at the next line for EOF
      peekChar = iFile.peek();
    }

    // Update the parameterInfo map with the input parameters map
    for (auto iter = inputParameters.begin(); iter != inputParameters.end(); iter++) {
      auto parInfoIter = parameterInfo.find(iter->first);
      if (parInfoIter != parameterInfo.end()) {
        auto replacePair = std::make_pair(Utils::trim_copy(iter->second),std::get<1>(parInfoIter->second));
        paramInfoPtr->at(iter->first) = replacePair;

      } else {
        throw std::runtime_error("Parameter "+iter->first+" has no associated information!");
      } 
    }
  }
  // Global master has read the input file, now it's time to pass the parameterInfo map to the other procs
  if (mpiGlobalSize > 1) MPIUtils::passParamMap(mpiGlobalRank, this);
}


/*
 * Test function for viewing the state of the parameterInfo map
 */
void PTSM3DControl::viewParameterInfo() {
  for (auto i = 0; i < mpiGlobalSize; ++i) {
    MPI_Barrier(globalComm);
    for (auto iter = paramInfoPtr->begin(); iter != paramInfoPtr->end(); iter++) {
      if (i == mpiGlobalRank) {
        char type = std::get<1>(iter->second);
        if (type == 'd') {
          double val;
          Utils::getParameterValue(val,iter->first,paramInfoPtr);
          std::cout << "Rank: " << mpiGlobalRank << " with key " << iter->first << " and value " << val << '\n';
        } else if (type == 'i') {
          int val;
          Utils::getParameterValue(val,iter->first,paramInfoPtr);
          std::cout << "Rank: " << mpiGlobalRank << " with key " << iter->first << " and value " << val << '\n';
        } else if (type == 's') {
          std::string val;
          Utils::getParameterValue(val,iter->first,paramInfoPtr);
          std::cout << "Rank: " << mpiGlobalRank << " with key " << iter->first << " and value " << val << '\n';
        } else {
          throw std::runtime_error("Cannot obtain type!");
        }
      }
    }
    MPI_Barrier(globalComm);
  }
}

void PTSM3DControl::setGlobalComm(const MPI_Comm& comm) {
  if (globalComm == MPI_COMM_WORLD) {
    throw std::runtime_error("Cannot free MPI_COMM_WORLD!");
  } else {
    MPI_Comm_free(&globalComm);
    globalComm = comm;
    MPI_Comm_size(globalComm,&mpiGlobalSize);
    MPI_Comm_rank(globalComm,&mpiGlobalRank);
    globalMaster = mpiGlobalRank != 0 ? false : true;
  }
}

int PTSM3DControl::partitionMPIComm(const int n_split_groups) {
  auto nSplitGroups = mpiGlobalSize % (mpiGlobalSize/n_split_groups) != 1 ? n_split_groups : n_split_groups-1 != 0 ? n_split_groups-1 : 1;
  auto groupSize = mpiGlobalSize/nSplitGroups;
  int color = (mpiGlobalRank+1) <= groupSize*(nSplitGroups-1) ? mpiGlobalRank / groupSize : nSplitGroups-1;
  MPI_Comm_split(globalComm, color, mpiGlobalRank, &localComm);
  MPI_Comm_size(localComm, &mpiLocalSize);
  MPI_Comm_rank(localComm, &mpiLocalRank);
  localMaster = mpiLocalRank != 0 ? false : true;

  //commMap[mpiGlobalRank] = localComm;

  // Compute the partitioning of the DriftWave spectrum
  Utils::getParameterValue(nKx,"nkx",paramInfoPtr);
  Utils::getParameterValue(nKy,"nky",paramInfoPtr);
  Utils::getParameterValue(dKx,"dkx",paramInfoPtr);
  Utils::getParameterValue(dKy,"dky",paramInfoPtr);
  Utils::getParameterValue(nFields,"nFields",paramInfoPtr);
  auto nModes = nKx*nKy;
  auto kyPerGroup = nKy/groupSize;
  // For now we will have kyPerGroup = 1
  kyPerGroup = 1;
  for (auto k = 0; k < nKy; ++k) {
    auto km = k-1;
    auto kp = k+1 < nKy ? k+1 : -1;
    neighborMap[k] = std::make_pair(km,kp);
  }
    
  auto probSize = geomPtr->getNPts();
  // Set up the drift wave spectrum objects
  if (globalMaster) {  
    spectrum.setSpectrumSize(nKx,nKy,probSize,nFields);
  }// else if (localMaster) {
   // spectrum.setSpectrumSize(nKx,kyPerGroup,probSize,nFields);
 // }
  for (auto i = 0; i < mpiGlobalSize; ++i) if (i == mpiGlobalRank) DebugMsg(std::cout << i << ": " << localMaster << '\n';);
  if (localMaster) {
    localSpectrum.setSpectrumSize(nKx,kyPerGroup,probSize,nFields);

  }
  // Partitioning will 

  return nSplitGroups;
}


void PTSM3DControl::initializeGeometry() {
  if (globalMaster) {
    std::string geomType;
    Utils::getParameterValue(geomType,"geomType",paramInfoPtr);
    if ( geomType == "eq" || geomType == "s_alpha" ) {
      geomPtr->initializeGeometry(paramInfoPtr);
    } else {
      throw std::runtime_error("Equilibrium specified by "+geomType+" does not exist!");
    }
  }

  if (mpiGlobalSize > 1) MPIUtils::passGeometry(mpiGlobalRank, geomPtr, this);
}


void PTSM3DControl::testVectorPass(const int n) {
  dblVec testVec(n);
  testVec = 0.0;

  if (globalMaster) {
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(0.0,1.0);
    for (auto i = 0; i < n; ++i) {
      testVec[i] = dis(gen);
    }
  }
  MPIUtils::passDblVec(testVec,0,1,this,true);
  if (mpiGlobalRank == 1 || mpiGlobalRank == 2) MPIUtils::passDblVec(testVec,1,2,this,true);
  DebugMsg(std::cout << "Rank " << mpiGlobalRank << " is here" << '\n';);
  for (auto i = 0; i < mpiGlobalSize; ++i) {
    MPI_Barrier(globalComm);
    if (i == mpiGlobalRank) {
      DebugMsg(std::cout << "Proc: " << mpiGlobalRank << '\n';);
      for (auto j = 0; j < n; ++j) DebugMsg(std::cout << testVec[j] << '\t';);
      DebugMsg(std::cout << '\n';);
    }
    MPI_Barrier(globalComm);
  }
}
