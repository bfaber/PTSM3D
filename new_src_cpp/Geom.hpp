#ifndef GEOM_HPP
#define GEOM_HPP

#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include <functional>
#include <memory>

#include "Definitions.hpp"

class Geom;
// Define a function pointer to Geom class functions that takes a vector of doubles and returns void
using geomFuncPtr = void(Geom::*)(const dblVec&);
using geomDataPtr = dblVec(Geom::*)();

class Geom
{
  public:
    // Public member variables
    dblVec gxx, gxy, gyy, modB, jac, dBdx, dBdy, dBdz, eta, modBInv, jacBInv,
          dparModBInv, dpar2ModBInv, dparJacBInv;
    Geom();
    void initializeGeometry(const std::shared_ptr<StringPairMap>& paramInfoPtr);
    void setGeomSize(const int nz);

    inline void setGeomField(const dblVec& fieldData, geomFuncPtr f) { (this->*f)(fieldData); };
    inline void setGxx(const dblVec& x) { this->gxx = x; };
    inline void setGxy(const dblVec& x) { this->gxy = x; };
    inline void setGyy(const dblVec& x) { this->gyy = x; };
    inline void setModB(const dblVec& x) { this->modB = x; };
    inline void setJac(const dblVec& x) { this->jac = x; };
    inline void setDBdx(const dblVec& x) { this->dBdx = x; };
    inline void setDBdy(const dblVec& x) { this->dBdy = x; };
    inline void setDBdz(const dblVec& x) { this->dBdz = x; };
    inline void setEta(const dblVec& x) { this->eta = x; };

    inline dblVec getGeomField(geomDataPtr f) { return (this->*f)(); };
    inline dblVec getGxx() { return this->gxx; };
    inline dblVec getGxy() { return this->gxy; };
    inline dblVec getGyy() { return this->gyy; };
    inline dblVec getModB() { return this->modB; };
    inline dblVec getJac() { return this->jac; };
    inline dblVec getDBdx() { return this->dBdx; };
    inline dblVec getDBdy() { return this->dBdy; };
    inline dblVec getDBdz() { return this->dBdz; };
    inline dblVec getEta() { return this->eta; };


    // Functions for computing model quantites, like Bk, Dk and the parallel derivatives of B

    // buildBk: compute the polarization term Bk for a given (kx,ky) over a parallel range
    // specific by the indices idx1 (low) and idx2(high)
    // Fills the result in the preallocated vector vecBk
    void buildBk(const double kx, const double ky, const int idx1, const int idx2,
                 dblVec& vecBk);

    // buildDk: compute the curvature term Dk for a given (kx,ky) over a parallel range
    // specific by the indices idx1 (low) and idx2(high)
    // Fills the result in the preallocated vector vecDk
    void buildDk(const double kx, const double ky, const int idx1, const int idx2,
                 dblVec& vecDk);
    int findZeroIndex();

    void computeDerivedQuantities(const int errOrder);
   // void callVmec2Pest(std::string eqFile, double surface, const double dky, const int nz0, const int buffer);

    // Function for computing the s-alpha geometry of np poloidal turns
    void s_alpha(const double q0, const double shat, const double invEps, const double np);

    // Routines for accessing private variables
    inline void setq0(const double x) { q0 = x; };
    inline void setShat(const double x) { shat = x; };
    inline void setInvEps(const double x) { invEps = x; };
    inline void setDEta(const double x) { dEta = x; };
    inline void setNPts(const int x) { nPts = x; };
    inline void setNfp(const int x) { nfp = x; };
    inline auto getq0() const { return q0; };
    inline auto getShat() const { return shat; };
    inline auto getInvEps() const { return invEps; };
    inline auto getNPts() const { return nPts; } ;
    inline auto getDEta() const { return dEta; } ;
    inline auto getNfp() const { return nfp; };

  private:
    double q0, shat, invEps, dEta;
    int nPts, nfp;
    std::string geomFile;

    // This map maps string values that are needed to get data from vmec2pest
    // with function pointers pointing to the function necessary to set the data
    std::unordered_map<std::string,geomFuncPtr> v2p_DiagMap;
};
#endif
