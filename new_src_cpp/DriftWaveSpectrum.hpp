#ifndef _DRIFTWAVESPECTRUM_HPP_
#define _DRIFTWAVESPECTRUM_HPP_

#include <memory>

#include "blaze/math/DynamicVector.h"
#include "blaze/math/DynamicMatrix.h"

#include "Definitions.hpp"

class DriftWaveSpectrum {
public:
  DriftWaveSpectrum();
  DriftWaveSpectrum(const int nkx, const int nky, const int nz, const int nf);

  void setSpectrumSize(const int nkx, const int nky, const int nz, const int nf);
  void setSpectrumSize();
  //Function to return the array row based on (kx,ky) index
  inline int getKxKyRow(const int ix, const int iy) { return nKxModes*iy + ix; };

private:
  int probSize;
  int nModes;
  int nKxModes;
  int nKyModes;
  int nFields;
  std::unique_ptr<cmplxMat[]> eigVecs;
  std::unique_ptr<cmplxMat> eigVals;
  std::unique_ptr<cmplxMat> massMatrix;
};

#endif
