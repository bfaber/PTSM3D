#ifndef _PTSM3DDRIVER_HPP_
#define _PTSM3DDRIVER_HPP_

#include <memory>

#include "slepc.h"
#include "mpi.h"

#include "Definitions.hpp"
#include "PTSM3DControl.hpp"

class FluidModel;

class PTSM3DDriver {
  public:
    inline PTSM3DDriver() {};
    inline ~PTSM3DDriver() {};
    void initializeLocalDriver(const int n_Local_Comms, const std::shared_ptr<PTSM3DControl> control_ptr);
    void finalizeLocalDriver();

    void computeDriftWaveSpectrum();
    void computeKxZeroMode();
    void computeKxSpectrum();
    inline void setInitialCondition(const dblVec& init_cond) { initCond = init_cond; };

  private:
    std::shared_ptr<FluidModel> model;
    std::shared_ptr<PTSM3DControl> controller;
    int nLocalComms;
    int nFields;
    dblVec initCond;
    int nKxModes;
    int nKyModes;
    int localKxModes;
    int localKyModes;
};

#endif
