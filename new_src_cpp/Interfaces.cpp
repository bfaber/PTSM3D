#include <iostream>
#include <string>
#include <unordered_map>
#include <complex>

#include "slepc.h"

#include "Interfaces.hpp"
#include "Utils.hpp"
#include "PTSM3DControl.hpp"
#include "Geom.hpp"

// CObject defined in Interfaces.hpp

/*
CObject createPTSM3D()
{
  SlepcInitialize(0,(char***)0,(char*)0,(char*)0);
  PTSM3D *SatModel = new PTSM3D;
  std::cout << "Creating PTSM3D" << '\n';
  return (CObject)SatModel;
}

void destroyPTSM3D(CObject CObj)
{
  PTSM3D *SatModel = (PTSM3D *)CObj;
  std::cout << "Destorying PTSM3D" << '\n';
  delete(SatModel);
  SlepcFinalize();
  return;
}

void updatePTSM3DParam(CObject cPtr, char** key, char** val)
{
  std::string keyString, valString;
  PTSM3D *SatModel = (PTSM3D *)cPtr;
  std::cout << *key << '\n';
  keyString = *key;
  valString = *val;
  std::cout << "Setting parameter " << keyString << " to " << valString << '\n';
  SatModel->setParamString(keyString,valString);
  SatModel->viewParamString(keyString);
  return;
}

void callPTSM3D(CObject cPtr, double* re_freq, double* im_freq)
{
  PTSM3D *SatModel = (PTSM3D *)cPtr;
  double *reFreq = re_freq;
  double *imFreq = im_freq;
  std::complex<double> linFreq;

  SatModel->setupPTSM3D();
  SatModel->computeLinearSpectrum();
  linFreq = SatModel->getLinFreq();
  std::cout << "Linear frequency is: " << linFreq << '\n';
  *reFreq = std::real(linFreq);
  *imFreq = std::imag(linFreq);
  return;
}

void Interfaces::PTSM3DStelloptInterface()
{
}
*/

void Interfaces::callVmec2Pest(std::string eqFile, double surface, const double dky,
                               const int nz0, const int buffer, Geom* geom)
{
  std::unordered_map<std::string,geomFuncPtr> v2p_DiagMap;
  char *diag_name;
  std::string diag_str;
  int x1, x2, data_size, n_dims;
  double* c_data;

  v2p_opts vmec2pest_options;
  char *file = &eqFile[0u];
  char gridType[] = "gene";
  char x3Coord[] = "zeta";
  char normType[] = "minor_r";
  vmec2pest_options.vmec_file = file;
  vmec2pest_options.grid_type = gridType;
  vmec2pest_options.x3_coord = x3Coord;
  vmec2pest_options.norm_type = normType;

  vmec2pest_options.x1 = &surface;
  vmec2pest_options.nx1 = 1;
  vmec2pest_options.nx2 = 1;
  vmec2pest_options.nx3 = 1;

  vmec2pest_options.nfpi = 1.0;
  vmec2pest_options.x2_center = 0.0;
  vmec2pest_options.x3_center = 0.0;
  DebugMsg(std::cout << "Calling vmec2pest_c_interface" << '\n';);
  vmec2pest_c_interface(&vmec2pest_options);

  diag_str = "nfp";
  diag_name = &diag_str[0u];
  x1 = 0;
  x2 = 0;
  n_dims = -1;
  data_size = 1;
  c_data = new double[data_size];
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  geom->setNfp((int)c_data[0]);

  diag_str = "shat";
  diag_name = &diag_str[0u];
  n_dims = 0;
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  geom->setShat(c_data[0]);

  diag_str = "safety_factor_q";
  diag_name = &diag_str[0u];
  n_dims = 0;
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  geom->setq0(c_data[0]);

  delete[] c_data;

  //Compute the maximum distance needed to compute along a field line
  auto max_z = 1.0/(std::abs(geom->getShat())*dky);
  data_size = (std::ceil(max_z/Utils::pi()) + buffer)*geom->getNfp()*nz0;
  vmec2pest_options.nx3 = data_size;
  vmec2pest_options.nfpi = (double)((std::ceil(max_z/Utils::pi())+buffer)*geom->getNfp());
  data_size += 1;
  vmec2pest_c_interface(&vmec2pest_options);

  v2p_DiagMap["g11"] = &Geom::setGxx;
  v2p_DiagMap["g12"] = &Geom::setGxy;
  v2p_DiagMap["g22"] = &Geom::setGyy;
  v2p_DiagMap["bmag"] = &Geom::setModB;
  v2p_DiagMap["jac"] = &Geom::setJac;
  v2p_DiagMap["curv_drift_x2"] = &Geom::setDBdx;
  v2p_DiagMap["curv_drift_x1"] = &Geom::setDBdy;
  v2p_DiagMap["d_B_d_x3"] = &Geom::setDBdz;
  v2p_DiagMap["x3"] = &Geom::setEta;

  dblVec temp;
  n_dims = 1;
  geom->setGeomSize(data_size);
  temp.resize(data_size+1);
  c_data = new double[data_size+1];
  for (std::unordered_map<std::string,geomFuncPtr>::iterator iter=v2p_DiagMap.begin(); iter != v2p_DiagMap.end(); ++iter)
  {
    diag_str = iter->first;
    diag_name = &diag_str[0u];
    get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
    for (auto i = 0; i < data_size; ++i) temp[i] = c_data[i];
    geom->setGeomField(temp,iter->second);
  }
  delete[] c_data;
  geom->setDEta((geom->eta[1]-geom->eta[0])/Utils::pi());
}
