#ifndef _MPIUTILS_HPP_
#define _MPIUTILS_HPP_
#include <memory>

//#include "mpi.h"

#include "Definitions.hpp"

class Geom;
class PTSM3DControl;

namespace MPIUtils {
  void passGeometry(const int mpiRank, std::shared_ptr<Geom>& geom, PTSM3DControl* const controller);
  void passParamMap(const int mpiRank, PTSM3DControl* controller);
  void passCmplxVec(cmplxVec&, const int mpiSourceRank, const int mpiTargetRank,
                    PTSM3DControl* controller, bool global=true);
  void passDblVec(dblVec&, const int mpiSourceRank, const int mpiTargetRank,
                  PTSM3DControl* controller, bool global=true);
}

#endif
