// STL includes
#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <cmath>
#include <string>
#include <algorithm>
#include <utility>

// External includes
//#include "petsc.h"

// Local includes
#include "Definitions.hpp"
#include "ITG3Field.hpp"
#include "Geom.hpp"
#include "SlepcSolver.hpp"
#include "Utils.hpp"
#include "FluidModel.hpp"

ITG3Field::ITG3Field() : FluidModel{} {}

ITG3Field::ITG3Field(const int n_fields) : FluidModel{n_fields} {}

ITG3Field::~ITG3Field() {}

void ITG3Field::buildLinearOperators(SlepcSolver::SolverContext& sc,
                                     const double kx, const double ky)
{
  if (sc.probType == "eps") {
    this->buildEPSOperators(kx,ky);
  } else if (sc.probType == "pep") {
    this->buildPEPOperators(kx,ky);
  }
}

void ITG3Field::extractSolution(const SlepcSolver::SolverContext& sc,
                                const double kx, const double ky)
{
  if (sc.probType == "eps") {
    this->extractEPSSolution(sc,kx,ky);
  } else if (sc.probType == "pep") {
    this->extractPEPSolution(sc,kx,ky);
  }
}

/******************************************************************************
*                                                                             *
* Routines to solve the eigenvalue problem using the SLEPc EPS functions      *
*                                                                             *
******************************************************************************/

/*
  Solving the generalized eigenvalue problme A*x = lambda B*x for the eigenvector
  x = (phi, v_parallel, T)
  The EPS problem has operators laid out in block matrix form corresponding
  to the different fields.  The left-hand-side operator has the form

       phi   v    T
      | A0 | A1 | A2 | N rows
  A = | A3 | A4 | A5 | N rows
      | A6 | A7 | A8 | N rows

  while the right hand side operator has the form

      | B0 | 0 | B1 | N rows
  B = | 0  | 1 | 0  | N rows
      | 0  | 0 | 1  | N rows
*/
/*
void ITG3Field::buildEPSOperators(const double kx, const double ky)
{
  // Const definitions
  PetscViewer viewer;
  const PetscScalar piInv = 1.0/Utils::pi();
  const PetscReal dz = geomPtr->getDEta();
  const PetscInt one = 1;

  // PetscInt pointers for the array column indices used in SetMatValues
  PetscInt *colInds, *bcolInds, *colHypzInds, *bcolHypzInds;

  // PetscScalar pointers to the data structures used in SetMatValues
  PetscScalar *pdc, *pdc1, *pdsc, *bpdc, *bpdc1, *hypzc, *bhypzc, *ihypzc;

  // Integers controlling array bounds/indices
  PetscInt i, j, k, globalIdx, nHypzPts, nSplinePts, li1, li2, sBound, hypzBound, blockRow;

  // Internal variables
  PetscInt pdOrder;
  PetscScalar hypz, ijacBInv, iDk, iBk, igamma;

  // Vectors containing the finite difference spline coefficients
  cmplxVec fdSpline, bfdSpline, hypzSpline;
  // Vectors containing the geometric factors Bk and Dk
  if (probSize != (FluidModel::getZBoundUpper() - FluidModel::getZBoundLower() + 1)) throw(std::runtime_error("PTSM3D Error! Vectors have incompatible sizes!"));
  d_Vec Bk(probSize), Dk(probSize);

  // Build the Bk and Dk vectors
  // TODO(bfaber): Compute the integer offset necessary for finite theta-k
  geomPtr->buildBk(kx,ky,FluidModel::getZBoundLower(),FluidModel::getZBoundUpper(),Bk);
  geomPtr->buildDk(kx,ky,FluidModel::getZBoundLower(),FluidModel::getZBoundUpper(),Dk);


  // TODO(bfaber): Move these constant definitions to read from input file
  pdOrder = 1;
  sBound = errOrder/2;
  hypzBound = hypzOrder/2;

  // Set the appropriate sizes for the vectors and resize
  nSplinePts = errOrder+1;
  nHypzPts = hypzOrder+1;
  fdSpline.resize(nSplinePts);
  hypzSpline.resize(nHypzPts);

  // Allocate memory for the Petsc pointers
  colHypzInds = new PetscInt[nHypzPts];
  bcolHypzInds = new PetscInt[hypzOrder];
  colInds = new PetscInt[nSplinePts];
  bcolInds = new PetscInt[errOrder];
  pdc = new PetscScalar[nSplinePts];
  pdc1 = new PetscScalar[nSplinePts];
  pdsc = new PetscScalar[nSplinePts];
  bpdc = new PetscScalar[errOrder];
  bpdc1 = new PetscScalar[errOrder];
  hypzc = new PetscScalar[nHypzPts];
  bhypzc = new PetscScalar[hypzOrder];
  ihypzc = new PetscScalar[nHypzPts];

  // Compute the parallel hyperdiffusion coefficient
  hypz = -(std::pow(PETSC_i,hypzOrder))*hypzEps*std::pow(0.5*dz,hypzOrder);

  // For the non-boundary matrix rows, no need to recompute the finite difference
  // spline, compute once here and reuse in the later loop
  SlepcSolver::FDCoeffGen(-sBound,sBound,pdOrder,dz,fdSpline);
  SlepcSolver::FDCoeffGen(-hypzBound,hypzBound,hypzOrder,dz,hypzSpline);

  for (j=0; j<=errOrder; ++j)
  {
    pdsc[j] = fdSpline[j];
  }

  for (j=0; j<=hypzOrder; ++j)
  {
    ihypzc[j] = hypz*hypzSpline[j];
  }


  // Necessary for parallel eigenvalue calculations
  MatGetOwnershipRange(linOpArr[0],&li1,&li2);

  for (k=li1; k<li2; ++k) // Start global index loop
  {
    // Calculate the block index i for row k
    i = k % probSize;
    blockRow = k/probSize;
    globalIdx = FluidModel::getZBoundLower()+i;

    // Calculate these quantities once rather than reading from array each time
    ijacBInv = geomPtr->jacBInv[globalIdx];
    igamma = -piInv*ijacBInv;
    iDk = Dk[i];
    iBk = Bk[i];

    // Compute the entries for the right-hand-side operator
    // B0 and B1 and the diagonal entries
    if (blockRow == 0)
    {
      MatSetValue(linOpArr[1],i,i,1.0+iBk*(1.0+5.0/3.0*tauTemp),ADD_VALUES);
      MatSetValue(linOpArr[1],i,2*probSize+i,iBk*tauTemp,ADD_VALUES);
    }
    else
    {
      MatSetValue(linOpArr[1],k,k,1.0,ADD_VALUES);
    }


    // Fill in the hyperdiffusions
    // This fills in the diagonal bands in blocks A0, A5 and A8
    if (i < hypzBound)
    {
      SlepcSolver::FDCoeffGen(-hypzBound+std::abs(hypzBound-i)-1,hypzOrder-i-1,hypzOrder,dz,hypzSpline);
      for (j=0; j<hypzOrder; ++j)
      {
        bcolHypzInds[j] = blockRow*probSize + i-hypzBound+std::abs(hypzBound-i)+j;
        bhypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*hypz*hypzSpline[j+1];
      }
      MatSetValues(linOpArr[0],one,&k,hypzOrder,bcolHypzInds,bhypzc,ADD_VALUES);
    }
    else if (i >= probSize-hypzBound)
    {
      SlepcSolver::FDCoeffGen(probSize-1-i-hypzOrder,probSize-1-i,hypzOrder,dz,hypzSpline);
      for (j=0; j<hypzOrder; ++j)
      {
        bcolHypzInds[j] = blockRow*probSize + probSize-hypzOrder+j;
        bhypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*hypz*hypzSpline[j];
      }
      MatSetValues(linOpArr[0],one,&k,hypzOrder,bcolHypzInds,bhypzc,ADD_VALUES);
    }
    else
    {
      for (j=0; j<=hypzOrder; ++j)
      {
        colHypzInds[j] = blockRow*probSize + i + j - hypzBound;
        hypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*ihypzc[j];
      }
      MatSetValues(linOpArr[0],one,&k,nHypzPts,colHypzInds,hypzc,ADD_VALUES);
    }


    // Fill linear operator
    // Deal with the boundary terms first
    if (i < sBound)
    {
      SlepcSolver::FDCoeffGen(-sBound+std::abs(sBound-i)-1,errOrder-i-1,pdOrder,dz,fdSpline);
      // Assign the entries for blocks A0, A1 and A2
      if (blockRow == 0)
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = probSize + i-sBound+std::abs(sBound-i)+j;
          bpdc[j] = -PETSC_i*igamma*fdSpline[j+1];
        }
        // Block A1
        MatSetValues(linOpArr[0],one,&i,errOrder,bcolInds,bpdc,ADD_VALUES);
        // Block A0
        MatSetValue(linOpArr[0],i,i,(ky*omn*+iDk*(1.0+5.0/3.0*tauTemp)),ADD_VALUES);
        // Block A2
        MatSetValue(linOpArr[0],i,2*probSize+i,iDk*tauTemp,ADD_VALUES);
      }
      else if (blockRow == 1) // Assign the entries for blocks A3 and A5
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = i-sBound+std::abs(sBound-i)+j;
          bpdc[j] = -PETSC_i*igamma*tauTemp*fdSpline[j+1];
          bpdc1[j] = -PETSC_i*igamma*(1.0+5.0/3.0*tauTemp)*fdSpline[j+1];
        }
        // Block A3
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc1,ADD_VALUES);
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = 2*probSize + i-sBound+std::abs(sBound-i)+j;
        }
        // Block A5
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);
      }
      else
      {
        // Block A6
        MatSetValue(linOpArr[0],k,i,ky*(omt-2.0/3.0*omn),ADD_VALUES);
      }

    }
    else if (i >= probSize-sBound)
    {

      SlepcSolver::FDCoeffGen(probSize-1-i-errOrder,probSize-1-i,pdOrder,dz,fdSpline);
      // Assign the entries for blocks A0, A1 and A2
      if (blockRow == 0)
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = probSize + probSize-errOrder+j;
          bpdc[j] = -PETSC_i*igamma*fdSpline[j];
        }
        // Block A1
        MatSetValues(linOpArr[0],one,&i,errOrder,bcolInds,bpdc,ADD_VALUES);
        // Block A0
        MatSetValue(linOpArr[0],i,i,ky*omn*+iDk*(1.0+5.0/3.0*tauTemp),ADD_VALUES);
        // Block A2
        MatSetValue(linOpArr[0],i,2*probSize+i,iDk*tauTemp,ADD_VALUES);
      }
      else if (blockRow == 1) // Assign the entries for blocks A3 and A5
      {
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = probSize-errOrder+j;
          bpdc[j] = -PETSC_i*igamma*tauTemp*fdSpline[j];
          bpdc1[j] = -PETSC_i*igamma*(1.0+5.0/3.0*tauTemp)*fdSpline[j];
        }
        // Block A3
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc1,ADD_VALUES);
        for (j=0; j<errOrder; ++j)
        {
          bcolInds[j] = 2*probSize + probSize-errOrder+j;
        }
        // Block A5
        MatSetValues(linOpArr[0],one,&k,errOrder,bcolInds,bpdc,ADD_VALUES);
      }
      else
      {
        // Block A6
        MatSetValue(linOpArr[0],k,i,ky*(omt-2.0/3.0*omn),ADD_VALUES);
      }
    }
    else
    {
      // Assign the entries for blocks A0, A1 and A2
      if (blockRow == 0)
      {
        for (j=0; j<=errOrder; ++j)
        {
          colInds[j] = probSize + i + j - sBound;
          pdc[j] = -PETSC_i*igamma*pdsc[j];
        }
        // Block A1
        MatSetValues(linOpArr[0],one,&i,nSplinePts,colInds,pdc,ADD_VALUES);
        // Block A0
        MatSetValue(linOpArr[0],i,i,ky*omn*+iDk*(1.0+5.0/3.0*tauTemp),ADD_VALUES);
        // Block A2
        MatSetValue(linOpArr[0],i,2*probSize+i,iDk*tauTemp,ADD_VALUES);
      }
      else if (blockRow == 1) // Assign the entries for blocks A3 and A5
      {
        for (j=0; j<=errOrder; ++j)
        {
          colInds[j] = i + j - sBound;
          pdc[j] = -PETSC_i*igamma*tauTemp*pdsc[j];
          pdc1[j] = -PETSC_i*igamma*(1.0+5.0/3.0*tauTemp)*pdsc[j];
        }
        // Block A3
        MatSetValues(linOpArr[0],one,&k,nSplinePts,colInds,pdc1,ADD_VALUES);
        for (j=0; j<=errOrder; ++j)
        {
          colInds[j] = 2*probSize + i + j - sBound;
        }
        // Block A5
        MatSetValues(linOpArr[0],one,&k,nSplinePts,colInds,pdc,ADD_VALUES);
      }
      else
      {
        // Block A6
        MatSetValue(linOpArr[0],k,i,ky*(omt-2.0/3.0*omn),ADD_VALUES);
      }

    }
  } // End global loop
  // Assemble the operators
  for (auto n = 0; n<2 ; n++)
  {
    MatAssemblyBegin(linOpArr[n],MAT_FINAL_ASSEMBLY);
    MatAssemblyEnd(linOpArr[n],MAT_FINAL_ASSEMBLY);
  }

  //PetscViewerBinaryOpen(PETSC_COMM_WORLD,"matA.dat",FILE_MODE_WRITE,&viewer);
  //PetscViewerPushFormat(viewer,PETSC_VIEWER_BINARY_MATLAB);
  //MatView(linOpArr[0],viewer);
  //PetscViewerDestroy(&viewer);

  //PetscViewerBinaryOpen(PETSC_COMM_WORLD,"matB.dat",FILE_MODE_WRITE,&viewer);
  //PetscViewerPushFormat(viewer,PETSC_VIEWER_BINARY_MATLAB);
  //MatView(linOpArr[0],viewer);
  //PetscViewerDestroy(&viewer);

  // Clean up the pointers
  delete[] colHypzInds;
  delete[] colInds;
  delete[] bcolInds;
  delete[] pdc;
  delete[] pdc1;
  delete[] bpdc;
  delete[] bpdc1;
  delete[] pdsc;
  delete[] hypzc;
  delete[] bhypzc;
  delete[] ihypzc;
}

void ITG3Field::extractEPSSolution(const SlepcSolver::SolverContext& sc, const double kx, const double ky)
{
  PetscScalar kr, ki;
  Vec xr, xi;
  PetscInt vecSize, nConv;
  PetscReal err;
  const PetscScalar *v;
  cmplxVec ev, evals;
  cmplxMat evs;
  d_Vec kpar, distToTarget, distToSoln, modeFilter, ev2, a2;
  cmplxVec soln;
  std::vector<bool> useMode;
  int evIdx;

  VecDuplicate(petscEigVec,&xr);
  VecDuplicate(petscEigVec,&xi);
  EPSGetConverged(eps,&nConv);
  if (nConv >= 1) {
    soln.resize(3);
    kpar.resize(nConv);
    distToTarget.resize(nConv);
    distToSoln.resize(nConv);
    modeFilter.resize(nConv);
    evals.resize(nConv);
    //evs.resize(nConv);
    a2.resize(nConv);
    useMode.resize(nConv);
    for (int i=0; i<nConv; i++)
    {
      EPSGetEigenpair(eps,i,&kr,&ki,xr,xi);
      petscEigVal = kr;
      evals[i] = kr;
      if (std::abs(PetscImaginaryPart(petscEigVal)) >= 1e-4) {
        EPSGetErrorEstimate(eps,i,&err);
        VecGetSize(xr,&vecSize); 
        if (vecSize/sc.nFields != probSize) throw(std::runtime_error("PTSM3D Error! Vectors have incompatible sizes!"));
        ev.resize(probSize);
        ev2.resize(probSize);
        //evs[i].resize(probSize);
        
        SlepcSolver::extractSeqVecField(sc,petscEigVecs[0],xr,0);
        VecGetArrayRead(petscEigVecs[0],&v);
        for (int j=0; j < probSize; ++j)
        {
          ev[j] = v[j];
          ev2[j] = PetscRealPart(v[j]*std::conj(v[j]));
        }
        VecRestoreArrayRead(xr,&v);
        //evs[i] = ev;
        auto maxPhi2Iter = std::max_element(ev2.begin(),ev2.end());
        auto maxPhi2 = *maxPhi2Iter;
        
        if (ev2.front()/maxPhi2 <= 1e-4 && ev2.back()/maxPhi2 <= 1e-4) {
          auto marginalMode = computeMarginalMode(kx,ky,FluidModel::getZBoundLower(),FluidModel::getZBoundUpper(),ev);
          a2[i] = std::get<0>(marginalMode);
          kpar[i] = std::get<1>(marginalMode);
          distToTarget[i] = std::abs(sc.target-petscEigVal);
          useMode[i] = true;
        } else {
          a2[i] = 0;
          kpar[i] = 1e8;
          distToTarget[i] = 1.0;
          useMode[i] = false;
        }
        VecDestroy(&petscEigVecs[0]);
      } else {
        a2[i] = 0;
        kpar[i] = 1e8;
        distToTarget[i] = 1.0;
        useMode[i] = false;
      } 
      modeFilter[i] = PetscImaginaryPart(sc.target) > 0 ?  std::pow(kpar[i],2)*std::pow(distToTarget[i],4) : std::pow(kpar[i],2)/std::abs(std::imag(soln[0]));

      //PetscScalar val[1];
      //PetscReal realView[3];
      //realView[0] = kpar[i]*kpar[i];
      //realView[1] = std::pow(distToTarget[i],4);
      //realView[2] = modeFilter[i];
      //PetscScalar scalView[3];
      //scalView[0] = -a2[i];
      //scalView[1] = -2*std::real(evals[i]);
      //scalView[2] = -a2[i] - 2*std::real(evals[i]);

      //DebugMsg(PetscIntView(1,&i,PETSC_VIEWER_STDOUT_WORLD););
      //val[0] = evals[i];
      //DebugMsg(PetscScalarView(1,val,PETSC_VIEWER_STDOUT_WORLD););
      //DebugMsg(PetscRealView(3,realView,PETSC_VIEWER_STDOUT_WORLD););
      //val[0] = -a2[i] - 2*std::real(evals[i]);
      //DebugMsg(PetscScalarView(3,scalView,PETSC_VIEWER_STDOUT_WORLD););
      //DebugMsg(std::cout << i << '\t' << evals[i] << '\t' << kpar[i]*kpar[i] << '\t' << a2[i] << '\t' << std::pow(distToTarget[i],4)  << '\t' << modeFilter[i] << '\t' << -a2[i] - 2*std::real(evals[i]) << '\n';);
    }
//#if defined(DEBUG)
//    HDF5Utils::writeEigenspectrum(this->getHDF5Mid(),evals,evs);
//#endif

    auto minIter = std::min_element(modeFilter.begin(),modeFilter.end());
    evIdx = std::distance(modeFilter.begin(),std::find(modeFilter.begin(),modeFilter.end(),*minIter));
    //std::cout << evIdx << '\n';
    MPI_Bcast(&evIdx,1,MPI_INT,0,PETSC_COMM_WORLD);
    MPI_Barrier(PETSC_COMM_WORLD);

    if (useMode[evIdx]) {
      //DebugMsg(std::cout << evIdx << '\n';);
      EPSGetEigenpair(eps,evIdx,&kr,&ki,xr,xi);
      petscEigVal = kr;
      // Copy the vector that will serve as the initial
      // condition for the next iteration
      VecCopy(xr,petscEigVec);

      VecGetSize(petscEigVec,&vecSize);
      if (vecSize/sc.nFields != probSize)
        throw(std::runtime_error("PTSM3D Error! Vectors have incompatible sizes!"));

      cmplxMat tempVec;
      tempVec.resize(3);
      for (auto j=0; j<3; ++j) {
        SlepcSolver::extractSeqVecField(sc,petscEigVecs[j],petscEigVec,j);
        VecGetArrayRead(petscEigVecs[j],&v);
        tempVec[j].resize(probSize);
        tempVec[j].assign(v,v+probSize);
        VecRestoreArrayRead(petscEigVecs[j],&v);
      }
      if (PetscImaginaryPart(petscEigVal) >= 0) {
        eigVals[0] = petscEigVal;
        eigVals[1] = std::conj(petscEigVal);
      } else {
        eigVals[0] = std::conj(petscEigVal);
        eigVals[1] = petscEigVal;
      }
      eigVals[2] = -a2[evIdx] - 2*std::real(petscEigVal);

      d_Vec Phi2(probSize);
      for (auto i = 0; i<probSize; ++i) Phi2[i] = std::pow(std::abs(tempVec[0][i]),2);
      auto maxPhi2Iter = std::max_element(Phi2.begin(),Phi2.end());
      auto maxPhi2 = *maxPhi2Iter;
      zIdx1 = 0;
      zIdx2 = probSize;
      for (auto i = 0; i<probSize; i++) {
        if (Phi2[i]/maxPhi2 > 1e-8) {
          zIdx1 = i;
          break;
        }
      }
      for (auto i=probSize-1; i>=0; i--) {
        if (Phi2[i]/maxPhi2 > 1e-8) {
          zIdx2 = i;
          break;
        }
      }
      for (auto i=0; i<3; ++i) {
        eigVec[i].resize(zIdx2-zIdx1+1);
        for (auto j=zIdx1; j <=zIdx2; ++j) {
          eigVec[i][j-zIdx1] = tempVec[i][j];
        }
      }

      DebugMsg(std::cout << "zIdx1:" << '\t' << zIdx1 << '\t' << "zIdx2" << '\t' << zIdx2 << '\n';);
    } else {
      petscEigVal = 0.0;
      eigVals[0] = petscEigVal;
      eigVals[1] = petscEigVal;
      eigVals[2] = petscEigVal;
    }

  } else {
    petscEigVal = 0.0;
    eigVals[0] = petscEigVal;
    eigVals[1] = petscEigVal;
    eigVals[2] = petscEigVal;
  }

  VecDestroy(&xr);
  VecDestroy(&xi);
}
*/
/******************************************************************************
*                                                                             *
* Routines to solve the eigenvalue problem using the SLEPc PEP functions      *
*                                                                             *
******************************************************************************/
/*
  Solving the polynomial eigenvalue equation L3*lambda^3 + L2*lambda^2 + L1*lambda + L0 = 0
*/
/*
void ITG3Field::buildPEPOperators(const double kx, const double ky)
{
  for (auto i=0; i<4; ++i)
  {
    MatZeroEntries(linOpArr[i]);
  }
  // Const definitions
  const PetscScalar piInv = 1.0/Utils::pi();
  const PetscScalar pi2Inv = std::pow(piInv,2.0);
  const PetscReal dz = geomPtr->getDEta();
  const PetscInt one = 1;

  // PetscInt pointers for the array column indices used in SetMatValues
  PetscInt *colInds, *bcolInds, *colHypzInds, *bcolHypzInds;

  // PetscScalar pointers to the data structures used in SetMatValues
  PetscScalar *pdc, *pdsc1, *pdsc2, *bpdc, *hypzc, *bhypzc, *ihypzc;

  // PetscInt variables for controlling indices and array bounds
  PetscInt i, j, globalIdx, nHypzPts, nSplinePts, li1, li2, sBound, hypzBound;

  // Internal variables
  PetscScalar hypz, ijacBInv, iDk, iBk, imodB, idparJacBInv;

  // Vectors containing the finite difference spline coefficients
  // The first order derivatives have suffix 1 and the second order derivatives have suffix 2
  cmplxVec fdSpline1, bfdSpline1, fdSpline2, bfdSpline2, hypzSpline;

  // Vectors containing the geometric coefficients Bk and Dk
  if (probSize != (FluidModel::getZBoundUpper() - FluidModel::getZBoundLower() + 1)) throw(std::runtime_error("PTSM3D Error! Vectors have incompatible sizes!"));
  d_Vec Bk(probSize), Dk(probSize);

  // Build the Bk and Dk vectors
  // TODO(bfaber): Compute the integer offset necessary for finite theta-k
  geomPtr->buildBk(kx,ky,FluidModel::getZBoundLower(),FluidModel::getZBoundUpper(),Bk);
  geomPtr->buildDk(kx,ky,FluidModel::getZBoundLower(),FluidModel::getZBoundUpper(),Dk);

  // TODO(bfaber): Move these constant definitions to read from input file
  sBound = errOrder/2;
  hypzBound = hypzOrder/2;

  // Set the vector sizes and resize
  nSplinePts = errOrder+1;
  nHypzPts = hypzOrder+1;
  fdSpline1.resize(nSplinePts);
  fdSpline2.resize(nSplinePts);
  hypzSpline.resize(nHypzPts);

  // Allocate memory for the PETSc data structures
  colHypzInds = new PetscInt[nHypzPts];
  bcolHypzInds = new PetscInt[hypzOrder];
  colInds = new PetscInt[nSplinePts];
  bcolInds = new PetscInt[errOrder];
  pdc = new PetscScalar[nSplinePts];
  pdsc1 = new PetscScalar[nSplinePts];
  pdsc2 = new PetscScalar[nSplinePts];
  bpdc = new PetscScalar[errOrder];
  hypzc = new PetscScalar[nHypzPts];
  bhypzc = new PetscScalar[hypzOrder];
  ihypzc = new PetscScalar[nHypzPts];

  // Define the hypz coefficient
  hypz = std::pow(PETSC_i,hypzOrder)*hypzEps*std::pow(0.5*dz,hypzOrder);

  // Compute the derivative splines once for the interior points
  SlepcSolver::FDCoeffGen(-sBound,sBound,1,dz,fdSpline1);
  SlepcSolver::FDCoeffGen(-sBound,sBound,2,dz,fdSpline2);
  SlepcSolver::FDCoeffGen(-hypzBound,hypzBound,hypzOrder,dz,hypzSpline);

  for (j=0; j<=errOrder; ++j)
  {
    pdsc1[j] = fdSpline1[j];
    pdsc2[j] = fdSpline2[j];
  }

  for (j=0; j<=hypzOrder; ++j)
  {
    hypzc[j] = hypz*hypzSpline[j];
  }


  // Necessary for doing parallel eigenvalue calculations
  MatGetOwnershipRange(linOpArr[0],&li1,&li2);

  for (i=li1; i<li2; ++i) // Begin global loop
  {
    globalIdx = FluidModel::getZBoundLower() + i;
    // Get the geometry coefficients at index i along the field line
    ijacBInv = geomPtr->jacBInv[globalIdx];
    imodB = geomPtr->modB[globalIdx];
    idparJacBInv = geomPtr->dparJacBInv[globalIdx];

    iDk = Dk[i];
    iBk = Bk[i];

    // Fill in the hyperdiffusions
    // Since the hyperdiffusions do not act on the eigenvalue, these are only inserted into the L0 operator
    if (i < hypzBound)
    {
      SlepcSolver::FDCoeffGen(-hypzBound+std::abs(hypzBound-i)-1,hypzOrder-i-1,hypzOrder,dz,hypzSpline);
      for (j=0; j<hypzOrder; ++j)
      {
        bcolHypzInds[j] = i-hypzBound+std::abs(hypzBound-i)+j;
        bhypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*hypz*hypzSpline[j+1];
      }
      MatSetValues(linOpArr[0],one,&i,hypzOrder,bcolHypzInds,bhypzc,ADD_VALUES);
    }
    else if (i >= probSize-hypzBound)
    {
      SlepcSolver::FDCoeffGen(probSize-1-i-hypzOrder,probSize-1-i,hypzOrder,dz,hypzSpline);
      for (j=0; j<hypzOrder; ++j)
      {
        bcolHypzInds[j] = probSize-hypzOrder+j;
        bhypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*hypz*hypzSpline[j];
      }
      MatSetValues(linOpArr[0],one,&i,hypzOrder,bcolHypzInds,bhypzc,ADD_VALUES);
    }
    else
    {
      for (j=0; j<=hypzOrder; ++j)
      {
        colHypzInds[j] = i + j - hypzBound;
        ihypzc[j] = -PETSC_i*std::pow(piInv*ijacBInv,hypzOrder)*hypzc[j];
      }
      MatSetValues(linOpArr[0],one,&i,nHypzPts,colHypzInds,ihypzc,ADD_VALUES);
    }


    // Fill linear operator
    // These terms do not involve derivatives of phi
    // The operator being add to is given by the linOpArr index, i.e. L3 is linOpArr[3]
    MatSetValue(linOpArr[3],i,i,1.0+iBk*(1.0+5.0/3.0*tauTemp),ADD_VALUES);
    MatSetValue(linOpArr[2],i,i,(ky*((omt-2.0/3.0*omn)*iBk*tauTemp-omn)-iDk*(1.0+5.0/3.0*tauTemp)),ADD_VALUES);
    MatSetValue(linOpArr[1],i,i,-ky*(omt-2.0/3.0*omn)*iDk*tauTemp,ADD_VALUES);

    //Deal with the derivatives
    if (i < sBound)
    {
      SlepcSolver::FDCoeffGen(-sBound+std::abs(sBound-i)-1,errOrder-i-1,1,dz,fdSpline1);
      SlepcSolver::FDCoeffGen(-sBound+std::abs(sBound-i)-1,errOrder-i-2,1,dz,fdSpline2);
      // Add entries to L1
      for (j=0; j<errOrder; ++j)
      {
        bcolInds[j] = i-sBound+std::abs(sBound-i)+j;
        bpdc[j] = -(1.0+5.0/3.0*tauTemp)*pi2Inv*ijacBInv*(idparJacBInv*fdSpline1[j+1] + ijacBInv*fdSpline2[j+1]);
      }
      MatSetValues(linOpArr[1],one,&i,errOrder,bcolInds,bpdc,ADD_VALUES);
      // Add entries to L0
      for (j=0; j<errOrder; ++j)
      {
        bcolInds[j] = i-sBound+std::abs(sBound-i)+j;
        bpdc[j] = -ky*(omt-2.0/3.0*omn)*tauTemp*ijacBInv*pi2Inv*(idparJacBInv*fdSpline1[j+1] + ijacBInv*fdSpline2[j+1]);
      }
      MatSetValues(linOpArr[0],one,&i,errOrder,bcolInds,bpdc,ADD_VALUES);
    } else if (i >= probSize-sBound)  {
      SlepcSolver::FDCoeffGen(probSize-1-i-errOrder,probSize-1-i,1,dz,fdSpline1);
      SlepcSolver::FDCoeffGen(probSize-1-i-errOrder,probSize-1-i,2,dz,fdSpline2);
      // Add entries to L1
      for (j=0; j<errOrder; ++j)
      {
        bcolInds[j] = probSize-errOrder+j;
        bpdc[j] = -(1.0+5.0/3.0*tauTemp)*pi2Inv*ijacBInv*(idparJacBInv*fdSpline1[j] + ijacBInv*fdSpline2[j]);
      }
      MatSetValues(linOpArr[1],one,&i,errOrder,bcolInds,bpdc,ADD_VALUES);
      // Add entries to L0
      for (j=0; j<errOrder; ++j)
      {
        bcolInds[j] = probSize-errOrder+j;
        bpdc[j] = -ky*(omt-2.0/3.0*omn)*tauTemp*ijacBInv*pi2Inv*(idparJacBInv*fdSpline1[j] + ijacBInv*fdSpline2[j]);
      }
      MatSetValues(linOpArr[0],one,&i,errOrder,bcolInds,bpdc,ADD_VALUES);
    } else {
      // Add entries to L1
      for (j=0; j<=errOrder; ++j)
      {
        colInds[j] = i + j - sBound;
        pdc[j] = -(1.0*5.0/3.0*tauTemp)*pi2Inv*ijacBInv*(idparJacBInv*pdsc1[j] + ijacBInv*pdsc2[j]);
      }
      MatSetValues(linOpArr[1],one,&i,nSplinePts,colInds,pdc,ADD_VALUES);
      // Add entries to L0
      for (j=0; j<=errOrder; ++j)
      {
        colInds[j] = i + j - sBound;
        pdc[j] = -ky*(omt-2.0/3.0*omn)*tauTemp*ijacBInv*pi2Inv*(idparJacBInv*pdsc1[j]+ ijacBInv*pdsc2[j]);
      }
      MatSetValues(linOpArr[0],one,&i,nSplinePts,colInds,pdc,ADD_VALUES);
    }
  }
  // Assemble the linear operators
  MatAssemblyBegin(linOpArr[0],MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(linOpArr[0],MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(linOpArr[1],MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(linOpArr[1],MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(linOpArr[2],MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(linOpArr[2],MAT_FINAL_ASSEMBLY);
  MatAssemblyBegin(linOpArr[3],MAT_FINAL_ASSEMBLY);
  MatAssemblyEnd(linOpArr[3],MAT_FINAL_ASSEMBLY);

  // Release the PETSc objects
  delete[] colHypzInds;
  delete[] colInds;
  delete[] bcolInds;
  delete[] pdc;
  delete[] bpdc;
  delete[] pdsc1;
  delete[] pdsc2;
  delete[] hypzc;
  delete[] bhypzc;
  delete[] ihypzc;
}

void ITG3Field::extractPEPSolution(const SlepcSolver::SolverContext& sc, const double kx, const double ky)
{
  PetscScalar kr, ki;
  Vec xr, xi;
  PetscInt vecSize, nConv;
  PetscReal err;
  const PetscScalar *v;
  cmplxVec ev, evals;
  cmplxMat evs;
  d_Vec kpar, distToTarget, modeFilter;
  cmplxVec soln;

  VecDuplicate(petscEigVec,&xr);
  VecDuplicate(petscEigVec,&xi);
  PEPGetConverged(pep,&nConv);
  if (nConv >= 1) {
    soln.resize(3);
    kpar.resize(nConv);
    distToTarget.resize(nConv);
    modeFilter.resize(nConv);
    evals.resize(nConv);
    evs.resize(nConv);
    for (int i=0; i<nConv; i++)
    {
      PEPGetEigenpair(pep,i,&kr,&ki,xr,xi);
      petscEigVal = kr;
      evals[i] = kr;
      PEPGetErrorEstimate(pep,i,&err);

      VecGetArrayRead(xr,&v);
      VecGetSize(xr,&vecSize);
      if (vecSize != probSize) throw(std::runtime_error("PTSM3D Error! Vectors have incompatible sizes!"));
      ev.resize(probSize);
      evs[i].resize(probSize);
      for (int j=0; j < probSize; ++j)
      {
        ev[j] = v[j];
      }
      VecRestoreArrayRead(xr,&v);
      evs[i] = ev;
      kpar[i] = computeLinearRoots(kx,ky,FluidModel::getZBoundLower(),FluidModel::getZBoundUpper(),ev,soln);
      distToTarget[i] = std::abs(sc.target-petscEigVal);
      modeFilter[i] = PetscImaginaryPart(sc.target) > 0 ? std::pow(kpar[i]*distToTarget[i],2) : std::pow(kpar[i],2);
      DebugMsg(std::cout << i << '\t' << evals[i] << '\t' << kpar[i]*kpar[i] << '\t' << distToTarget[i]*distToTarget[i] << '\t' << modeFilter[i] << '\n';);
    }
//#if defined(DEBUG)
//    HDF5Utils::writeEigenspectrum(this->getHDF5Mid(),evals,evs);
//#endif

    auto minIter = std::min_element(modeFilter.begin(),modeFilter.end());

    if (minIter != modeFilter.end()) {
      auto idx = std::distance(modeFilter.begin(),std::find(modeFilter.begin(),modeFilter.end(),*minIter));
      DebugMsg(std::cout << idx << '\n';);
      PEPGetEigenpair(pep,idx,&kr,&ki,xr,xi);
      petscEigVal = kr;
      VecCopy(xr,petscEigVec);

      VecGetSize(petscEigVec,&vecSize);
      VecGetArrayRead(petscEigVec,&v);
      for (auto i=0; i<3; ++i) eigVec[i].resize(vecSize);
      for (auto i=0; i<vecSize; i++)
      {
        eigVec[0][i] = v[i];
      }
      VecRestoreArrayRead(petscEigVec,&v);

      d_Vec FDSpline1;
      FDSpline1.resize(errOrder+1);

      const auto sBound = errOrder/2;
      const double dz = geomPtr->getDEta();
      c_double dparEvecSum1;

      for (auto i=0; i<vecSize; ++i) {
        dparEvecSum1 = 0.;
        eigVec[2][i] = -PETSC_i*ky/petscEigVal*(omt-2.0/3.0*omn)*eigVec[0][i];
        if (i < sBound) {
          Utils::FDCoeffGen(-sBound+std::abs(sBound-i),errOrder-i,1,dz,FDSpline1);
          for (auto j=0; j<=errOrder; j++)
          {
            auto idx = i-sBound+std::abs(sBound-i)+j;
            dparEvecSum1 += geomPtr->modBInv[i]*geomPtr->jacBInv[idx]*FDSpline1[j]*eigVec[0][idx];
          }
        // Deal with the upper boundary points
        } else if (i >= vecSize-sBound) {
          Utils::FDCoeffGen(vecSize-1-i-errOrder,vecSize-1-i,1,dz,FDSpline1);
          for (auto j=0; j<=errOrder; j++)
          {
            auto idx = vecSize-errOrder+j-1;
            dparEvecSum1 += geomPtr->modBInv[i]*geomPtr->jacBInv[idx]*FDSpline1[j]*eigVec[0][idx];
          }

        // Deal with the interior points
        } else {
          Utils::FDCoeffGen(-sBound,sBound,1,dz,FDSpline1);
          for (auto j=0; j<=errOrder; j++)
          {
            auto idx = i-sBound+j;
            dparEvecSum1 += geomPtr->modBInv[i]*geomPtr->jacBInv[i]*FDSpline1[j]*eigVec[0][idx];
          }
        }
        eigVec[1][i] = -1.0/(petscEigVal*Utils::pi())*((1.0+5.0/3.0*tauTemp) - PETSC_i*ky*tauTemp/petscEigVal*(omt-2.0/3.0*omn))*dparEvecSum1;

      }
      computeLinearRoots(kx,ky,FluidModel::getZBoundLower(),FluidModel::getZBoundUpper(),eigVec[0],soln);
      if (PetscImaginaryPart(petscEigVal) > 0) {
        eigVals[0] = petscEigVal;
        eigVals[1] = std::conj(eigVals[0]);
      } else {
        eigVals[0] = std::conj(petscEigVal);
        eigVals[1] = petscEigVal;
      }
      eigVals[2] = soln[2];
      d_Vec Phi2(probSize);
      for (auto i = 0; i<probSize; ++i) Phi2[i] = std::pow(std::abs(eigVec[0][i]),2);
      auto maxPhi2Iter = std::max_element(Phi2.begin(),Phi2.end());
      auto maxPhi2 = *maxPhi2Iter;
      zIdx1 = 0;
      zIdx2 = probSize;
      for (auto i = 0; i<probSize; i++) {
        if (Phi2[i]/maxPhi2 > 1e-8) {
          zIdx1 = i;
          break;
        }
      }
      for (auto i=probSize-1; i>=0; i--) {
        if (Phi2[i]/maxPhi2 > 1e-8) {
          zIdx2 = i;
          break;
        }
      }
      DebugMsg(std::cout << "zIdx1:" << '\t' << zIdx1 << '\t' << "zIdx2" << '\t' << zIdx2 << '\n';);


    } else {
      petscEigVal = 0.0;
      eigVals[0] = petscEigVal;
    }

  } else {
    petscEigVal = 0.0;
    eigVals[0] = petscEigVal;
  }

  VecDestroy(&xr);
  VecDestroy(&xi);
}
*/
/******************************************************************************
*                                                                             *
* Solver independent routines                                                 *
*                                                                             *
******************************************************************************/

/*
void ITG3Field::solve(SlepcSolver::SolverContext& sc)
{
  if (sc.probType == "eps") {
    SlepcSolver::solve(eps,sc,linOpArr);
  } else if (sc.probType == "pep") {
    SlepcSolver::solve(pep,sc,linOpArr);
  }
}

PetscScalar ITG3Field::guessTarget(const double kx, const double ky)
{
  PetscScalar targetGuess;
  cmplxVec phi;
  cmplxVec roots(3);
  //const auto delta = geomPtr->getq0()/Utils::pi();
  const auto delta = geomPtr->getNfp() > 1 ? 2.0/(geomPtr->getNfp() - 1.0/geomPtr->getq0())/Utils::pi() : geomPtr->getq0()/Utils::pi();
  const auto dz = geomPtr->getDEta();
  const auto nz = geomPtr->getNPts();
  const auto shat = geomPtr->getShat();
  const int vecSize =  (int)(std::round(delta/dz*std::sqrt(2.0*35.0))+1) % 2 == 1 ? std::round(delta/dz*std::sqrt(2.0*35.0))+1 : std::round(delta/dz*std::sqrt(2.0*35.0))+2;
  DebugMsg(std::cout << delta << '\t' << dz << '\t' << delta/dz << '\t' << shat << '\t' << vecSize << '\n';);
  //

  phi = Utils::gaussianMode(vecSize,delta,dz);
  auto zeroInd = std::floor(-kx/(shat*ky)/Utils::pi()) + nz/2;
  DebugMsg(std::cout << -kx/(shat*ky) << '\t' << std::floor(-kx/(shat*ky)) << '\n';);
  auto iz1 = zeroInd - vecSize/2-1;
  auto iz2 = zeroInd + vecSize/2-1;
  //DebugMsg(std::cout << nz << '\t'<<  vecSize << '\t' << zeroInd << '\t' << iz1 << '\t' << iz2 << '\t' << iz2-iz1+1 <<'\n';);
  computeLinearRoots(kx,ky,iz1,iz2,phi,roots);

  targetGuess = roots[0];
  return targetGuess;
}
*/

/*
  !!!! WARNING !!!!
  The formula for the cubic equation works best when coefficients are real.  This is
  accomplished by substituting lambda = -i omega to make all the coefficients real.
  This is opposite of the GENE convention and the convention used in buildEPS(PEP)operators!
*/
/*
double ITG3Field::computeLinearRoots(const double kx, const double ky, const int iz1, const int iz2,
                                   const cmplxVec& evec, cmplxVec& roots)
{
  const auto piInv = 1.0/Utils::pi();
  // Constants necessary for solving the dispersion relation
  const c_double posRootUnity(-0.5,0.5*std::sqrt(3.0)), negRootUnity(-0.5,-0.5*std::sqrt(3.0));

  // A vector of vectors to hold the linear operator
  cmplxVec Lavg(4,0);
  c_double iLavg3, iLavg2, iLavg1, iLavg0;
  c_double jLavg3, jLavg2, jLavg1, jLavg0;

  // Coefficient vectors for the interior points and the boundary points first derivatives
  cmplxVec fdc1Interior, fdc1Boundary;
  cmplxVec fdc2Interior, fdc2Boundary;

  // Vectors
  d_Vec evec2, FDSpline1, FDSpline2, Bk, Dk;
  // 1st and 2nd derivatives of the eigenvector along the field line
  c_double idparEvec, idparEvecConj, idpar2Evec, dparEvecSum, dpar2EvecSum, ievec, ievecConj, ievec2Sum, jevec2Sum, evec2Sum;
  c_double a0, a1, a2, p, q, d, u, v, usign, vsign;

  // Running sums to compute averages
  double iBk, iDk, ijac, imodB, ijacBInv, idparJacBInv;

  // Resize the vectors
  const auto vecSize = evec.size();

  const double dz = geomPtr->getDEta();
  FDSpline1.resize(errOrder+1);
  FDSpline2.resize(errOrder+1);
  evec2.resize(vecSize);
  Bk.resize(vecSize);
  Dk.resize(vecSize);
  geomPtr->buildBk(kx,ky,iz1,iz2,Bk);
  geomPtr->buildDk(kx,ky,iz1,iz2,Dk);

  // Compute the norm of the square of the eigenvector
  evec2Sum = 0;

  // Compute the 2nd order parallel derivative of the eigenfunction 
  // Compute the interior spline points first, so as to not repeat during
  // global loop
  auto sBound = errOrder/2;

  // Compute the elements in one global loop along the field line
  // Do the first element for the trapezoid rule
  iBk = Bk[0];
  iDk = Dk[0];
  ijac = geomPtr->jac[iz1];
  imodB = geomPtr->modB[iz1];
  ijacBInv = geomPtr->jacBInv[iz1];
  idparJacBInv = geomPtr->dparJacBInv[iz1];
  ievec = evec[0];
  ievecConj = std::conj(ievec);
  Utils::FDCoeffGen(-sBound+sBound,errOrder,1,dz,FDSpline1);
  Utils::FDCoeffGen(-sBound+sBound,errOrder,2,dz,FDSpline2);

  dparEvecSum = 0.;
  dpar2EvecSum = 0.;
  for (auto j=0; j<=errOrder; j++)
  {
    auto idx = -sBound+sBound+j;
    dparEvecSum += FDSpline1[j]*evec[idx];
    dpar2EvecSum += FDSpline2[j]*evec[idx];
  }

  idparEvec = dparEvecSum;
  idparEvecConj = std::conj(idparEvec);
  idpar2Evec = dpar2EvecSum;

  ievec2Sum = imodB*ijac*ievecConj*ievec;

  Utils::FDCoeffGen(-sBound,sBound,1,dz,FDSpline1);
  Utils::FDCoeffGen(-sBound,sBound,2,dz,FDSpline2);

  fdc1Interior.resize(errOrder+1);
  fdc2Interior.resize(errOrder+1);
  for (auto i=0; i<=errOrder; i++)
  {
    fdc1Interior[i] = FDSpline1[i];
    fdc2Interior[i] = FDSpline2[i];
  }

  for (auto k=1; k<vecSize-1; k++)
  {
    auto i = iz1 + k;
    iBk = Bk[k];
    iDk = Dk[k];
    ijac = geomPtr->jac[i];
    imodB = geomPtr->modB[i];
    ijacBInv = geomPtr->jacBInv[i];
    idparJacBInv = geomPtr->dparJacBInv[i];
    ievec = evec[k];
    ievecConj = std::conj(ievec);

    dparEvecSum = 0.;
    dpar2EvecSum = 0.;

    // Deal with the lower boundary points
    if (k < sBound) {
      Utils::FDCoeffGen(-sBound+std::abs(sBound-k),errOrder-k,1,dz,FDSpline1);
      Utils::FDCoeffGen(-sBound+std::abs(sBound-k),errOrder-k,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = k-sBound+std::abs(sBound-k)+j;
        dparEvecSum += FDSpline1[j]*evec[idx];
        dpar2EvecSum += FDSpline2[j]*evec[idx];
      }
    // Deal with the upper boundary points
    } else if (k >= vecSize-sBound) {
      Utils::FDCoeffGen(vecSize-1-k-errOrder,vecSize-1-k,1,dz,FDSpline1);
      Utils::FDCoeffGen(vecSize-1-k-errOrder,vecSize-1-k,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = vecSize-errOrder+j-1;
        dparEvecSum += FDSpline1[j]*evec[idx];
        dpar2EvecSum += FDSpline2[j]*evec[idx];
      }

    // Deal with the interior points
    } else {
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = k-sBound+j;
        dparEvecSum += fdc1Interior[j]*evec[idx];
        dpar2EvecSum += fdc2Interior[j]*evec[idx];
      }
    }
    idparEvec = dparEvecSum;
    idparEvecConj = std::conj(idparEvec);
    idpar2Evec = dpar2EvecSum;

    jLavg3 = iLavg3;
    iLavg3 = imodB*ijac*ievecConj*(1.0 + iBk*(1.0+5.0/3.0*tauTemp))*ievec;
    Lavg[3] += 0.5*(iLavg3 + jLavg3);

    jLavg2 = iLavg2;
    iLavg2 = imodB*ijac*ievecConj*(ky*((omt-2.0/3.0*omn)*iBk*tauTemp-omn)-iDk*(1.0+5.0/3.0*tauTemp))*ievec;
    Lavg[2] += 0.5*(iLavg2 + jLavg2);

    jLavg1 = iLavg1;
    iLavg1 = -imodB*ijac*ievecConj*(iDk*ky*(omt-2.0/3.0*omn)*tauTemp)*ievec -
              piInv*piInv*(1.0+5.0/3.0*tauTemp)*ijacBInv*idparEvecConj*idparEvec;
    Lavg[1] += 0.5*(iLavg1 + jLavg1);

    jLavg0 = iLavg0;
    iLavg0 = -ky*(omt-2.0/3.0*omn)*tauTemp*piInv*piInv*ijacBInv*idparEvecConj*idparEvec;
    Lavg[0] += 0.5*(iLavg0 + jLavg0);

    jevec2Sum = ievec2Sum;
    ievec2Sum = imodB*ijac*ievecConj*ievec;
    evec2Sum += 0.5*(ievec2Sum + jevec2Sum);
  } // End global loop

  // Normalize Lavg by the square of the eigenvector to actually compute the average
  const auto evec2Norm = 1.0/evec2Sum;
  for (auto i=0; i<4; i++)
  {
    Lavg[i] *= evec2Norm;
  }

  double kpar2_avg = -std::real(Lavg[0])/(ky*(omt-2.0/3.0*omn)*tauTemp);

  a2 = Lavg[2]/Lavg[3];
  a1 = Lavg[1]/Lavg[3];
  a0 = Lavg[0]/Lavg[3];

  dispersionCoeffs[0] = a0;
  dispersionCoeffs[1] = a1;
  dispersionCoeffs[2] = a2;

  p = 1.0/3.0*(3.0*a1 - std::pow(a2,2));
  q = 1.0/27.0*(9.0*a1*a2 - 27.0*a0 - 2.0*std::pow(a2,3));
  d = 1.0/27.0*std::pow(p,3) + 0.25*std::pow(q,2);


  if (std::real(d) > 0.) {
    usign = ((0.5*std::real(q) + std::sqrt(std::real(d))) > 0.) ? 1. : -1.;
    vsign = ((0.5*std::real(q) - std::sqrt(std::real(d))) < 0.) ? -1. : 1.;
    u = std::pow(std::abs(0.5*q + std::sqrt(d)),1.0/3.0);
    v = std::pow(std::abs(0.5*q - std::sqrt(d)),1.0/3.0);

    roots[0] = -1.0/3.0*a2 + posRootUnity*u*usign + negRootUnity*v*vsign;
    roots[1] = -1.0/3.0*a2 + posRootUnity*v*vsign + negRootUnity*u*usign;
    roots[2] = -1.0/3.0*a2 + usign*u + vsign*v;
  } else {
    u = std::pow(0.5*std::real(q) + std::sqrt(d),1.0/3.0);
    v = std::pow(0.5*std::real(q) - std::sqrt(d),1.0/3.0);

    roots[0] = std::real(-1.0/3.0*a2 + posRootUnity*u + negRootUnity*v);
    roots[1] = std::real(-1.0/3.0*a2 + posRootUnity*u + negRootUnity*v);
    roots[2] = std::real(-1.0/3.0*a2 + u + v);
  }

  //Diagnostic messages
  DebugMsg(std::cout << "====" << '\n';);
  DebugMsg(std::cout << Lavg[0] << '\t' << Lavg[1] << '\t' << Lavg[2] << '\t' << Lavg[3] << '\n';);
  DebugMsg(std::cout << p << '\t' << q << '\t' << d << '\t' << std::real(d) << '\n';);
  DebugMsg(std::cout << roots[0] << '\t' << roots[1] << '\t' << roots[2] << '\n';);
  // This should be zero for all modes
  DebugMsg(std::cout << roots[0]*roots[0]*roots[0] + roots[0]*roots[0]*a2 + roots[0]*a1 + a0 << '\n';);
  DebugMsg(std::cout << roots[1]*roots[1]*roots[1] + roots[1]*roots[1]*a2 + roots[1]*a1 + a0 << '\n';);
  DebugMsg(std::cout << roots[2]*roots[2]*roots[2] + roots[2]*roots[2]*a2 + roots[2]*a1 + a0 << '\n';);
  DebugMsg(std::cout << "====" << '\n';);

  //for (auto& root : roots) root = -PETSC_i*root;
  //return std::abs(Lavg[0]);
  return kpar2_avg;

}

std::pair<double,double> ITG3Field::computeMarginalMode(const double kx, const double ky, const int iz1, const int iz2,
                                   const cmplxVec& evec)
{
  const auto piInv = 1.0/Utils::pi();
  // Constants necessary for solving the dispersion relation
  // Coefficient vectors for the interior points and the boundary points first derivatives
  cmplxVec fdc1Interior, fdc1Boundary;

  // Vectors
  d_Vec evec2, FDSpline1, Bk, Dk;
  // 1st and 2nd derivatives of the eigenvector along the field line
  c_double idparEvec, idparEvecConj, dparEvecSum, ievec, ievecConj, ievec2Sum, jevec2Sum, evec2Sum;
  c_double ia0, ia1, ia2, ia3, ikpar2;
  c_double ja0, ja1, ja2, ja3, jkpar2;
  c_double a0 = 0, a1 = 0, a2 = 0, a3 = 0, kpar2 = 0;

  // Running sums to compute averages
  double iBk, iDk, ijac, imodB, ijacBInv;

  // Resize the vectors
  const auto vecSize = evec.size();

  const double dz = geomPtr->getDEta();
  //DebugMsg(std::cout << this->getErrOrder() << '\t' << errOrder << '\n';);
  FDSpline1.resize(errOrder+1);
  evec2.resize(vecSize);
  Bk.resize(vecSize);
  Dk.resize(vecSize);
  geomPtr->buildBk(kx,ky,iz1,iz2,Bk);
  geomPtr->buildDk(kx,ky,iz1,iz2,Dk);

  // Compute the norm of the square of the eigenvector
  evec2Sum = 0;

  // Compute the parallel derivative of the eigenfunction 
  // Compute the interior spline points first, so as to not repeat during
  // global loop
  auto sBound = errOrder/2;

  // Begin first entry for interation
  iBk = Bk[0];
  iDk = Dk[0];
  ijac = geomPtr->jac[iz1];
  imodB = geomPtr->modB[iz1];
  ijacBInv = geomPtr->jacBInv[iz1];
  ievec = evec[iz1];
  ievecConj = std::conj(ievec);

  Utils::FDCoeffGen(0,errOrder,1,dz,FDSpline1);
  for (auto j=0; j<=errOrder; j++)
  {
    auto idx = -sBound+sBound+j;
    dparEvecSum += FDSpline1[j]*evec[idx];
  }
  idparEvec = dparEvecSum;
  idparEvecConj = std::conj(idparEvec);

  ia3 = imodB*ijac*ievecConj*(1.0+iBk*(1.0+5.0/3.0*tauTemp))*ievec;
  ia2 = imodB*ijac*ievecConj*(ky*((omt-2.0/3.0*omn)*iBk*tauTemp-omn)-iDk*(1.0+5.0/3.0*tauTemp))*ievec;
  ia1 = -imodB*ijac*ievecConj*(iDk*ky*(omt-2.0/3.0*omn)*tauTemp)*ievec -
  ia0 = -ky*(omt-2.0/3.0*omn)*tauTemp*piInv*piInv*ijacBInv*idparEvecConj*idparEvec;
  ikpar2 += piInv*piInv*(1.0+5.0/3.0*tauTemp)*ijacBInv*idparEvecConj*idparEvec;
  ievec2Sum = imodB*ijac*ievecConj*ievec;
  //End first entry

  Utils::FDCoeffGen(-sBound,sBound,1,dz,FDSpline1);
  fdc1Interior.resize(errOrder+1);
  for (auto i=0; i<=errOrder; i++) fdc1Interior[i] = FDSpline1[i];

  // Compute the elements in one global loop along the field line
  for (auto k=1; k<vecSize-1; k++)
  {
    auto i = iz1 + k;
    iBk = Bk[k];
    iDk = Dk[k];
    ijac = geomPtr->jac[i];
    imodB = geomPtr->modB[i];
    ijacBInv = geomPtr->jacBInv[i];
    ievec = evec[k];
    ievecConj = std::conj(ievec);

    dparEvecSum = 0.;
    // Deal with the lower boundary points
    if (k < sBound) {
      Utils::FDCoeffGen(-sBound+std::abs(sBound-k),errOrder-k,1,dz,FDSpline1);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = k-sBound+std::abs(sBound-k)+j;
        dparEvecSum += FDSpline1[j]*evec[idx];
      }
    // Deal with the upper boundary points
    } else if (k >= vecSize-sBound) {
      Utils::FDCoeffGen(vecSize-1-k-errOrder,vecSize-1-k,1,dz,FDSpline1);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = vecSize-errOrder+j-1;
        dparEvecSum += FDSpline1[j]*evec[idx];
      }

    // Deal with the interior points
    } else {
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = k-sBound+j;
        dparEvecSum += fdc1Interior[j]*evec[idx];
      }
    }
    idparEvec = dparEvecSum;
    idparEvecConj = std::conj(idparEvec);

    ja3 = ia3;
    ia3 = imodB*ijac*ievecConj*(1.0+iBk*(1.0+5.0/3.0*tauTemp))*ievec;
    a3 += 0.5*(ia3 + ja3);

    ja2 = ia2;
    ia2 = imodB*ijac*ievecConj*(ky*((omt-2.0/3.0*omn)*iBk*tauTemp-omn)-iDk*(1.0+5.0/3.0*tauTemp))*ievec;
    a2 += 0.5*(ia2 + ja2);

    ja1 = ia1;
    ia1 = -imodB*ijac*ievecConj*(iDk*ky*(omt-2.0/3.0*omn)*tauTemp)*ievec -
      piInv*piInv*(1.0+5.0/3.0*tauTemp)*ijacBInv*idparEvecConj*idparEvec;
    a1 += 0.5*(ia1 + ja1);

    ja0 = ia0;
    ia0 = -ky*(omt-2.0/3.0*omn)*tauTemp*piInv*piInv*ijacBInv*idparEvecConj*idparEvec;
    a0 += 0.5*(ia0 + ja0);

    jkpar2 = ikpar2;
    ikpar2 += piInv*piInv*(1.0+5.0/3.0*tauTemp)*ijacBInv*idparEvecConj*idparEvec;
    kpar2 += 0.5*(ikpar2 + jkpar2);

    jevec2Sum = ievec2Sum;
    ievec2Sum = imodB*ijac*ievecConj*ievec;
    evec2Sum += 0.5*(ievec2Sum + jevec2Sum);
  } // End global loop

  // Normalize Lavg by the square of the eigenvector to actually compute the average
  const auto evec2Norm = 1.0/evec2Sum;
  a3 = a3*evec2Norm;
  a2 = a2*evec2Norm;
  a1 = a1*evec2Norm;
  a0 = a0*evec2Norm;
  kpar2 = kpar2*evec2Norm;

  a2 = a2/a3;
  a1 = a1/a3;
  a0 = a0/a3;

  dispersionCoeffs[0] = a0;
  dispersionCoeffs[1] = a1;
  dispersionCoeffs[2] = a2;

  auto result = std::make_pair(std::real(a2),std::real(kpar2));
  return result;
}
*/

void ITG3Field::computeMassMatrix(const double ky, const cmplxVec& dispRoots) {
  cmplxMat temp;
  temp.resize(3,3);

  massMat(0,0) = 1.0;
  massMat(0,1) = 1.0;
  massMat(0,2) = 1.0;
  massMat(1,0) = omt*ky/dispRoots[0];
  massMat(1,1) = omt*ky/dispRoots[1];
  massMat(1,2) = omt*ky/dispRoots[2];
  massMat(2,0) = PETSC_i*(1.0+omt*ky/dispRoots[0])/dispRoots[0];
  massMat(2,1) = PETSC_i*(1.0+omt*ky/dispRoots[1])/dispRoots[1];
  massMat(2,2) = PETSC_i*(1.0+omt*ky/dispRoots[2])/dispRoots[2];

  massMatInv = blaze::inv(massMat);
}
