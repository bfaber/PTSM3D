#ifndef _FLUIDMODEL_HPP_
#define _FLUIDMODEL_HPP_
#include <memory>

#include "Definitions.hpp"
#include "SlepcSolver.hpp"
//#include "HDF5Utils.hpp"

class PTSM3DControl; class Geom; class ITG3Field;

class FluidModel : std::enable_shared_from_this<FluidModel>
{
public:
  FluidModel();
  FluidModel(const int nFields);
  ~FluidModel();

  void initialize(const std::shared_ptr<PTSM3DControl> control);
  void setModelParameters();
  // Wrapper for solving the eigensystem
  void solve(SlepcSolver::SolverContext& sc);
  void createSlepcSolver();
  void destroySlepcSolver();

  // Provides an initial guess of the target
  virtual PetscScalar guessTarget(const double kx, const double ky);

  // Operations for building the linear operators
  void createLinearOperators(SlepcSolver::SolverContext& sc);
  void destroyLinearOperators(const SlepcSolver::SolverContext& sc);
  void zeroLinearOperators(const SlepcSolver::SolverContext& sc, const bool setVecToZero);
  virtual void buildLinearOperators(SlepcSolver::SolverContext& sc, const double kx, const double ky);
  virtual void extractSolution(const SlepcSolver::SolverContext& sc, const double kx, const double ky);
  virtual void computeMassMatrix(const double ky, const cmplxVec& dispRoots);

  inline void setErrOrder(const int x) { errOrder = x; };
  inline void setHypzOrder(const int x) { hypzOrder = x; };
  inline void setHypzEps(const double x) { hypzEps = x; };

  inline void setZBoundLower(const int x) { zBoundLower = x; };
  inline void setZBoundUpper(const int x) { zBoundUpper = x; };
  inline void setProbSize(const int x) { probSize = x; };
  inline void setNFields(const int x) { nFields = x; };
  inline void setBuffer( const int x) { buffer = x; };

  inline c_double getEigVal(const int fieldID) const { if (fieldID >= 0 && fieldID < nFields) { return eigVals[fieldID]; } else { throw std::runtime_error("FieldID "+std::to_string(fieldID)+" not in range 0 <= fieldID < nFields"); } }; // Access the eigenvalue, returns complex<double>
  void getEigVal(const int fieldID, c_double& ev) { if (fieldID >= 0 && fieldID < nFields) ev = eigVals[fieldID]; else throw std::runtime_error("FieldID "+std::to_string(fieldID)+" not in range 0 <= fieldID < nFields"); };

  // Access the eigenvector, returns cmplxVec
  inline cmplxVec getEigVals() const { return eigVals; };
  inline void getEigVals(cmplxVec& evs) { evs = eigVals; };

  cmplxVec getEigVecField(const int fieldID) const;
  void getEigVecField(const int fieldID, cmplxVec& vec);

  inline cmplxMat getEigVec() const { return eigVec; };
  inline void getEigVec(cmplxMat& vecs) { vecs = eigVec; };

  inline cmplxMat getMassMat() const { return massMat; };
  inline cmplxMat getMassMatInv() const { return massMatInv; };
  inline void getMassMat(cmplxMat& mass_Mat) { mass_Mat = massMat; };
  inline void getMassMatInv(cmplxMat& mass_Mat_Inv) { mass_Mat_Inv = massMatInv; };

  inline auto getErrOrder() const { return errOrder; };
  inline auto getHypzOrder() const { return hypzOrder; };
  inline auto getHypzEps() const { return hypzEps; };
  inline auto getNFields() const { return nFields; };

  inline auto getZBoundLower() const { return zBoundLower; };
  inline auto getZBoundUpper() const { return zBoundUpper; };
  inline auto getZIdx1() const { return zIdx1; };
  inline auto getZIdx2() const { return zIdx2; };

  inline auto getProbSize() const { return probSize; };
  inline auto getBuffer() const { return buffer; };

  inline void setGeomPtr(const std::shared_ptr<Geom> extGeomPtr) { geomPtr = extGeomPtr; };
  inline void setPTSM3DController(const std::shared_ptr<PTSM3DControl> extController) { controller = extController; };
  std::weak_ptr<FluidModel> getFluidModel();
protected:
  // Pointer to the geometry data, this can only be set to an external pointer and cannot
  // modify or delete the pointer
  std::shared_ptr<Geom> geomPtr;
  std::shared_ptr<PTSM3DControl> controller;
  std::shared_ptr<ITG3Field> model3Field;
  cmplxMat eigVec; // Standard complex double vector for eigenvector
  cmplxVec eigVals; // Standard complex double type for the eigenvalue
  cmplxMat massMat; // Mass matrix for the model
  cmplxMat massMatInv; // Inverse mass matrix for the model

  double omt;
  double omn;
  double tauTemp;

  int nFields; // Number of fields (identifies which model to use!)
  int probSize; // Size of the problem to solve
  int zIdx1; // z-index 1
  int zIdx2; // z-index 2
  int zBoundUpper;
  int zBoundLower;
  int buffer;
  int errOrder; //The error order of the derivative calculation
  int hypzOrder; // The order of the parallel hyperdiffusion operator
  double hypzEps; // The coefficient for the hyperdiffusion operator

  /* Slepc Objects */
  EPS eps;
  PEP pep;
  Mat* linOpArr;
  Vec petscEigVec;

};

#endif
