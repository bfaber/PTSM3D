#ifndef _INTERFACES_HPP_
#define _INTERFACES_HPP_

#include <memory>

class Geom;

typedef void * CObject;

namespace Interfaces {
  struct v2p_opts {
    char* vmec_file;
    char* grid_type;
    char* x3_coord;
    char* norm_type;
    int nx1, nx2, nx3;
    double* x1;
    double x2_center, x3_center, nfpi;
  };

  void PTSM3DStelloptInterface();
  void callVmec2Pest(std::string eqFile, double surface, const double dky,
                     const int nz0, const int buffer, Geom* geom);
};

extern "C"  {
  CObject createPTSM3D();
  void updatePTSM3DParam(CObject,char**,char**);
  void callPTSM3D(CObject,double*,double*);
  void destroyPTSM3D(CObject);

  // vmec2pest interface functions
  void vmec2pest_c_interface(Interfaces::v2p_opts*);
  void get_pest_data_c_interface(int*,int*,char**,int*,int*,double*);
}
#endif
