#include <iostream>
#include <fstream>
#include <vector>
#include <complex>
#include <string>

#include "blaze/math/DynamicVector.h"

#include "Definitions.hpp"
#include "SlepcSolver.hpp"
#include "Utils.hpp"


void SlepcSolver::solve(EPS eps,SlepcSolver::SolverContext solver, const std::shared_ptr<Mat> epsMatArr)
{
  const PetscScalar zero = 0;
  ST st;

  EPSSetOperators(eps,epsMatArr.get()[0],epsMatArr.get()[1]);
  EPSSetDimensions(eps,solver.nev,solver.ncv,solver.mpd);
  EPSSetTolerances(eps,solver.tol,solver.maxIt);
  EPSGetST(eps,&st);

  if (solver.target != zero) {
    STSetType(st,STSINVERT);
    STSetShift(st,solver.target);
    EPSSetTarget(eps,solver.target);
    EPSSetWhichEigenpairs(eps,EPS_TARGET_MAGNITUDE);
  } else {
    STSetType(st,STSHIFT);
    STSetShift(st,zero);
    EPSSetWhichEigenpairs(eps,EPS_LARGEST_IMAGINARY);
  }

  if (solver.icFlag == PETSC_TRUE) EPSSetInitialSpace(eps,1,solver.initCond.get());

  EPSSetType(eps,solver.eps.solver);

  EPSSolve(eps);
}

void SlepcSolver::solve(PEP pep, SlepcSolver::SolverContext solver, const std::shared_ptr<Mat> pepMatArr)
{
  const PetscScalar zero = 0;
  ST st;

  PEPSetOperators(pep,solver.nOps,pepMatArr.get());
  PEPSetProblemType(pep,PEP_GENERAL);
  PEPSetType(pep,solver.pep.solver);

  PEPSetDimensions(pep,solver.nev,solver.ncv,solver.mpd);
  PEPSetTolerances(pep,solver.tol,solver.maxIt);

  if (solver.target != zero) {
    PEPGetST(pep,&st);
    STSetType(st,STSINVERT);
    PEPSetTarget(pep,solver.target);
    PEPSetWhichEigenpairs(pep,PEP_TARGET_MAGNITUDE);
  } else {
    PEPSetWhichEigenpairs(pep,PEP_LARGEST_IMAGINARY);
  }

  PEPSolve(pep);
}

void SlepcSolver::extractSeqVecField(const SolverContext& solver, Vec& fieldVec,
                                 Vec& globalVec, const int fieldIdx) {
  Vec localVec;
  VecScatter gatherCtx;
  IS gatherIS;
  const PetscScalar *v;
  PetscScalar *vecValues;
  PetscInt *vecInds;
  PetscInt globalVecSize, fieldVecSize, localIdx, globalIdx;

  VecGetSize(globalVec,&globalVecSize);

  vecInds = new PetscInt[globalVecSize];
  for (localIdx = 0; localIdx < globalVecSize; ++localIdx) vecInds[localIdx] = localIdx;
  ISCreateGeneral(PETSC_COMM_SELF,globalVecSize,vecInds,PETSC_COPY_VALUES,&gatherIS);

  VecCreateSeq(PETSC_COMM_SELF, globalVecSize, &localVec);

  VecScatterCreate(globalVec,NULL,localVec,gatherIS,&gatherCtx);
  VecScatterBegin(gatherCtx,globalVec,localVec,INSERT_VALUES,SCATTER_FORWARD);
  VecScatterEnd(gatherCtx,globalVec,localVec,INSERT_VALUES,SCATTER_FORWARD);
  VecScatterDestroy(&gatherCtx);
  ISDestroy(&gatherIS);

  fieldVecSize = globalVecSize/solver.nFields;
  VecCreateSeq(PETSC_COMM_SELF,fieldVecSize,&fieldVec);

  vecValues = new PetscScalar[fieldVecSize];
  vecInds = new PetscInt[fieldVecSize];
  VecGetArrayRead(localVec,&v);
  for (localIdx = 0; localIdx < fieldVecSize; ++localIdx) {
    globalIdx = fieldIdx*fieldVecSize + localIdx;
    vecValues[localIdx] = v[globalIdx];
    vecInds[localIdx] = localIdx;
  }

  VecSetValues(fieldVec,fieldVecSize,vecInds,vecValues,INSERT_VALUES);
  VecAssemblyBegin(fieldVec);
  VecAssemblyEnd(fieldVec);
  VecDestroy(&localVec);
  delete[] vecValues;
  delete[] vecInds;
}
