#include <string>
#include <memory>
#include <exception>
#include <list>
#include <algorithm>

#include "mpi.h"

#include "PTSM3DControl.hpp"
#include "PTSM3DDriver.hpp"
#include "DriftWaveSpectrum.hpp"

int main(int argc, char* argv[]) {
  MPI_Init(NULL,NULL);
  PTSM3DControl controller(MPI_COMM_WORLD);

  // Parse the command line arguments for the number of split groups
  int n_split_groups = 1;
  if (controller.getGlobalRank() == 0) {
    std::list<std::string> argVals; 
    for (auto arg = 1; arg < argc; ++arg) {
      argVals.push_back(std::string(argv[arg]));
    }
    
    auto iter = std::find(argVals.begin(), argVals.end(), "-ng");
    if (iter != argVals.end()) {
      if (std::next(iter,1) != argVals.end()) {
        auto nextIter = std::next(iter,1);
        try {
          n_split_groups = std::stoi(*nextIter); 
        } catch (const std::exception& e) {
          std::cout << "Input argument exception caught with " << e.what() << '\n';
          n_split_groups = 1;
        }
      }
    }
  }
  
  
  /*
  //DriftWaveSpectrum(5, 5, 10, 3);
  controller.partitionMPIComm(n_split_groups);
  */
  controller.readInputFile("PTSM3D.inp");
  controller.initializeGeometry();
  MPI_Comm global_comm;
  controller.getGlobalComm(global_comm);
  MPI_Bcast(&n_split_groups,1,MPI_INT,0,global_comm);
  PTSM3DDriver driver();
  std::shared_ptr<PTSM3DControl> control_ptr = controller.getController();
  driver.initializeLocalDriver(n_split_groups,control_ptr);
  controller.testVectorPass(10);
  //controller.viewParameterInfo();
  MPI_Finalize();
}
