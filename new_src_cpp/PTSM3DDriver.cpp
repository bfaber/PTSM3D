#include <iostream>
#include <memory>

#include "mpi.h"

#include "PTSM3DDriver.hpp"
#include "SlepcSolver.hpp"
#include "Utils.hpp"
#include "DriftWaveSpectrum.hpp"
#include "FluidModel.hpp"

void PTSM3DDriver::initializeLocalDriver(const int n_Local_Comms, const std::shared_ptr<PTSM3DControl> control_ptr) {
  controller = control_ptr;
  nLocalComms = controller->partitionMPIComm(n_Local_Comms);
  MPI_Comm localComm;
  controller->getLocalComm(localComm);
  const std::shared_ptr<StringPairMap> paramInfoPtr = controller->getParamInfoPtr();
  Utils::getParameterValue(nKxModes,"nkx",paramInfoPtr);
  Utils::getParameterValue(nKyModes,"nky",paramInfoPtr);
  Utils::getParameterValue(nFields,"nFields",paramInfoPtr);

  PETSC_COMM_WORLD = localComm;
  SlepcInitialize((int*)0,(char***)0,(char*)0,(char*)0);
  model = std::make_shared<FluidModel>(nFields);
  model->initialize(control_ptr);
}

void PTSM3DDriver::finalizeLocalDriver() {
  SlepcFinalize();
}
