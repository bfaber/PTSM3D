#ifndef ITG3FIELD_HPP
#define ITG3FIELD_HPP

// STL includes
#include <iostream>
#include <vector>
#include <complex>
#include <utility>
#include <memory>

// External includes
#include "slepc.h"
#include "hdf5.h"

// Local includes
#include "Definitions.hpp"
#include "SlepcSolver.hpp"
//#include "HDF5Utils.hpp"
#include "FluidModel.hpp"

// Forward declarations of necessary classes
class Geom;
class DriftWave;

//class ITG3Field : public FluidModel
class ITG3Field : public FluidModel
{
  public:
    //Constructor and destructor
    ITG3Field();
    ITG3Field(const int n_fields);
    virtual ~ITG3Field();

    /********************************************************************************
      SLEPc eigensolver functions
    ********************************************************************************/
    // Wrapper for solving the eigensystem
    virtual void solve(SlepcSolver::SolverContext& sc);
    //virtual void createSlepcSolver();
    //virtual void destroySlepcSolver();

    // Provides an initial guess of the target
    virtual PetscScalar guessTarget(const double kx, const double ky);

    // Operations for building the linear operators
    virtual void buildLinearOperators(SlepcSolver::SolverContext& sc, const double kx, const double ky);
    virtual void extractSolution(const SlepcSolver::SolverContext& sc, const double kx, const double ky);
    virtual void computeMassMatrix(const double ky, const cmplxVec& dispRoots);

    //void duplicatePetscVec(Vec* vec);
    //Vec getPetscEigVec() const;

    // Cubic dispersion relation, does not require knowledge of the type of eigensolver
    virtual double computeLinearRoots(const double kx, const double ky, const int iz1, const int iz2,
                            const cmplxVec& evec, cmplxVec& roots);
    std::pair<double,double> computeMarginalMode(const double kx, const double ky, const int iz1, const int iz2, const cmplxVec& evec);

    // Function to build the EPS problem, only called by buildLinearOperators()
    void buildEPSOperators(const double kx, const double ky);
    void extractEPSSolution(const SlepcSolver::SolverContext& sc, const double kx, const double ky);

    // Function to build the PEP problem, only called by buildLinearOperators()
    void buildPEPOperators(const double kx, const double ky);
    void extractPEPSolution(const SlepcSolver::SolverContext& sc, const double kx, const double ky);

    inline void setModelPtr(std::weak_ptr<FluidModel> model_ptr) { modelPtr = model_ptr; };

  private:
    std::weak_ptr<FluidModel> modelPtr;
};

#endif
