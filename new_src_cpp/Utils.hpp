#ifndef _UTILS_HPP_
#define _UTILS_HPP_

#include <vector>
#include <complex>
#include <cmath>
#include <algorithm>
#include <cctype>
#include <locale>
#include <functional>
#include <exception>
#include <memory>
#include <sstream>

#include "Definitions.hpp"

namespace Utils
{
  constexpr auto pi() { return 3.1415926535897932846264338327950; };
  unsigned int factorial(const unsigned int);
  void FDCoeffGen(const int, const int, const int, const double, const bool, dblVec&);
  void FDCoeffGen(const int, const int, const int, const double, dblVec&);
  void TaylorCoeffs(const int, const int, const double, dblVec&);

  cmplxVec gaussianMode(const int vecSize, const double delta, const double dz);


  // Useful functions for dealing with strings
  // These functions were written by Evan Teran and found at
  // https://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
  // trim from start (in place)
  static inline void ltrim(std::string &s) {
      s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
          return !std::isspace(ch);
      }));
  }

  // trim from end (in place)
  static inline void rtrim(std::string &s) {
      s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
          return !std::isspace(ch);
      }).base(), s.end());
  }

  // trim from both ends (in place)
  static inline void trim(std::string &s) {
      ltrim(s);
      rtrim(s);
  }

  // trim from start (copying)
  static inline std::string ltrim_copy(std::string s) {
      ltrim(s);
      return s;
  }

  // trim from end (copying)
  static inline std::string rtrim_copy(std::string s) {
      rtrim(s);
      return s;
  }

  // trim from both ends (copying)
  static inline std::string trim_copy(std::string s) {
      trim(s);
      return s;
  }

  template <typename T>
  static void getParameterValue(T& par, const StringPairMap::iterator& iter) {
    std::stringstream convertStream;
    convertStream << std::get<0>(iter->second);
    convertStream >> par;
  }

  template <typename T>
  static void getParameterValue(T& par, const std::string parName, const std::shared_ptr<StringPairMap>& parameterInfo) {
    std::stringstream convertStream;
    auto iter = parameterInfo->find(parName);
    if (iter != parameterInfo->end()) {
      convertStream << trim_copy(std::get<0>(iter->second));
      convertStream >> par;
    } else {
      throw std::runtime_error("PTSM3D parameter "+parName+" has no info!");
    }

  }
  /*
  template <typename T>
  static void getParameterValue(T& par, const std::string parName, const std::shared_ptr<StringPairMap>& parameterInfo) {
    void* valPtr;
    auto convertParameter{ [&valPtr](StringPairMap::iterator iter) {
      char type = std::get<1>(iter->second);
      if (type == 'd') {
        auto val = std::stod(std::get<0>(iter->second));
        valPtr = static_cast<void*>(&val);
      } else if (type == 'i') {
        auto val = std::stoi(std::get<0>(iter->second));
        valPtr = static_cast<void*>(&val);
      } else if (type == 's') {
        auto val = std::string(std::get<0>(iter->second));
        valPtr = static_cast<void*>(&val);
      } else {
        throw std::runtime_error("PTSM3D input parameter "+(iter->first)+" has no type!");
      } }
    };

    auto iter = parameterInfo->find(parName);
    if (iter != parameterInfo->end()) {
      DebugMsg(std::cout << "#2 Converting " << iter->first << " with value " << std::string(std::get<0>(iter->second)) << " and type " << std::get<1>(iter->second) << '\n';);
      convertParameter(iter);
      par = *static_cast<T*>(valPtr);
    } else {
      throw std::runtime_error("PTSM3D parameter "+parName+" has no info!");
    }

  }

  template <typename T>
  static void getParameterValue(T& par, const StringPairMap::iterator& iter) {
    void* valPtr;
    auto convertParameter{ [&valPtr](StringPairMap::iterator iter) {
      char* type = &std::get<1>(iter->second);
      if (type == (char*)'d') {
        auto val = std::stod(std::get<0>(iter->second));
        valPtr = static_cast<void*>(&val);
      } else if (type == (char*)'i') {
        auto val = std::stoi(std::get<0>(iter->second));
        valPtr = static_cast<void*>(&val);
      } else if (type == (char*)'s') {
        auto val = std::string(std::get<0>(iter->second));
        valPtr = static_cast<void*>(&val);
      } else {
        throw std::runtime_error("PTSM3D input parameter "+std::string(iter->first)+" has no type!");
      } }
    };

    
    DebugMsg(std::cout << "Converting " << iter->first << " with value " << std::string(std::get<0>(iter->second)) << " and type " << std::get<1>(iter->second) << '\n';);
    convertParameter(iter);
    par = *static_cast<T*>(valPtr);

  }
  */

}
#endif
