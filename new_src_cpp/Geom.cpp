#include <iostream>
#include <vector>
#include <cmath>
#include <functional>
#include <memory>

#include "Definitions.hpp"
#include "Geom.hpp"
#include "Utils.hpp"
#include "Interfaces.hpp"

Geom::Geom() {}

void Geom::initializeGeometry(const std::shared_ptr<StringPairMap>& paramInfoPtr) {
  std::string geomType;
  Utils::getParameterValue(geomType,"geomType",paramInfoPtr);
  if (geomType == "eq") {
    std::string eqType;
    Utils::getParameterValue(eqType,"eqType",paramInfoPtr);
    if (eqType == "vmec") {
      std::string eqFile;
      Utils::getParameterValue(eqFile,"geomFile",paramInfoPtr);
      double surf, dky;
      int nz0, buff;
      Utils::getParameterValue(surf,"s0",paramInfoPtr);
      Utils::getParameterValue(dky,"dky",paramInfoPtr);
      Utils::getParameterValue(nz0,"nzfp",paramInfoPtr);
      Utils::getParameterValue(buff,"buffer",paramInfoPtr);
      Interfaces::callVmec2Pest(eqFile, surf, dky, nz0, buff, this);
    } else {
      throw std::runtime_error("Geometry module "+eqType+" is not currently implemented");
    }
  } else if (geomType == "s_alpha") {
    int nz0, buff;
    Utils::getParameterValue(nz0,"nzfp",paramInfoPtr);
    Utils::getParameterValue(buff,"buffer",paramInfoPtr);
    setGeomSize(nz0*buff);
    Utils::getParameterValue(q0,"q0_salpha",paramInfoPtr);
    Utils::getParameterValue(shat,"shat_salpha",paramInfoPtr);
    Utils::getParameterValue(invEps,"invEps_salpha",paramInfoPtr);
    s_alpha(q0,shat,invEps,(double)buff);

  } else {
    throw std::runtime_error("Geometry module "+geomType+" is not currently implemented");
  }

}


void Geom::setGeomSize(const int nz)
{
  nPts = (nz % 2 == 1) ? nz : nz+1;

  gxx.resize(nPts);
  gxy.resize(nPts);
  gyy.resize(nPts);
  modB.resize(nPts);
  jac.resize(nPts);
  dBdx.resize(nPts);
  dBdy.resize(nPts);
  dBdz.resize(nPts);
  eta.resize(nPts);
  modBInv.resize(nPts);
  jacBInv.resize(nPts);
  dparModBInv.resize(nPts);
  dpar2ModBInv.resize(nPts);
  dparJacBInv.resize(nPts);
}



void Geom::s_alpha(const double q0, const double shat, const double invEps, const double np)
{
  // Const definitions
  const auto nz = this->getNPts();
  const auto pi = Utils::pi();
  this->setq0(q0);
  this->setShat(shat);
  this->setInvEps(invEps);
  this->setNfp(1);

  int nz2, nz0;
  double dz, lz;
  double sinz, cosz, psz;

  nz2 = (nz-1)/2;
  nz0 = (nz-1)/(int)np;
  dz = 2.0/((double)nz0);
  this->setDEta(dz);
  
  for (int i=0; i<nz; ++i)
  {
    lz = (i-nz2)*dz; 
    psz = pi*shat*lz;
    sinz = std::sin(pi*lz);
    cosz = std::cos(pi*lz); 
    this->eta[i] = lz;
    this->gxx[i] = 1.0;
    this->gxy[i] = psz;
    this->gyy[i] = 1.0 + (psz)*(psz);
    this->modB[i] = 1.0/(1.0 + invEps*cosz);
    this->jac[i] = q0*(1.0 + invEps*cosz);
    this->dBdy[i] = -(cosz + psz*sinz)/((1.0+invEps*cosz)*(1.0+invEps*cosz));
    this->dBdx[i] = -sinz/((1.0+invEps*cosz)*(1.0+invEps*cosz));
    this->dBdz[i] = 0.0; 
    this->modBInv[i] = 1.0/this->modB[i];
    this->jacBInv[i] = 1.0/(this->jac[i]*this->modB[i]);
  }
}

void Geom::buildBk(const double kx, const double ky, const int z1, const int z2, dblVec& Bk)
{
  int i, j;
  for (i=z1; i<=z2; ++i)
  {
    j = i-z1;
    Bk[j] = ((kx*kx*(this->gxx[i]) + 2*kx*ky*(this->gxy[i]) + ky*ky*(this->gyy[i]))/(this->modB[i]*this->modB[i]));
  }
}

void Geom::buildDk(const double kx, const double ky, const int z1, const int z2, dblVec& Dk)
{
  int i, j;
  for (i=z1; i<=z2; ++i)
  {
    j = i-z1;
    Dk[j] = 2*(kx*(this->dBdx[i]) + ky*(this->dBdy[i]))/this->modB[i];
  }
}

void Geom::computeDerivedQuantities(const int errOrder)
{
  dblVec FDSpline1, FDSpline2;
  const int sBound = errOrder/2;
  const auto dz = getDEta();
  double modBInvSum, modBInvSum2, jacBInvSum;

  FDSpline1.resize(errOrder+1);
  FDSpline2.resize(errOrder+1);
  
  for (auto i=0; i<nPts; i++)
  {
    this->modBInv[i] = 1.0/(this->modB[i]);
    this->jacBInv[i] = 1.0/(this->jac[i]*this->modB[i]);
  }
  
  for (auto i=0; i<nPts; i++)
  {
    modBInvSum = 0;
    modBInvSum2 = 0;
    jacBInvSum = 0;
    if(i < sBound) {
      Utils::FDCoeffGen(-sBound+std::abs(sBound-i),errOrder-i,1,dz,FDSpline1);
      Utils::FDCoeffGen(-sBound+std::abs(sBound-i),errOrder-i,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = i-sBound+std::abs(sBound-i)+j;
        modBInvSum += FDSpline1[j]*this->modBInv[idx];
        modBInvSum2 += FDSpline2[j]*this->modBInv[idx];
        jacBInvSum += FDSpline1[j]*this->jacBInv[idx];
      }
    } else if (i >= nPts-sBound) {
      Utils::FDCoeffGen(nPts-1-i-errOrder,nPts-1-i,1,dz,FDSpline1);
      Utils::FDCoeffGen(nPts-1-i-errOrder,nPts-1-i,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = nPts-errOrder+j-1;
        modBInvSum += FDSpline1[j]*this->modBInv[idx];
        modBInvSum2 += FDSpline2[j]*this->modBInv[idx];
        jacBInvSum += FDSpline1[j]*this->jacBInv[idx];
      }
    } else {
      Utils::FDCoeffGen(-sBound,sBound,1,dz,FDSpline1);
      Utils::FDCoeffGen(-sBound,sBound,2,dz,FDSpline2);
      for (auto j=0; j<=errOrder; j++)
      {
        auto idx = i-sBound+j;
        modBInvSum += FDSpline1[j]*this->modBInv[idx];
        modBInvSum2 += FDSpline2[j]*this->modBInv[idx];
        jacBInvSum += FDSpline1[j]*this->jacBInv[idx];
      }
    }

    this->dparModBInv[i] = modBInvSum;
    this->dpar2ModBInv[i] = modBInvSum2;
    this->dparJacBInv[i] = jacBInvSum;
  
  }
}

int Geom::findZeroIndex()
{
  int zeroIndex = -1;
  auto dz = this->getDEta();
  for (auto i=0; std::abs(eta[i]) > dz; ++i) {
    zeroIndex = i;
  }
  return zeroIndex;
}

/*
void Geom::callVmec2Pest(std::string eqFile, double surface, const double dky,
                         const int nz0, const int buffer)
{
  char *diag_name;
  std::string diag_str;
  int x1, x2, data_size, n_dims;
  double* c_data;

  Interfaces::v2p_opts vmec2pest_options;
  char *file = &eqFile[0u];
  char gridType[] = "gene";
  char x3Coord[] = "theta";
  char normType[] = "minor_r";
  vmec2pest_options.vmec_file = file;
  vmec2pest_options.grid_type = gridType;
  vmec2pest_options.x3_coord = x3Coord;
  vmec2pest_options.norm_type = normType;

  vmec2pest_options.x1 = &surface;
  vmec2pest_options.nx1 = 1;
  vmec2pest_options.nx2 = 1;
  vmec2pest_options.nx3 = 1;
  
  vmec2pest_options.nfpi = 1.0;
  vmec2pest_options.x3_center = 0.0;
  DebugMsg(std::cout << "Calling vmec2pest_c_interface" << '\n';);
  vmec2pest_c_interface(&vmec2pest_options);
  
  diag_str = "nfp";
  diag_name = &diag_str[0u];
  x1 = 0;
  x2 = 0;
  n_dims = -1;
  data_size = 1;
  c_data = new double[data_size];
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  this->setNfp((int)c_data[0]);

  diag_str = "shat";
  diag_name = &diag_str[0u];
  n_dims = 0;
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  this->setShat(c_data[0]);

  diag_str = "safety_factor_q";
  diag_name = &diag_str[0u];
  n_dims = 0;
  get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
  this->setq0(c_data[0]);

  delete[] c_data;

  //Compute the maximum distance needed to compute along a field line 
  auto max_z = 1.0/(std::abs(shat)*dky);
  data_size = (std::ceil(max_z/Utils::pi()) + buffer)*nfp*nz0;
  vmec2pest_options.nx3 = data_size;
  vmec2pest_options.nfpi = (double)((std::ceil(max_z/Utils::pi())+buffer)*nfp);
  data_size += 1;
  vmec2pest_c_interface(&vmec2pest_options);

  v2p_DiagMap["g11"] = &Geom::setGxx;
  v2p_DiagMap["g12"] = &Geom::setGxy;
  v2p_DiagMap["g22"] = &Geom::setGyy;
  v2p_DiagMap["bmag"] = &Geom::setModB;
  v2p_DiagMap["jac"] = &Geom::setJac;
  v2p_DiagMap["curv_drift_x2"] = &Geom::setDBdx;
  v2p_DiagMap["curv_drift_x1"] = &Geom::setDBdy;
  v2p_DiagMap["d_B_d_x3"] = &Geom::setDBdz;
  v2p_DiagMap["x3"] = &Geom::setEta;

  dblVec temp;
  n_dims = 1; 
  this->setGeomSize(data_size);
  temp.resize(data_size+1);
  c_data = new double[data_size+1];
  for (std::unordered_map<std::string,geomFuncPtr>::iterator iter=v2p_DiagMap.begin(); iter != v2p_DiagMap.end(); ++iter)
  {
    diag_str = iter->first;
    diag_name = &diag_str[0u];
    get_pest_data_c_interface(&x1,&x2,&diag_name,&n_dims,&data_size,c_data);
    //for (auto i = 0; i < data_size; ++i) temp[i] = c_data[i];
    temp.assign(c_data,c_data+data_size+1);
    this->setGeomField(temp,iter->second);
  }
  delete[] c_data;
  this->setDEta((eta[1]-eta[0])/Utils::pi());
}
*/
