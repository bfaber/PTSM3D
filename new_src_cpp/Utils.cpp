#include <iostream>
#include <vector>
#include <cmath>

#include <blaze/math/DynamicMatrix.h>

#include "Definitions.hpp"
#include "Utils.hpp"

unsigned int Utils::factorial(const unsigned int x)
{
  return x <= 1 ? x : x*factorial(x-1);
}

void Utils::TaylorCoeffs(const int sl, const int sr, const double sc, dblVec& coeffs)
{
  int n;
  double fk;

  n = sr - sl + 1;

  for (int i = 0; i<n; ++i)
  {
    fk = (double)Utils::factorial(i);
    coeffs[i] = std::pow(sc,i)/fk;
  }
}


void Utils::FDCoeffGen(const int sl, const int sr, const int pdo,
                       const double h, const bool neg, dblVec& FDSpline)
{
  int j,k,n;
  double csign;
  dblVec tCoeffs;
  dblMat coeffMat, coeffMatInv;

  n = sr - sl + 1;
  tCoeffs.resize(n);
  coeffMat.resize(n,n);
  coeffMatInv.resize(n,n);

  if (neg)
  {
    csign = -1.0;
  } else {
    csign = 1.0;
  }

  for (k=0; k<n; ++k) 
  {
    Utils::TaylorCoeffs(sl,sr,(double)(sl+k),tCoeffs);
    for (j=0; j<n; ++j)
    {
      coeffMat(k,j) = tCoeffs[j];
    }
  }

  coeffMatInv = blaze::inv(coeffMat);

  for (j=0; j<n; ++j)
  {
    FDSpline[j] = csign*1.0/(std::pow(h,pdo))*coeffMatInv(pdo,j);
  } 
}

void Utils::FDCoeffGen(const int sl, const int sr, const int pdo,
                  const double h, dblVec& FDSpline)
{
  Utils::FDCoeffGen(sl,sr,pdo,h,false,FDSpline);
}

cmplxVec Utils::gaussianMode(const int vecSize, const double delta, const double dz)
{
  cmplxVec phi;
  phi.resize(vecSize);

  const auto bound = vecSize/2;

  for (int i=0; i<vecSize; i++)
  {
    phi[i] = std::exp(-0.5*std::pow((dz/delta),2.0)*std::pow((double)(i-bound),2.0));
  }
  return phi;
}

